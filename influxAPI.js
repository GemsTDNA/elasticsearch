/**
 * Created by Radhika on 6/21/2017.
 */
const Influx = require('influx');
const influx = new Influx.InfluxDB({
    host: '10.225.253.132',
    database: 'netdata',
    schema: [
        {
            measurement: 'kpi_alarms',
            fields: {
                VALUE: Influx.FieldType.FLOAT

            },
            tags: [
                'PRODUCT_NAME', 'LOCATION', 'VENDOR_NAME', 'ALARM_NAME', 'STATUS', 'level', 'IP_ADDRESS'
            ],
            timestamp: Date


        }
    ]

});
var logger = require('../libs/logger').influxLogger
var moment = require('moment')


function insertAlarm(req, res, next) {
    var message = JSON.parse(req.body.message)
    logger.info("the parsed message is" + JSON.stringify(message))
    logger.info("the time is" + req.body.time)
    logger.info("the level is" + req.body.level)
    var time = req.body.time
    if (message.VALUE == undefined) {
        var err = new Error("req.body.value is undefined")
        return next(err)
    }
    var epoch = moment.parseZone(req.body.time).unix()
    var level = req.body.level
    influx.writePoints([
        {
            measurement: 'kpi_alarms',
            tags: {
                PRODUCT_NAME: message.PRODUCT_NAME,
                LOCATION: message.LOCATION,
                VENDOR_NAME: message.VENDOR_NAME,
                ALARM_NAME: message.ALARM_NAME,
                STATUS: message.STATUS,
                level: req.body.level,
                IP_ADDRESS: message.IP_ADDRESS
            },
            fields: {VALUE: message.VALUE}, // fields should not be empty

            timestamp: epoch + "000000000"
        }
    ]).then(function () {
        logger.info("data inserted to influx")
        res.json({"status": 1})
    }).catch(function (err) {
        logger.error(err)
        return next(err)
    })

}


// get a2p and p2p (service_type)
function network(req, res, next) {
    influx.query(["select * from kpi_alarms where ALARM_NAME = 'Success_Rate' and level!= 'OK'", "select * from kpi_alarms where ALARM_NAME = 'Compliance' and level!= 'OK'", "select * from kpi_alarms where ALARM_NAME = 'Service_Uptime' and level!= 'OK'"]).then(function (results) {
        res.json({"status": 1, "data":{"Service":results[0],"Compliance":results[1],"Customer":results[2]}});

    }).catch(function (err) {
        logger.error(err)
        return next(err)
    })

}


exports.insertAlarm = insertAlarm
exports.network = network
