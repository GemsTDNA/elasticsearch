var express = require('express')
var app = express()
var config = require('config')
var path = require('path')
var fs = require('file-system')
//var elasticsearch = require('elasticsearch');
// var elasticSearchClient = new elasticsearch.Client({
//     host: config.get('elasticSearch.host'),
//     log: config.get('elasticSearch.log')
// })
var connection = 1

var mongoURL = config.get('stackStorm.mongoURL')
var st2Certificate = config.get('stackStorm.certificate')
var st2Key = config.get('stackStorm.key')




var certificatePath = path.join(__dirname, '../', st2Certificate);
var certificateKey = path.join(__dirname, '../', st2Key);


function checkIfElasticSearchIsOnline(req, res, next) {
    connection ++
    elasticSearchClient.ping({
        // ping usually has a 3000ms timeout
        requestTimeout: 1000
    }, function (error) {
        if (error) {
            return next(error)

        } else {
            console.log("the numbe rof connections is"+ connection)
            req.elasticSearchClient = elasticSearchClient
            return next()

        }
    });
}


function readSSLCertificates(req, res, next) {
    fs.readFile(certificatePath, function (errReadingCertificate, certificateFound) {
        if (errReadingCertificate) {
            return next(errReadingCertificate)
        }
        req.certificate = certificateFound
        fs.readFile(certificateKey, function (errReadingCertificateKey, certificateKey) {
            if (errReadingCertificateKey) {
                return next(errReadingCertificateKey)
            }
            req.key = certificateKey
            return next()
        })
    })

}





exports.checkIfElasticSearchIsOnline = checkIfElasticSearchIsOnline
exports.readSSlCertificates = readSSLCertificates
