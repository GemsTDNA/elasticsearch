
var path = require('path')
var mkdirp = require('mkdirp')
var winston = require('winston');
//require('winston-daily-rotate-file');
//var Rotate = require('winston-logrotate').Rotate;
var expressWinston = require('express-winston')
var dt 		= require('datetimejs');
var config	= require('config');




var LogsDir = path.join(__dirname, '..','logs'); // current directory is libs
var accessFileName = path.join(LogsDir, "access.log");
var errorFileName = path.join(LogsDir, "error.log");
var elasticSearchFileName = path.join(LogsDir, "elasticSearch.log");
var workflowFileName = path.join(LogsDir , "workflow.log");
var metricFileName = path.join(LogsDir , "metric.log");
var influxFileName = path.join(LogsDir,"influx.log");
var serviceUtilsFileName = path.join(LogsDir,"serviceUtils.log")
var stackStormFileName = path.join(LogsDir,"stackStorm.log")
var neo4jFileName = path.join(LogsDir,"neo4j.log")
var auditTrailFileName = path.join(LogsDir, "auditTrail.log")


mkdirp.sync(LogsDir);

function myTimestamp() {
	var date = new Date();
	return dt.strftime(date,'%Y-%m-%d %H:%M:%S:%r');
  //  return new Date().toString();
};




var accessLogObj = {
    transports: [
        new winston.transports.File({
            filename: accessFileName,
            maxsize: 1048576, //1MB
            maxFiles: 2,
            json: true,
            timestamp: myTimestamp,
            colorize: true
        })
    ],
    meta: true, // optional: control whether you want to log the meta data about the request (default to true)
    msg: "{{res.statusCode}} {{req.method}} {{req.url}} {{res.responseTime}}ms",
    colorStatus: true, // Color the status code, using the Express/morgan color palette (default green, 3XX cyan, 4XX yellow, 5XX red).
    // ignoreRoute: function (req, res) {
    //     if (req.url.startsWith("/docs") || req.url.startsWith("/dummy")){
    //         // Only for experiment
    //         return true;
    //     }
    //     else {
    //         return false;
    //     }
    // },
    requestWhitelist: ['url', 'method', 'httpVersion', 'query'] /// Array of request properties to log. Overrides global requestWhitelist for this instance
}

var accessLogger = new expressWinston.logger(accessLogObj)


var errorLogObj = {
    transports: [
        new winston.transports.File({
            filename: errorFileName,
            maxsize: 1048576, //1MB
            maxFiles: 2,
            json: true,
            timestamp: myTimestamp,
            colorize: true,
            handleExceptions: true,
            exitOnError: false,
        })
    ],
    colorStatus: true,
    requestWhitelist: ['url', 'headers', 'method', 'httpVersion', 'originalUrl', 'query', 'body']
}


var errorLogger = new expressWinston.errorLogger(errorLogObj);

var elasticSearchLogObj = {
    transports: [
        new (winston.transports.File)({
            name: 'message-log',
            filename: elasticSearchFileName,
            maxsize: 1048576, //1MB
            maxFiles: 2,
            level: 'info',
            json: true,
            timestamp: myTimestamp,
            colorStatus: true,
        })
        // new (winston.transports.DailyRotateFile)({
        //     filename: msgFileName,
        //     datePattern: 'yyyy-MM-dd.',
        //     prepend: true,
        //     level: 'info',
        //     json: true,
        //     timestamp: myTimestamp,
        // }),
        // new Rotate({
        //     file:  elasticSearchFileName, // this path needs to be absolute
        //     colorize: false,
        //     timestamp: myTimestamp,
        //     json: true,
        //     max: 1048576, //1MB
        //     keep: 5,
        //     compress: true
        // })

    ]
}


var logger = new winston.Logger(elasticSearchLogObj);


var workflowLogObj = {
    transports: [
        new (winston.transports.File)({
            name: 'workflow-log',
            filename:  workflowFileName,
            maxsize: 1048576, //1MB
            maxFiles: 2,
            level: 'info',
            json: true,
            timestamp: myTimestamp,
            colorStatus: true,
        })
        // new (winston.transports.DailyRotateFile)({
        //     filename: workflowFileName,
        //     datePattern: 'yyyy-MM-dd.',
        //     prepend: true,
        //     level: 'info',
        //     json: true,
        //     timestamp: myTimestamp,
        // }),

    //     new Rotate({
    //     file:  workflowFileName, // this path needs to be absolute
    //     colorize: false,
    //     timestamp: myTimestamp,
    //     json: true,
    //     max: 1048576, //1MB
    //     keep: 5,
    //     compress: true
    // })

    ]
}


var workflowLogger = new winston.Logger(workflowLogObj)

var metricLogObj = {
    transports: [
        new (winston.transports.File)({
            name: 'metric-log',
            filename:  metricFileName,
            maxsize: 1048576, //1MB
            maxFiles: 2,
            level: 'info',
            json: true,
            timestamp: myTimestamp,
            colorStatus: true,
        })
        // new (winston.transports.DailyRotateFile)({
        //     filename: metricFileName,
        //     datePattern: 'yyyy-MM-dd.',
        //     prepend: true,
        //     level: 'info',
        //     json: true,
        //     timestamp: myTimestamp,
        // }),
        // new Rotate({
        //     file:  metricFileName, // this path needs to be absolute
        //     colorize: false,
        //     timestamp: myTimestamp,
        //     json: true,
        //     max: 1048576, //1MB
        //     keep: 5,
        //     compress: true
        // })


    ]
}

var metricLogger = new winston.Logger(metricLogObj);


var influxLogObj = {
    transports: [
        new (winston.transports.File)({
            name: 'influx-log',
            filename:  influxFileName,
            maxsize: 1048576, //1MB
            maxFiles: 2,
            level: 'info',
            timestamp: myTimestamp,
            json: true,

            colorStatus: true,
        })
        // new (winston.transports.DailyRotateFile)({
        //     filename: metricFileName,
        //     datePattern: 'yyyy-MM-dd.',
        //     prepend: true,
        //     level: 'info',
        //     json: true,
        //     timestamp: myTimestamp,
        // }),
        // new Rotate({
        //     file:   influxFileName, // this path needs to be absolute
        //     colorize: false,
        //     timestamp: myTimestamp,
        //     json: true,
        //     max: 1048576, //1MB
        //     keep: 5,
        //     compress: true
        // })


    ]
}

var influxLogger = new winston.Logger(influxLogObj);


var serviceUtilsObj = {
    transports: [
        new (winston.transports.File)({
            name: 'serviceUtils-log',
            filename:  serviceUtilsFileName,
            maxsize: 1048576, //1MB
            maxFiles: 2,
            level: 'info',
            timestamp: myTimestamp,
            json: true,
            colorStatus: true,
        })
    ]
}

var serviceUtilsLogger = new winston.Logger(serviceUtilsObj);

var stackStormObj = {
    transports: [
        new (winston.transports.File)({
            name: 'stackStorm-log',
            filename:stackStormFileName  ,
            maxsize: 1048576, //1MB
            maxFiles: 2,
            level: 'info',
            timestamp: myTimestamp,
            json: true,
            colorStatus: true,
        })
    ]
}

var stackStormLogger = new winston.Logger(stackStormObj);


var neo4jObj = {
    transports: [
        new (winston.transports.File)({
            name: 'neo4j-log',
            filename:neo4jFileName  ,
            maxsize: 1048576, //1MB
            maxFiles: 2,
            level: 'info',
            timestamp: myTimestamp,
            json: true,
            colorStatus: true,
        })
    ]
}

var neo4jLogger = new winston.Logger(neo4jObj);

//

var auditTrailObj = {
    transports: [
        new (winston.transports.File)({
            name: 'auditTrail-log',
            filename:auditTrailFileName  ,
            maxsize: 1048576, //1MB
            maxFiles: 2,
            level: 'info',
            timestamp: myTimestamp,
            json: true,
            colorStatus: true,
        })
    ]
}

var auditTrailLogger = new winston.Logger(auditTrailObj);



exports.accessLogger = accessLogger
exports.errorLogger = errorLogger
exports.logger = logger
exports.workflowLogger = workflowLogger
exports.metricLogger = metricLogger
exports.influxLogger = influxLogger
exports.serviceUtilsLogger = serviceUtilsLogger
exports.stackStormLogger  = stackStormLogger
exports.neo4jLogger = neo4jLogger
exports.auditTrailLogger = auditTrailLogger

