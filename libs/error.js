/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var util = require("util")
var config = require("config")

function displayName(message, status) {
    /*INHERITANCE*/
    Error.call(this); //super constructor
    Error.captureStackTrace(this, this.constructor); //super helper method to include stack trace in error object

    //Set the name for the ERROR
    this.name = this.constructor.name; //set our function’s name as error name.
    this.status = status || 200;
    this.message = message || config.errorMessages.displayName;
}


function customErrorMessage(message,status){
      Error.call(this); //super constructor
    Error.captureStackTrace(this, this.constructor); //super helper method to include stack trace in error object

    //Set the name for the ERROR
    this.name = this.constructor.name; //set our function’s name as error name.
    this.status = status || 200;
    this.message = message ;
}

function filePath(message, status) {
    Error.call(this); //super constructor
    Error.captureStackTrace(this, this.constructor); //super helper method to include stack trace in error object

    //Set the name for the ERROR
    this.name = this.constructor.name; //set our function’s name as error name.
    this.status = status || 200;
    this.message = message || config.errorMessages.filePath;
}

function IPisNotUnique(message, status) {
    Error.call(this); //super constructor
    Error.captureStackTrace(this, this.constructor); //super helper method to include stack trace in error object

    //Set the name for the ERROR
    this.name = this.constructor.name; //set our function’s name as error name.
    this.status = status || 200;
    this.message = message || config.errorMessages.IP;
}


function serverError(message, status) {
    Error.call(this); //super constructor
    Error.captureStackTrace(this, this.constructor); //super helper method to include stack trace in error object

    //Set the name for the ERROR
    this.name = this.constructor.name; //set our function’s name as error name.
    this.status = status || 500;
    this.message = message || config.errorMessages.error;
}




util.inherits(displayName, Error);
util.inherits(filePath,Error)
util.inherits(IPisNotUnique,Error)
util.inherits(serverError,Error)
util.inherits(customErrorMessage,Error)


exports.displayName = displayName;
exports.filePath = filePath
exports.IPisNotUnique = IPisNotUnique
exports.serverError = serverError
exports.customErrorMessage =  customErrorMessage
