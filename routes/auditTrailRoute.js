/**
 * Created by Radhika on 9/27/2017.
 */
var express = require('express');
var router = express.Router();

var auditTrailHandlers = require('../handlers/auditTrailAPI')
var elasticSearchOnline = require('../libs/middleware').checkIfElasticSearchIsOnline

// router.get("/getServiceLayer", neo4jHandlers.getTopology)
 router.post("/insertAuditTrailData" ,elasticSearchOnline, auditTrailHandlers.insertAuditTrailData )
router.get("/listUserAuditConfigureDetails" ,elasticSearchOnline, auditTrailHandlers.listUserAuditConfigureDetails )
router.post('/validateAuditTrail' ,elasticSearchOnline , auditTrailHandlers.validateWhenAddingAuditTrailData)
router.post('/deleteUserAuditConfigureData',elasticSearchOnline,auditTrailHandlers.deleteUserAuditConfigureData)
router.post('/editUserAuditTrailData',elasticSearchOnline,auditTrailHandlers.editUserAuditTrailData)
//router.post('/validationWhileEditingUserAuditData',elasticSearchOnline,auditTrailHandlers.validationWhileEditingUserAuditData)
router.post('/getModifiedFileBasedOnOriginalFile',elasticSearchOnline,auditTrailHandlers.getModifiedFileBasedOnOriginalFile)





module.exports = router;
