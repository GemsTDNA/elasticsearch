/**
 * Created by Radhika on 7/7/2017.
 */
var express = require('express');
var router = express.Router();

var serviceUtilsHandler = require('../handlers/serviceUtilsAPI')
var elasticSearchOnline = require('../libs/middleware').checkIfElasticSearchIsOnline

router.get("/utils" , elasticSearchOnline, serviceUtilsHandler.getServiceUtils)
router.post("/getDetailedServiceUtils", elasticSearchOnline, serviceUtilsHandler.getDetailedServiceUtils)
//router.post("/geta2pServiceUtils", elasticSearchOnline, serviceUtilsHandler.geta2pServiceUtils)

module.exports = router;
