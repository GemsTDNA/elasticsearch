var express = require('express')
var elasticSearchRoute = require('./elasticSearchRoute')
var workFlowRoute = require('./workFlowRoute')
var metricsRoute = require('./metricRoute')
//var testRoute = require('./testRoute')
var influxRoute = require('./influxRoute')
var neo4jRoute = require('./neo4jRoute')
var serviceUtilsRoute = require('./serviceUtilsRoute')
var stackStormRoute = require('./stackStormRoute')
var auditTrailRoute = require('./auditTrailRoute')
var router = express.Router();

router.use('/elasticsearch', elasticSearchRoute)
router.use('/workflow' , workFlowRoute)
router.use('/metric' ,metricsRoute)
//router.use('/test',testRoute)
router.use('/influx',influxRoute)
router.use('/neo4j',neo4jRoute)
router.use('/serviceUtils',serviceUtilsRoute)
router.use('/stackStorm',stackStormRoute)
router.use('/auditTrail',auditTrailRoute)

module.exports = router;



