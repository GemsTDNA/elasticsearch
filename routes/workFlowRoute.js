/**
 * Created by Radhika on 6/8/2017.
 */
var express = require('express')
var router = express.Router()
var elasticSearchOnline = require('../libs/middleware').checkIfElasticSearchIsOnline

var workFlowHandler = require('../handlers/workFlowAPI')


router.use(elasticSearchOnline)
router.post('/insert' , workFlowHandler.insert)
router.get('/getData' ,workFlowHandler.getData)
//router.post('/test',workFlowHandler.test)
//router.post('/setData' ,workFlowHandler.setData)
router.post('/update' , workFlowHandler.update)


module.exports = router;