var express = require('express');
var router = express.Router();


var elasticSearchOnline = require('../libs/middleware').checkIfElasticSearchIsOnline
var metricHandlers = require('../handlers/metricAPI')

router.use( elasticSearchOnline)
router.post("/insert" , metricHandlers.insert)
router.get("/getData" , metricHandlers.getData)


module.exports = router;
