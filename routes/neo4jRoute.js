var express = require('express');
var router = express.Router();

var neo4jHandlers = require('../handlers/neo4jAPI')

router.get("/getServiceLayer", neo4jHandlers.getTopology)
router.post("/getGeoHash" , neo4jHandlers.getGeoHash)
router.post("/getServerDetails",neo4jHandlers.getServerDetails)
router.post('/getIPAndServerDetails',neo4jHandlers.getIPAndServerDetails)
router.post('/IPBasedOnProduct',neo4jHandlers.getIPBasedOnProduct)
router.post('/PLV',neo4jHandlers.PLV)
router.post('/LVP',neo4jHandlers.LVP)
router.post('/VLP',neo4jHandlers.VLP)
router.post('/VLPStackstorm',neo4jHandlers.VLPStackstorm)

//logical onboarding

router.get('/serverInfo',neo4jHandlers.serverInfoBasedOnProduct)
// homepage
router.post('/userListEscalate',neo4jHandlers.userListEscalate)
router.post('/sendEmail',neo4jHandlers.sendEmail)
router.post('/getIPAddressBasedOnVLP',neo4jHandlers.getIPAddressBasedOnVLP )



module.exports = router;
