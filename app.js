var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var config = require('config')

//var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var expressMongoDb = require('express-mongo-db');
var mongoURL = config.get('stackStorm.mongoURL')
var elasticsearch = require('elasticsearch');
 global.elasticSearchClient = new elasticsearch.Client({
    host: config.get('elasticSearch.host'),
    log: config.get('elasticSearch.log')
})
process.env.SUPPRESS_NO_CONFIG_WARNING = 'y';



var logger = require(path.join(__dirname, 'libs', 'logger'));


var api = require('./routes/api')


var app = express();
var logs = require('./libs/logger');
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
//app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});
//accessLogging
app.use(logger.accessLogger);

app.use(expressMongoDb(mongoURL));

app.use('/api',api)

app.use(logger.errorLogger);

app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    var responseJson = {};
    responseJson.status = 0;
    responseJson.msg = err.message;
    responseJson.errorName = err.name;
    res.status(err.status || 500);
    res.json(responseJson);
    res.end();
});


/*
 Express app.get('env') returns 'development' if NODE_ENV is not defined. So you don't need the line to test its existence and set default.
 */

if (app.get('env') === 'development') {
    process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
    app.use(function (err, req, res, next) {
        var responseJson = {};
        responseJson.status = 0;
        responseJson.msg = err.message;
        responseJson.errorName = err.name;
        res.status(err.status || 500);
        if(err.path){
            responseJson.path = err.path
        }
       // res.status(500)
        res.json(responseJson);
        res.end();
        return;
    });
}




//
// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    var responseJson = {};
    responseJson.status = 0;
    responseJson.stack = err.stack
    responseJson.msg = err.message;
    responseJson.errorName = err.name;
    res.status(err.status || 500);
    res.json(responseJson);
    res.end();
    return;
});

//errorLogging




module.exports = app;
