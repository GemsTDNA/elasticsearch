/**
 * Created by radhika on 7/27/2017.
 */
var path = require('path')
var request = require('request')


var neo4j = require('neo4j');
var redis = require('redis')
require('redis-delete-wildcard')(redis);
var moment = require('moment')
var ObjectId = require('mongodb').ObjectID


var config = require('config')
var st2Certificate = config.get('stackStorm.certificate')
var st2Key = config.get('stackStorm.key')

var client = redis.createClient(6379, '172.16.23.27');
var db = new neo4j.GraphDatabase('http://neo4j:teledna@172.16.23.27:7474');
var logger = require('../libs/logger').stackStormLogger;
var generic_mapper={

		"Service Uptime (In Secs)": "service_uptime",
		"CPU (%)" : "CPU",
		"Disk I/O In (kBps)" : "disk_in",
		"Disk I/O Out (kBps)": "disk_out",
		"Disk Used(%)":"used_disk",
		"Memory Used(%)":"mem_used",
		"IPv4 Bandwidth Sent(Kbps)":"ipv4_sent",
		"IPv4 Bandwidth Received(Kbps)":"ipv4_recv",
		"IPv6 Bandwidth Sent(Kbps)":"ipv6_sent",
		"IPv6 Bandwidth Received(Kbps)":"ipv6_recv"			
		}
var  actionArr=
        {
		"windows_cmd":"core.windows_cmd",
		"check_loadavg":"linux.check_loadavg",
		"check_processes":"linux.check_processes",
		"cp":"linux.cp",
		"diag_loadavg":"linux.diag_loadavg",
                "dig":"linux.dig",
		"file_touch":"linux.file_touch",
		"lsof":"linux.lsof",
		"lsof_pids":"linux.lsof_pids",
		"mv":"linux.mv",
		"netstat":"linux.netstat",
		"netstat_grep":"linux.netstat_grep",
		"pkill":"linux.pkill" ,
		"rm":"linux.rm",
                "rsync":"linux.rsync",
		"scp":"linux.scp",
		"service":"linux.service",
		"traceroute":"linux.traceroute",
		"vmstat":"linux.vmstat",
		"wait_for_ssh":"linux.wait_for_ssh",
                "announcement":"core.announcement",
		"http":"core.http",
		"local":"core.local",
		"local_sudo":"core.local_sudo",
		"noop":"core.noop",
		"pause":"core.pause",
		"remote":"core.remote",
                "remote_sudo":"core.remote_sudo",
		"sendmail":"core.sendmail",
		"uuid":"core.uuid"}
var  actArr=
          {
		"core.windows_cmd":"windows_cmd",
		"linux.check_loadavg":"check_loadavg",
		"linux.check_processes":"check_processes",
		"linux.cp":"cp",
		"linux.diag_loadavg":"diag_loadavg",
                "linux.dig":"dig",
		"linux.file_touch":"file_touch",
		"linux.lsof":"lsof",
		"linux.lsof_pids":"lsof_pids",
		"linux.mv":"mv",
		"linux.netstat":"netstat",
		"linux.netstat_grep":"netstat_grep",
		"linux.pkill":"pkill" ,
		"linux.rm":"rm",
                "linux.rsync":"rsync",
		"linux.scp":"scp",
		"linux.service":"service",
		"linux.traceroute":"traceroute",
		"linux.vmstat":"vmstat",
		"linux.wait_for_ssh":"wait_for_ssh",
                "core.announcement":"announcement",
		"core.http":"http",
		"core.local":"local",
		"core.local_sudo":"local_sudo",
		"core.noop":"noop",
		"core.pause":"pause",
		"core.remote":"remote",
                "core.remote_sudo":"remote_sudo",
		"core.sendmail":"sendmail",
		"core.uuid":"uuid"}
var sym={
	 "and" : '&&',
	  "or" : '||'
	}

function getCriteriaFromElasticSearch(obj, elasticSearchClient, ruleName, callback) {
   logger.info("obj"+JSON.stringify(obj))
    if (obj.trigger.type != 'core.st2.CronTimer' && obj.pack != 'core') {
        console.log("not a cron rule")

        elasticSearchClient.search({
            index: 'lucene_test',
            body: {
                query: {
                    match: {
                        ruleName: ruleName
                    }
                }
            }
        }, function (err, data) {
            if (err) {
                logger.error("error querying with index test util elasticSearch" + JSON.stringify(err))
                return callback(err)
            }
            if (data) {
                logger.info("elastic search returns results")
                return callback(null, data)


            }
        })
    }
    else {
        logger.info("addRule API : not a cron rule")
        var criteraFound = {
            hits: {
                hits: []
            }
        }
        return callback(null, criteraFound)

    }
}

// displays data seen on the  stackstorm Integrated rules page
function userRules(req, res, next) {
    logger.info("user Rules API called")
    var certificate = req.certificate
    var key = req.key
    var elasticSearchClient = req.elasticSearchClient
    //TO-DO: check if roles are required since api's use the PLV associated with rulename right now
    client.hset(req.body.UserName, 'role', 'groupOwner') //roles are not added to neo4j , aren't being used in the logic flow of the api's
    // })
    client.EXISTS(req.body.UserName + '_rules', function (errFindingKey, keyFound) {
        if (errFindingKey) {
            return next(errFindingKey)
        }
        if (keyFound) {
	    //var str="undefined"
            logger.info("rules set exists for the user:" + req.body.UserName)
            client.SMEMBERS(req.body.UserName + '_rules', function (errFindingRules, rulesFound) {
                if (errFindingRules) {
                    return next(errFindingRules)
                }
                var ruleArray = []
		var incr=0;
                  //rulesFound.forEach(function (ruleId) {
                  for(var i=0; i <rulesFound.length; i++) {
                    // pass ruleId to get details of the rule
                    ruleId=rulesFound[i]
		    var timeStamp = parseInt(ruleId.toString().substr(0,8), 16)*1000
		    var created_date = new Date(timeStamp)
                    logger.info("fetching rule details for the rule ID:" + ruleId)
		    //if(ruleId == 'undefined')
		    if( ruleId == 'undefined' || ruleId == null)
			{
			   incr = incr+1;	
			     logger.info("inside undefined")
			      continue;	
			}
                    ruleDetails(ruleId, certificate, key, function (ruleErr, ruleFound) {
                        if (ruleErr) {
                            return next(ruleErr)
                        }
                        var obj = JSON.parse(ruleFound)
			obj.createdTime=created_date
                        logger.info("obj %O",JSON.stringify(obj))
                        logger.info("the ruleName is" + obj.name)
                        logger.info("rule data" + JSON.stringify(obj))
                        // criteria stored in mongo is trigger.ruleName == rulename . Fetch criteria entered by user from ES
                        getCriteriaFromElasticSearch(obj, elasticSearchClient, obj.name, function (errFindingCriteria, criteriaFound) {

                            if (errFindingCriteria) {
                                return next(errFindingCriteria)
                            }
                            if (criteriaFound.hits.hits.length > 0) {
				logger.info("riteriaFound.hits.hits[0]._source "+criteriaFound.hits.hits[0]._source )
                                obj.criteriaList = criteriaFound.hits.hits[0]._source.criteriaList
			        obj.criteria=criteriaFound.hits.hits[0]._source.criteria_ES
			        obj.severity=criteriaFound.hits.hits[0]._source.severity
				logger.info(JSON.stringify(criteriaFound))
				if(obj.criteria != "generic" )
				{
			        	obj.type=criteriaFound.hits.hits[0]._source.Subcriteria
					if(typeof criteriaFound.hits.hits[0]._source.duration !== "undefined" ){
				        obj.duration=criteriaFound.hits.hits[0]._source.duration
				        obj.duration_value=criteriaFound.hits.hits[0]._source.duration_value
					}
					else{
					  obj.duration = "OnOccurence"
					  obj.duration_value = "0"
					}
					logger.info("obj.duration "+obj.duration )
					logger.info("obj.duration_value "+obj.duration_value )
					logger.info("obj.type "+obj.type )
				}
				else{
					obj.criteria="generic"
				   }
                            }

                            else {
                                obj.criteriaList = {}
                                obj.criteria = {}

                            }
                            /*
                             "parameters": {
                             "file": {"val":"/home/stanley/test19\n"},
                             "hosts": {"val":"172.16.23.27"}
                             }
                             */

                            // loops through the documents to remove PLV from the name before displaying it to the user
                            var splitStringByAmpersand = obj.name.split('&')

                            var parameters = obj.action.parameters
                            
                            var paramObj = {}
                            Object.keys(parameters).forEach(function (key) {
                                paramObj[key] = {"val": parameters[key]}


                            })

                            obj.action.parameters = paramObj
			    logger.info("obj.action.ref"+obj.action.ref)
                            obj.action.ref = actArr[obj.action.ref]
 			     logger.info("obj.criteriaList"+JSON.stringify(obj.criteriaList))
			   /*
   			    if(obj.criteria == "generic" ){
                             for(var i = 0; i < obj.criteriaList.length; i++) {
                             if (obj.criteriaList[i].cPattern != undefined && obj.criteriaList[i].cName == 'IP_ADDRESS' ) {
 
                                 var IP = obj.criteriaList[i].cPattern.slice(1,-1)
                                 obj.criteriaList[i].cPattern = IP
 
                             }
                             if (obj.criteriaList[i].cPattern != undefined && obj.criteriaList[i].cName == 'CONNECTIVITY' ) {
 
                                 var connectivity = obj.criteriaList[i].cPattern.slice(1,-1)
                                 obj.criteriaList[i].cPattern = connectivity
 
                             }
			    }
			  }
			*/
                            var username = splitStringByAmpersand[0] // SMSC_p-v-l-l
                            var ruleNameTobeDisplayed = splitStringByAmpersand[1] // ruleName
                            obj.name = ruleNameTobeDisplayed
                            obj.createdBy = username
                            ruleArray.push(obj)
			    if(rulesFound.length == 1 && incr == 1){
					
                                res.json({"status": 1, "data":"No rules for this user!!"})
				
				}
                            if (rulesFound.length-incr == ruleArray.length) {
                                res.json({"status": 1, "data": ruleArray})
                            }
                        })


                    })
                }
                //})

            })

        }
        else {
            logger.info("rules set in redis does not exists for the user:" + req.body.UserName)
            res.send({"status": 0, "msg": "no rules"})
        }
    })

}


function ruleDetails(id, certificate, key, callback) {

    var options = {
        url: 'https://10.225.253.132/api/v1/rules/' + id,
        agentOptions: {
            cert: certificate,
            key: key,
            rejectUnauthorized: false,

        }
    }

    request.get(options,
        function (error, response, body) {
            if (error) {
                // logger.error(error)
                return callback(error)
            }
            callback(null, body)

        })
}


function getAllRules(req, res, next) {
    var certificate = req.certificate
    var key = req.key
    var options = {
        url: 'https://10.225.253.132/api/v1/rules',
        agentOptions: {
            cert: certificate,
            key: key,
            rejectUnauthorized: false,

        }
    }
    request.get(options,
        function (error, response, body) {
            if (error) {
                // logger.error(error)
                return next(error)
            }
            res.send(body)

        })
}

function insertCriteriaIntoElasticSearch(criteriaList,userName, ruleName,criteria_type,duration,duration_type,Subcriteria,severity,elasticSearchClient, callback) {

    logger.info("criteriaList,userName, ruleName,criteria_type,duration,duration_type,Subcriteria,severity "+JSON.stringify(criteriaList)+" "+userName+" "+ruleName+" "+criteria_type+" "+duration+" "+duration_type+" "+Subcriteria+" "+severity)
    var splitStringByAmpersand = ruleName.split('&')
    var ruleNameTobeDisplayed = splitStringByAmpersand[1]
    var originalCriteria = criteriaList
    //var jsonData = JSON.parse(criteriaList);
    var jsonData = JSON.parse(JSON.stringify(criteriaList));
    //var jsonData = criteriaList;
    if(criteria_type == 'generic'){
        var parametersArr = []
       if( jsonData.length != 0){
    	for (var i = 0; i < jsonData.length; i++) {
                var Ccriteria = jsonData[i];
		logger.info(Ccriteria)
		if(i == 0)
		   {
		        var PRODUCT_NAME = Ccriteria.cProduct
	                var VENDOR_NAME  = Ccriteria.cVendor;
        	        var LOCATION     = Ccriteria.cLocation;
			var rule_redis=PRODUCT_NAME+'-'+VENDOR_NAME+'-'+LOCATION+'&'+ruleName
			var metadata_rule_redis=PRODUCT_NAME+'-'+VENDOR_NAME+'-'+LOCATION
			 client.SADD(metadata_rule_redis,ruleName, function (err, data) {
	                    if (err) {
        	                return callback({"status": 0,"msg": "error adding metadata to redis"})
                	    }
                   	 else {
                        	logger.info("metadata rule added to redis "+rule_redis)
	
        	            }
                	})
                        var conditionInEs ;
                        var conditionInRedis ;
		   }
                //var triggerName = key.split('.')
                if (Ccriteria.cType == 'gt') {
                    var symbol = '>'
                }
                else if (Ccriteria.cType == 'lt') {
                    var symbol = '<'
                }
                else if (Ccriteria.cType == 'neq') {
                    var symbol = '!='
                }else{
		
                    var symbol = '=='
		}


                // arr.push("myhash[" + "'" + triggerName[1] + "'" + "]" + " " + symbol + " " + jsonObj.pattern)

                if (Ccriteria.cName == "IP_ADDRESS") {
                    Ccriteria.cPattern = "'" + Ccriteria.cPattern + "'"
                }

                if (Ccriteria.cName == "CONNECTIVITY") {
                    Ccriteria.cPattern = "'" + Ccriteria.cPattern + "'"
                }

                //arr.push("myhash[" + "'" + key + "'" + "]" + " " + symbol + " " + jsonObj.pattern)
		if(i == 0){
                    conditionInEs ="myhash[" + "'" + Ccriteria.cName + "'" + "]" + " " + symbol + " " + Ccriteria.cPattern
                    conditionInRedis ="myhash[" + "'" + Ccriteria.cName + "'" + "]" + " " + symbol + " " +Ccriteria.cPattern
		} 
		else{
                    conditionInEs +=" " + Ccriteria.criteriaOperation+" myhash[" + "'" + Ccriteria.cName + "'" + "]" + " " + symbol + " " + Ccriteria.cPattern
                    conditionInRedis +=" " + sym[Ccriteria.criteriaOperation]+" myhash[" + "'" + Ccriteria.cName + "'" + "]" + " " + symbol + " " + Ccriteria.cPattern
		}

		parametersArr.push(Ccriteria.cName)
		 client.SADD('parameters_'+rule_redis, Ccriteria.cName, function (err, data) {
                    if (err) {
                        return callback({"status": 1,"msg": "error adding to redis"})
                    }
                    else {
                        logger.info("rule added to redis "+rule_redis)
                        logger.info("condition added to redis "+conditionInRedis)

                    }
                })

            }
		client.SADD(rule_redis, conditionInRedis, function (err, data) {
                    if (err) {
                        return callback({"status": 1,"msg": "error adding to redis"})
                    }
                    else {
			logger.info("rule added to redis "+rule_redis)
			logger.info("condition added to redis "+conditionInRedis)

                    }
		})
	console.log(conditionInEs)
        var criteria = {}
        criteria['trigger.results.ruleName'] = {"pattern": ruleName, "type": "equals"} //  & and other special characters  in mongo is best avoided
        elasticSearchClient.index({
            index: 'lucene_test',
	    refresh: 'true',
            type: 'logs',
            body: {
		"PRODUCT_NAME":PRODUCT_NAME,
                "VENDOR_NAME":VENDOR_NAME,
                "LOCATION":LOCATION,
                "condition": conditionInEs,
                "ruleName": ruleName,
                "parameters": parametersArr,
                "criteriaList": criteriaList,
	        "criteria_ES": criteria_type,
	        "severity": severity
            }
        }, function (error, response) {
            if (error) {
                logger.error("error with pushing data to elastic search " + error)
                return callback(error)
            } else {

                logger.info("data inserted to elastic search ");
                return callback(null, criteria)
            }
        });
	}
    else {
        criteria['trigger.results.ruleName'] = {"pattern": ruleName, "type": "equals"}
        logger.info("no criteria specified")
        return callback(null, criteria)
      }
   }
   else if(criteria_type == 'monitoring' &&(Subcriteria == 'File' || Subcriteria == 'HTTP' ||Subcriteria == 'Counter' )){
	 var parametersArr = []
	 var setArr = []
         var conditionInEs ;
	logger.info("in for monitoring")
       if( jsonData.length != 0){
            for (var i = 0; i < jsonData.length; i++) {
    		
                    var Ccriteria = jsonData[i];
                    logger.info(Ccriteria)
                    var PRODUCT_NAME = Ccriteria.cProduct
                    var VENDOR_NAME  = Ccriteria.cVendor;
                    var LOCATION     = Ccriteria.cLocation;                   
                    var symbol = '=='
		    
		     if(duration_type == "HH")
		    	{
				var dur = parseInt(duration)*60;
			}
		     else if(duration_type == "OnOccurence"){
				
			   var dur=0
                            Ccriteria.cPattern=1
                            Ccriteria.cType="count"
			}
		      else{
			var dur = parseInt(duration)
			
			}
    
                    if (Ccriteria.cName == "IP_ADDRESS") {
                        Ccriteria.cPattern = "'" + Ccriteria.cPattern + "'"
                    }
    
                    if (Ccriteria.cName == "CONNECTIVITY") {
                        Ccriteria.cPattern = "'" + Ccriteria.cPattern + "'"
                    }
    
                    if(i == 0){
                        conditionInEs ="myhash[" + "'" + Ccriteria.cName + "'" + "]" + " " + symbol + " " + Ccriteria.cPattern
                    }
                    else{
                        conditionInEs +=" " + Ccriteria.criteriaOperation+" myhash[" + "'" + Ccriteria.cName + "'" + "]" + " " + symbol + " " + Ccriteria.cPattern
                    }
    		    setname= Ccriteria.cProduct+"-"+Ccriteria.cVendor+"-"+Ccriteria.cLocation+":"+Ccriteria.cName+":"+dur+":"+Ccriteria.cType+"&"+ruleName
		    logger.info("setname "+setname)
                    setArr.push(setname)
                    parametersArr.push(Ccriteria.cName)
                }
                    logger.info("the condition inserted to elastic search is")
                    logger.info(conditionInEs)
                console.log(conditionInEs)
                logger.info("the condition inserted to elastic search is")
		logger.info("criteriaList "+criteriaList)
		logger.info("ruleName "+ruleName)
		logger.info("criteria_type"+ criteria_type)
		logger.info("duration_value "+duration)
		logger.info("duration "+duration_type)
		logger.info("Subcriteria"+Subcriteria)
		
               var arrlen=setArr.length
    	       for(var j=0; j <arrlen;j++)
    	         {
		  logger.info("Monitoring start set "+setArr[0])
		  logger.info("Monitoring set "+setArr[j]+"j "+j +"arrlen"+arrlen)
    	            client.SADD(setArr[j], 1, function (err, data) {
                        if (err) {
    			    for(var ii=0; ii <= j ;ii++){
    				client.srem(setArr[ii], 1, function (err, data) {
                    		if (err){
                        			return callback(err)
                    		}
    				})
    			     }
                                return callback(err)
    			}
				logger.info("data "+data)
				logger.info("Monitoring set "+setArr[j]+"j "+j)
                        })
                    }	       
                    var criteria = {}
                    criteria['trigger.results.ruleName'] = {"pattern": ruleName, "type": "equals"} //  & and other special characters  in mongo is best avoided
                    elasticSearchClient.index({
                        index: 'lucene_test',
			refresh: 'true',
                        type: 'logs',
                        body: {
                            "PRODUCT_NAME":"MULTIPLE",
                            "VENDOR_NAME":"MULTIPLE",
                            "LOCATION":"MULTIPLE",
                            "condition": conditionInEs,
                            "ruleName": ruleName,
                            "parameters": parametersArr,
                            "criteriaList": criteriaList,			  
			    "criteria_ES": criteria_type,
   			    "duration_value" :duration,
			    "duration" :duration_type,
			    "Subcriteria" :Subcriteria,
			    "severity": severity
                        }
                    }, function (error, response) {
                        if (error) {
                            logger.error("error with pushing data to elastic search " + error)
                            return callback(error)
                        } else {
    
                            logger.info("data inserted to elastic search ");
                            return callback(null, criteria)
                        }
                    });
        }
	    else{
                   criteria['trigger.results.ruleName'] = {"pattern": ruleName, "type": "equals"}
	           return callback(null, criteria)
	     }
       }
  if(criteria_type == 'audittrail'){
        var parametersArr = []
       if( jsonData.length != 0){
                var Ccriteria = jsonData[0];
                var conditionInEs ;
                logger.info(Ccriteria)
                var PRODUCT_NAME = Ccriteria.cProduct
                var VENDOR_NAME  = Ccriteria.cVendor;
                var LOCATION     = Ccriteria.cLocation;
                var metadata_rule_redis="audittrail_"+PRODUCT_NAME+'-'+VENDOR_NAME+'-'+LOCATION+'-'+Ccriteria.cName+'-'+Subcriteria
                var symbol = '=='
                var criteria = {}

                conditionInEs  =        "myhash[" + "'displayName'" + "]" + " " + symbol + " " + Ccriteria.cName
                conditionInEs +=" and "+"myhash[" + "'PRODUCT_NAME'" + "]" + " " + symbol + " " +PRODUCT_NAME
                conditionInEs +=" and "+"myhash[" + "'VENDOR_NAME'" + "]" + " " + symbol + " " +VENDOR_NAME
                conditionInEs +=" and "+"myhash[" + "'LOCATION'" + "]" + " " + symbol + " " +LOCATION
                if(Ccriteria.cOperations == "User"){
                        conditionInEs +=" and "+" myhash[" + "'User'" + "]" + " " + symbol + " " + Ccriteria.cPattern
                        metadata_rule_redis=PRODUCT_NAME+'-'+VENDOR_NAME+'-'+LOCATION+'-'+Ccriteria.cName+'-'+Subcriteria+'&'+Ccriteria.cOperations+'&'+Ccriteria.cType
                }
                else if(Ccriteria.cOperations == "Source IP"){
                        conditionInEs +=" and "+" myhash[" + "'addr'" + "]" + " " + symbol + " " + Ccriteria.cPattern
                         metadata_rule_redis=PRODUCT_NAME+'-'+VENDOR_NAME+'-'+LOCATION+'-'+Ccriteria.cName+'-'+Subcriteria+'&addr&'+Ccriteria.cType
                }
                parametersArr.push(Ccriteria.cName)
                         client.SADD(metadata_rule_redis,ruleName, function (err, data) {
                            if (err) {
                                return callback({"status": 0,"msg": "error adding metadata to redis"})
                            }
                         else {
                                logger.info("metadata rule added to redis "+rule_redis)

                            }
                        })

	        criteria['trigger.results.ruleName'] = {"pattern": ruleName, "type": "equals"} //  & and other special characters  in mongo is best avoided
	        elasticSearchClient.index({
	            index: 'lucene_test',
		    refresh: 'true',
	            type: 'logs',
	            body: {
	                "PRODUCT_NAME":PRODUCT_NAME,
	                "VENDOR_NAME":VENDOR_NAME,
	                "LOCATION":LOCATION,
	                "condition": conditionInEs,
	                "ruleName": ruleName,
	                "displayName": Ccriteria.cName,
	                "parameters": parametersArr,
	                "criteriaList": criteriaList,
			"Subcriteria":Subcriteria,
	                "criteria_ES": criteria_type,
	                "severity": severity
	            }
        	}, function (error, response) {
	            if (error) {
        	        logger.error("error with pushing data to elastic search " + error)
                	return callback(error)
	            } else {

	                logger.info("data inserted to elastic search ");
	                return callback(null, criteria)
        	    }
	        });
            }
    else {
        criteria['trigger.results.ruleName'] = {"pattern": ruleName, "type": "equals"}
        logger.info("no criteria specified")
        return callback(null, criteria)
      }
   }
  else if(criteria_type == 'kpi'){
         var parametersArr = []
         var setArr = []
         var conditionInEs ;
        logger.info("in for kpi")
       if( jsonData.length != 0){
            for (var i = 0; i < jsonData.length; i++) {

                    var Ccriteria = jsonData[i];
                    logger.info(Ccriteria)
                    var PRODUCT_NAME = Ccriteria.cProduct
                    var VENDOR_NAME  = Ccriteria.cVendor;
                    var LOCATION     = Ccriteria.cLocation;
/*
                    if (Ccriteria.cType == 'gt') {
                            var symbol = '>'
                     }
                    else if (Ccriteria.cType == 'lt') {
                            var symbol = '<'
                     }
                    else if (Ccriteria.cType == 'neq') {
                            var symbol = '!='
                    }else{

                            var symbol = '=='
                    }
*/
                    var symbol = '=='

                    if(i == 0){
                        //conditionInEs = "myhash['"+PRODUCT_NAME+"']"+"['" +VENDOR_NAME+"']"+"['" +LOCATION+"']"+"['" +Ccriteria.cName+"']"+ " " + symbol + " '"+Ccriteria.cType+"'"
                        conditionInEs = "myhash['"+PRODUCT_NAME+"']"+"['" +VENDOR_NAME+"']"+"['" +LOCATION+"']"+ "['"+Ccriteria.cType+"']"+"['"+Ccriteria.cName+"']"+ " " + symbol + "1"
                    }
                    else{
                        //conditionInEs +=" " + Ccriteria.criteriaOperation+ "myhash['"+PRODUCT_NAME+"']"+"['" +VENDOR_NAME+"']"+"['" +LOCATION+"']"+"['" +Ccriteria.cName+"']"+ " " + symbol + " '" + Ccriteria.cType+"'"
                        conditionInEs +=" " + Ccriteria.criteriaOperation+ " myhash['"+PRODUCT_NAME+"']"+"['" +VENDOR_NAME+"']"+"['" +LOCATION+"']"+ "['"+Ccriteria.cType+"']"+"['" +Ccriteria.cName+"']"+ " " + symbol + "1"
                    }
                    parametersArr.push(Ccriteria.cName)
                }
                logger.info("the condition inserted to elastic search is")
                logger.info(conditionInEs)
                logger.info("the condition inserted to elastic search is")
                logger.info("criteriaList "+JSON.stringify(criteriaList))
                logger.info("ruleName "+ruleName)
                logger.info("criteria_type"+ criteria_type)
                logger.info("Subcriteria"+Subcriteria)

                 var criteria = {}
                 criteria['trigger.results.ruleName'] = {"pattern": ruleName, "type": "equals"} //  & and other special characters  in mongo is best avoided
                 elasticSearchClient.index({
                        index: 'lucene_test',
                        refresh: 'true',
                        type: 'logs',
                        body: {
                            "PRODUCT_NAME":"MULTIPLE",
                            "VENDOR_NAME":"MULTIPLE",
                            "LOCATION":"MULTIPLE",
                            "condition": conditionInEs,
                            "ruleName": ruleName,
                            "parameters": parametersArr,
                            "criteriaList": criteriaList,
                            "criteria_ES": criteria_type,
                            "Subcriteria" :Subcriteria,
                            "severity": severity
                        }
                    }, function (error, response) {
                        if (error) {
                            logger.error("error with pushing data to elastic search " + error)
                            return callback(error)
                        } else {

                            logger.info("data inserted to elastic search ");
                            return callback(null, criteria)
                        }
                    });
             }
                 else{
                       criteria['trigger.results.ruleName'] = {"pattern": ruleName, "type": "equals"}
                       return callback(null, criteria)
                 }
          }




}

//}


function addRuleToMongo(certificate, key, rule, userName, callback) {
    // function called if req.body.rule..trigger is core.st2.CronTimer
    // and if req.body.rule.pack is core
    /*
     "parameters": {
     "file": {"val":"/home/stanley/test19\n"},
     "hosts": {"val":"172.16.23.27"}
     }
     */
    var parameters = rule.action.parameters
    var paramObj = {}
    var mongoRule = {}
    var cri={}
    Object.keys(parameters).forEach(function (key) {

        var getVal = parameters[key]
        // console.log("the key is:" + key)
        // console.log("the value is" + parameters[key])
        // console.log("getVal of val is" + getVal.val)
        // console.log(typeof getVal.val)
        if (getVal.val) {
            //delete parameters[key]
            paramObj[key] = getVal.val


        }
        // console.log(getVal.val)


    })
    logger.info("the paramObj is")
    logger.info(paramObj)

    rule.action.ref =actionArr[rule.action.ref]
    logger.info("the ref is")
    logger.info(rule.action.ref)
    rule.action.parameters = paramObj
    mongoRule.id=rule.id
    mongoRule.name=rule.name
    mongoRule.description=rule.description
    mongoRule.enabled=rule.enabled
    //cri['trigger.results.ruleName'] = {"pattern": rule, "type": "equals"}
    mongoRule.criteria={"trigger.results.ruleName": {"pattern": rule.name, "type": "equals"}}
    mongoRule.pack=rule.pack
    mongoRule.trigger=rule.trigger
    mongoRule.action=rule.action
    mongoRule.action.parameters=rule.action.parameters

//    logger.info("adding rule data to mongoDB via stackstorm API:" + JSON.stringify(rule));
    logger.info("adding rule data to mongoDB via stackstorm API:" + JSON.stringify(mongoRule));
    var options = {
        url: 'https://10.225.253.132/api/v1/rules',
        method: "POST",
        json: mongoRule,
        agentOptions: {
            cert: certificate,
            key: key,
            rejectUnauthorized: false,
        }
    }
    request.post(options,
        function (error, response, body) {
            if (error) {
                return callback(error)
            }

            if (body.faultstring != undefined) {
                return callback(null, body)

            }
            else {
                var id = body.id
                logger.info("the ID of the rule inserted to mongo is:" + id)
                client.SADD(userName + '_rules', id, function (err, data) {
                    if (err) {
                        return callback(err)
                    }
                    else {
                        return callback(null, {"status": 1})

                    }

                    // else {
                    //                 var criteria = body.criteria
                    //                  delete criteria['trigger.ruleName']
                    //                  var splitStringByAmpersand = body.name.split('&')
                    //                 var PLVUserName = splitStringByAmpersand[0].split('_') // SMSC_p-v-l-l
                    //                 var ruleNameTobeDisplayed = splitStringByAmpersand[1] // ruleName
                    //                  var username = PLVUserName[0]
                    //                  var PLVString = PLVUserName[1]
                    //                 var PLV = PLVString.split('-')
                    //                  var PRODUCT_NAME = PLV[0]
                    //                  var VENDOR = PLV[1]
                    //                 var LOCATION = PLV.slice(2, PLV.length)
                    //                 body.PRODUCT_NAME = PRODUCT_NAME
                    //                body.VENDOR = VENDOR
                    //                 body.LOCATION = LOCATION
                    //                body.name = ruleNameTobeDisplayed
                    //               return callback(null, {"status": 1, "data": body})
                    //               //  res.json({"status": 1, "data": body})
                    //              }


                })
            }
        })

}


function addRule(req, res, next) {
    logger.info("addRules handler")
    var userName = req.body.UserName
     if(req.body.rule.hasOwnProperty('severity')){
     		var severity = req.body.rule.severity
        }
        else{
                 var severity = "Minor"
        }
    logger.info("the username is:" + userName)
    logger.info("the RuleName is:" + req.body.rule.name)
    var ruleName = userName + '&' + req.body.rule.name
    req.body.rule.name = ruleName
    if (req.body.rule.trigger.type == 'core.st2.CronTimer' && req.body.rule.pack == 'core') {
        logger.info("cron rule added")
        logger.info(req.body)
        if (req.body.rule.trigger.parameters.month != undefined) {
            delete req.body.rule.trigger.parameters.month
        }
        addRuleToMongo(certificate, key, req.body.rule, req.body.UserName, function (err, data) {
            logger.info("trigger type is cronSt2Timer trigger,  elastic search insertion is not required ")
            if (err) {
                return next(err)
            }
            res.send(data)
        })
    }
    else {
        logger.info("regular rule")
        logger.info(req.body)
        var certificate = req.certificate
        var key = req.key
        var criteria = req.body.rule.criteriaList
        var criteriaType = req.body.rule.criteria
        var elasticSearchClient = req.elasticSearchClient
	if(req.body.rule.hasOwnProperty('duration_value')){
		var duration = req.body.rule.duration_value
	}
	else{
		 var duration = 0
	}
	if(req.body.rule.hasOwnProperty('type')){
		var Subcriteria = req.body.rule.type
	}
	else{
		var Subcriteria = 0
	}
	 if(req.body.rule.hasOwnProperty('duration')){
                var duration_type = req.body.rule.duration
        }
        else{
                 var duration_type = "None"
        }

	//'duration_value' in req.body.rule ? var duration = req.body.duration_value : var duration = 0
	//'type' in req.body.rule ? var Subcriteria = req.body.type : var Subcriteria = 0
        insertCriteriaIntoElasticSearch(criteria,req.body.UserName, ruleName,criteriaType,duration,duration_type,Subcriteria,severity,elasticSearchClient, function (err, data) {
            if (err) {
                return next(err)
            }
	    logger.info("data %OI " + data)
            req.body.rule.trigger.type = "elasticsearch.count_event"
            req.body.rule.trigger.ref = "elasticsearch.count_event";
            req.body.rule.criteria  = data  //criteria Object returned from the function
            req.body.rule.pack = "elasticsearch"
            addRuleToMongo(certificate, key, req.body.rule, req.body.UserName, function (errWhileCallingSSApi, SSAPISuccess) {
                if (errWhileCallingSSApi) {
                    return next(errWhileCallingSSApi)
                }
                res.send(SSAPISuccess)
            })

        })
    }

}

//ruleName,criteria_type,duration,Subcriteria,
function editCriteriaInES(elasticSearchClient,criteria_type,ruleName,duration,duration_type,Subcriteria, criteriaList,severity, callback) {
    var originalCriteria = criteriaList
    logger.info(typeof criteriaList)
    logger.info("criteria_type "+criteria_type)
    logger.info("criteriaList"+JSON.stringify(criteriaList))
    var jsonData = JSON.parse(JSON.stringify(criteriaList));
    if(criteria_type == 'generic'){
        var parametersArr = []
       if( jsonData.length != 0){
        for (var i = 0; i < jsonData.length; i++) {
                var Ccriteria = jsonData[i];
                logger.info("Ccriteria  "+JSON.stringify(Ccriteria))
                if(i == 0)
                   {
                        var PRODUCT_NAME = Ccriteria.cProduct
                        var VENDOR_NAME  = Ccriteria.cVendor;
                        var LOCATION     = Ccriteria.cLocation;
			var rule_redis=PRODUCT_NAME+'-'+VENDOR_NAME+'-'+LOCATION+'&'+ruleName
                        var conditionInEs ;
                        var conditionInRedis ;
                   }
                //var triggerName = key.split('.')
                if (Ccriteria.cType == 'gt') {
                    var symbol = '>'
                }
                else if (Ccriteria.cType == 'lt') {
                    var symbol = '<'
                }
                else if (Ccriteria.cType == 'neq') {
                    var symbol = '!='
                }else{

                    var symbol = '=='
                }


                if (Ccriteria.cName == "IP_ADDRESS") {
                    Ccriteria.cPattern = "'" + Ccriteria.cPattern + "'"
                }

                if (Ccriteria.cName == "CONNECTIVITY") {
                    Ccriteria.cPattern = "'" + Ccriteria.cPattern + "'"
                }

                //arr.push("myhash[" + "'" + key + "'" + "]" + " " + symbol + " " + jsonObj.pattern)
                if(i == 0){
                    conditionInEs ="myhash[" + "'" + Ccriteria.cName + "'" + "]" + " " + symbol + " " + Ccriteria.cPattern
		    conditionInRedis ="myhash[" + "'" + Ccriteria.cName + "'" + "]" + " " + symbol + " " +Ccriteria.cPattern
                }
                else{
                    conditionInEs +=" " + Ccriteria.criteriaOperation+" myhash[" + "'" + Ccriteria.cName + "'" + "]" + " " + symbol + " " + Ccriteria.cPattern
		    conditionInRedis +=" " + sym[Ccriteria.criteriaOperation]+" myhash[" + "'" + Ccriteria.cName + "'" + "]" + " " + symbol + " " + Ccriteria.cPattern
                }

                parametersArr.push(Ccriteria.cName)
                //var conditionInEs = arr.join(' and ')
                logger.info("the condition inserted to elastic search is")
            }
	   client.del(rule_redis, function(err, response) {
  			if (response) {			
			       logger.info("Deleted old rule going to add new rule!")
				client.SADD(rule_redis, conditionInRedis, function (err, data) {
		                    if (err) {
                        		  return callback({"status": 0,"msg": "error adding to redis!"})
                    			}
                    		  else {
				  	  logger.info("rule added to redis "+rule_redis)
				  	  logger.info("condition added to redis "+conditionInRedis)
                    		   }		
				})
			   } else{
				       logger.info("Cannot delete old rule in redis for edit!")
                        	       return callback({"status": 0,"msg": "Cannot delete old rule in redis for edit!"})
   			}
		})

    if (originalCriteria.length != 0) {

        //var conditionInEs = arr.join(' and ')
        // console.log("condition inserted to ES is")
        // console.log(conditionInEs)

        var criteria = {}
        criteria['trigger.ruleName'] = {"pattern": ruleName, "type": "equals"}
        var scriptToAddCriteria = {"inline": "ctx._source.criteriaList = " + "\"" + criteriaList + "\"" + ";"}
        var theScript = {
            "inline": "ctx._source.condition = " + "\"" + conditionInEs + "\"" + ";" + "ctx._source.remove(\"criteriaList\");"

        }
        // the below is a work around to avoid "tried to parse field as object but found a concrete value" which shows up  when using updateByQuery to modify criteria field
        // changed mapping to resolve issue - has not worked
        elasticSearchClient.updateByQuery({
            index: 'lucene_test',
	    //refresh: 'true',
            type: 'logs',
            body: {
                script: theScript,
                query: {
                    term: {
                        ruleName: ruleName,

                    }
                }
            }
        }, function (error, response) {
            if (error) {
                logger.error("error with pushing data to elastic search " + error)
                return callback(error)
            } else {
                elasticSearchClient.search({
                    index: 'lucene_test',
		    //refresh: 'true',
                    type: 'logs',
                    body: {
                        query: {
                            match: {
                                ruleName: ruleName
                            }
                        }
                    }
                }, function (errFindingRule, ruleFound) {
                    elasticSearchClient.update({
                        index: 'lucene_test',
			//refresh: 'true',
                        type: 'logs',
                        id: ruleFound.hits.hits[0]._id,
                        body: {
                            doc: {
                                criteriaList: criteriaList,
                                parameters: parametersArr,
				PRODUCT_NAME:PRODUCT_NAME,
                		VENDOR_NAME:VENDOR_NAME,
		                LOCATION:LOCATION,
		                condition: conditionInEs,
		                criteria_ES: criteria_type,
				severity: severity

                            },

                            query: {
                                term: {
                                    ruleName: ruleName
                                }
                            }
                        }
                    }, function (errPartialUpdate, partialUpdateSuccessful) {
                        if (errPartialUpdate) {
                            return callback(errPartialUpdate)
                        }
                        return callback(null, criteria)
                    })
                })


            }
        });

    }
    else {
        criteria['trigger.ruleName'] = {"pattern": ruleName, "type": "equals"}
        logger.info("no criteria specified")
        return callback(null, criteria)
    }

}
}
 else if(criteria_type == 'monitoring' &&(Subcriteria == 'File' || Subcriteria == 'HTTP'||Subcriteria == 'Counter' )){
        var parametersArr = []
        var conditionInEs ;
        var setArr = []
        var vari='*'+ruleName+'*'
       if( jsonData.length != 0){
        for (var i = 0; i < jsonData.length; i++) {
                var Ccriteria = jsonData[i];
                logger.info(Ccriteria)

                    var symbol = '=='
                if (Ccriteria.cName == "IP_ADDRESS") {
                    Ccriteria.cPattern = "'" + Ccriteria.cPattern + "'"
                }

                if (Ccriteria.cName == "CONNECTIVITY") {
                    Ccriteria.cPattern = "'" + Ccriteria.cPattern + "'"
                }
                if(i == 0){
                    conditionInEs ="myhash[" + "'" + Ccriteria.cName + "'" + "]" + " " + symbol + " " + Ccriteria.cPattern
                }
                else{
                    conditionInEs +=" " + Ccriteria.criteriaOperation+" myhash[" + "'" + Ccriteria.cName + "'" + "]" + " " + symbol + " " + Ccriteria.cPattern
                }
                setname= Ccriteria.cProduct+"-"+Ccriteria.cVendor+"-"+Ccriteria.cLocation+":"+Ccriteria.cName+":"+duration+":"+Ccriteria.cType+"&"+ruleName
                setArr.push(setname)
                parametersArr.push(Ccriteria.cName)
                logger.info("the condition inserted to elastic search is")
            }
        client.delwild(vari, function(error, numberDeletedKeys) {
            logger.info("deleted old keys "+numberDeletedKeys);
        })
               var arrlen=setArr.length
               for(var j=arrlen-1; j >= 0;j--)
                 {
                    client.SADD(setArr[j], 1, function (err, data) {
                        if (err) {
                            for(var ii=j; ii <= arrlen-1 ;ii++){
                                client.srem(setArr[ii], 1, function (err, data) {
                                if (err){
						logger.info("error in edit calling callback1")
                                                return callback(err)
                                }
                                })
                             }
						logger.info("error in edit calling callback2")
                                return callback(err)
                        }
                        })
                    }


    if (originalCriteria.length != 0) {

        var criteria = {}
        criteria['trigger.ruleName'] = {"pattern": ruleName, "type": "equals"}
        var scriptToAddCriteria = {"inline": "ctx._source.criteriaList = " + "\"" + criteriaList + "\"" + ";"}
        var theScript = {
            "inline": "ctx._source.condition = " + "\"" + conditionInEs + "\"" + ";" + "ctx._source.remove(\"criteriaList\");"

        }
        // the below is a work around to avoid "tried to parse field as object but found a concrete value" which shows up  when using updateByQuery to modify criteria field
        // changed mapping to resolve issue - has not worked
        elasticSearchClient.updateByQuery({
            index: 'lucene_test',
	    //refresh: 'true',
            type: 'logs',
            body: {
                script: theScript,
                query: {
                    term: {
                        ruleName: ruleName,

                    }
                }
            }
        }, function (error, response) {
            if (error) {
                logger.error("error with pushing data to elastic search " + error)
                return callback(error)
            } else {
                elasticSearchClient.search({
                    index: 'lucene_test',
		  //  refresh: 'true',
                    type: 'logs',
                    body: {
                        query: {
                            match: {
                                ruleName: ruleName
                            }
                        }
                    }
                }, function (errFindingRule, ruleFound) {
                    elasticSearchClient.update({
                        index: 'lucene_test',
		       // refresh: 'true',
                        type: 'logs',
                        id: ruleFound.hits.hits[0]._id,
                        body: {
                            doc: {
                                criteriaList: criteriaList,
                                parameters: parametersArr,
                            	condition: conditionInEs,
                                criteria_ES: criteria_type,
                                duration_value: duration,
                                duration: duration_type,
                                Subcriteria: Subcriteria,
				severity: severity

                            },

                            query: {
                                term: {
                                    ruleName: ruleName
                                }
                            }
                        }
                    }, function (errPartialUpdate, partialUpdateSuccessful) {
                        if (errPartialUpdate) {
                            return callback(errPartialUpdate)
                        }
                        return callback(null, criteria)
                    })
                })


            }
        });

    }
    else {
        criteria['trigger.ruleName'] = {"pattern": ruleName, "type": "equals"}
        logger.info("no criteria specified")
        return callback(null, criteria)
    }

}
}
else if(criteria_type == 'audittrail'){
       var parametersArr = []
       var conditionInEs ;
       var setArr = []
       var vari='*'+ruleName+'*'
       if( jsonData.length != 0){
           var Ccriteria = jsonData[0];
           logger.info(Ccriteria)
           var symbol = '=='
           client.delwild(vari, function(error, numberDeletedKeys) {
              logger.info("deleted old keys "+numberDeletedKeys);
           })
           var conditionInEs ;
           logger.info(Ccriteria)
           var PRODUCT_NAME = Ccriteria.cProduct
           var VENDOR_NAME  = Ccriteria.cVendor;
           var LOCATION     = Ccriteria.cLocation;

            var metadata_rule_redis=PRODUCT_NAME+'-'+VENDOR_NAME+'-'+LOCATION+'-'+Ccriteria.cName
            var symbol = '=='
            var criteria = {}
            conditionInEs  =        "myhash[" + "'displayName'" + "]" + " " + symbol + " " + Ccriteria.cName
            conditionInEs +=" and "+"myhash[" + "'IP_ADDRESS'" + "]" + " " + symbol + " " +Ccriteria.cIpAddress
            conditionInEs +=" and "+"myhash[" + "'PRODUCT_NAME'" + "]" + " " + symbol + " " +PRODUCT_NAME
            conditionInEs +=" and "+"myhash[" + "'VENDOR_NAME'" + "]" + " " + symbol + " " +VENDOR_NAME
            conditionInEs +=" and "+"myhash[" + "'LOCATION'" + "]" + " " + symbol + " " +LOCATION
            if(Ccriteria.cOperations == "User"){
                    conditionInEs +=" and "+" myhash[" + "'User'" + "]" + " " + symbol + " " + Ccriteria.cPattern
               }
            else if(Ccriteria.cOperations == "Source IP"){
                     conditionInEs +=" and "+" myhash[" + "'addr'" + "]" + " " + symbol + " " + Ccriteria.cPattern
                }
             parametersArr.push(Ccriteria.cName)
             client.SADD(metadata_rule_redis,ruleName, function (err, data) {
                   if (err){
                             return callback({"status": 0,"msg": "error adding metadata to redis"})
                         }
                   else{
                              logger.info("metadata rule added to redis "+rule_redis)

                         }
                  })

    if (originalCriteria.length != 0) {

        var criteria = {}
        criteria['trigger.ruleName'] = {"pattern": ruleName, "type": "equals"}
        var scriptToAddCriteria = {"inline": "ctx._source.criteriaList = " + "\"" + criteriaList + "\"" + ";"}
        var theScript = {
            "inline": "ctx._source.condition = " + "\"" + conditionInEs + "\"" + ";" + "ctx._source.remove(\"criteriaList\");"

        }
        // the below is a work around to avoid "tried to parse field as object but found a concrete value" which shows up  when using updateByQuery to modify criteria field
        // changed mapping to resolve issue - has not worked
        elasticSearchClient.updateByQuery({
            index: 'lucene_test',
	    //refresh: 'true',
            type: 'logs',
            body: {
                script: theScript,
                query: {
                    term: {
                        ruleName: ruleName,

                    }
                }
            }
        }, function (error, response) {
            if (error) {
                logger.error("error with pushing data to elastic search " + error)
                return callback(error)
            } else {
                elasticSearchClient.search({
                    index: 'lucene_test',
	//	    refresh: 'true',
                    type: 'logs',
                    body: {
                        query: {
                            match: {
                                ruleName: ruleName
                            }
                        }
                    }
                }, function (errFindingRule, ruleFound) {
                    elasticSearchClient.update({
                        index: 'lucene_test',
			//refresh: 'true',
                        type: 'logs',
                        id: ruleFound.hits.hits[0]._id,
                        body: {
                            doc: {
                                criteriaList: criteriaList,
				displayName: Ccriteria.cName,
                                parameters: parametersArr,
                                condition: conditionInEs,
                                criteria_ES: criteria_type,
                                duration_value: duration,
                                duration: duration_type,
                                Subcriteria: Subcriteria,
                                severity: severity

                            },

                            query: {
                                term: {
                                    ruleName: ruleName
                                }
                            }
                        }
                    }, function (errPartialUpdate, partialUpdateSuccessful) {
                        if (errPartialUpdate) {
                            return callback(errPartialUpdate)
                        }
                        return callback(null, criteria)
                    })
                })


            }
        });

    }
    else {
        criteria['trigger.ruleName'] = {"pattern": ruleName, "type": "equals"}
        logger.info("no criteria specified")
        return callback(null, criteria)
    }

 }
}
 else if(criteria_type == 'kpi'){
        var parametersArr = []
        var conditionInEs ;
        var setArr = []
        var vari='*'+ruleName+'*'
       if( jsonData.length != 0){
        for (var i = 0; i < jsonData.length; i++) {
                var Ccriteria = jsonData[i];
                logger.info(Ccriteria)
                                    logger.info(Ccriteria)
                    var PRODUCT_NAME = Ccriteria.cProduct
                    var VENDOR_NAME  = Ccriteria.cVendor;
                    var LOCATION     = Ccriteria.cLocation;
/*
                    if (Ccriteria.cType == 'gt') {
                            var symbol = '>'
                     }
                    else if (Ccriteria.cType == 'lt') {
                            var symbol = '<'
                     }
                    else if (Ccriteria.cType == 'neq') {
                            var symbol = '!='
                    }else{

                            var symbol = '=='
                    }
*/
                    var symbol = '=='
                    if(i == 0){
                        //conditionInEs = "myhash['"+PRODUCT_NAME+"']"+"['" +VENDOR_NAME+"']"+"['" +LOCATION+"']"+"['" +Ccriteria.cName+"']"+ " " + symbol + " '"+Ccriteria.cType+"'"
                        conditionInEs = "myhash['"+PRODUCT_NAME+"']"+"['" +VENDOR_NAME+"']"+"['" +LOCATION+"']"+ "['"+Ccriteria.cType+"']"+"['"+Ccriteria.cName+"']"+ " " + symbol + "1"
                    }
                    else{
                        //conditionInEs +=" " + Ccriteria.criteriaOperation+ "myhash['"+PRODUCT_NAME+"']"+"['" +VENDOR_NAME+"']"+"['" +LOCATION+"']"+"['" +Ccriteria.cName+"']"+ " " + symbol + " '" + Ccriteria.cType+"'"
                        conditionInEs +=" " + Ccriteria.criteriaOperation+ "myhash['"+PRODUCT_NAME+"']"+"['" +VENDOR_NAME+"']"+"['" +LOCATION+"']"+ "['"+Ccriteria.cType+"']"+"['" +Ccriteria.cName+"']"+ " " + symbol + "1"
                    }
                parametersArr.push(Ccriteria.cName)
                logger.info("the condition inserted to elastic search is")
            }


    if (originalCriteria.length != 0) {

        var criteria = {}
        criteria['trigger.ruleName'] = {"pattern": ruleName, "type": "equals"}
        var scriptToAddCriteria = {"inline": "ctx._source.criteriaList = " + "\"" + criteriaList + "\"" + ";"}
        var theScript = {
            "inline": "ctx._source.condition = " + "\"" + conditionInEs + "\"" + ";" + "ctx._source.remove(\"criteriaList\");"

        }
        // the below is a work around to avoid "tried to parse field as object but found a concrete value" which shows up  when using updateByQuery to modify criteria field
        // changed mapping to resolve issue - has not worked
        elasticSearchClient.updateByQuery({
            index: 'lucene_test',
            //refresh: 'true',
            type: 'logs',
            body: {
                script: theScript,
                query: {
                    term: {
                        ruleName: ruleName,

                    }
                }
            }
        }, function (error, response) {
            if (error) {
                logger.error("error with pushing data to elastic search " + error)
                return callback(error)
            } else {
                elasticSearchClient.search({
                    index: 'lucene_test',
                  //  refresh: 'true',
                    type: 'logs',
                    body: {
                        query: {
                            match: {
                                ruleName: ruleName
                            }
                        }
                    }
                }, function (errFindingRule, ruleFound) {
                    elasticSearchClient.update({
                        index: 'lucene_test',
                       // refresh: 'true',
                        type: 'logs',
                        id: ruleFound.hits.hits[0]._id,
                        body: {
                            doc: {
                                criteriaList: criteriaList,
                                parameters: parametersArr,
                                condition: conditionInEs,
                                criteria_ES: criteria_type,
                                Subcriteria: Subcriteria,
                                severity: severity

                            },

                            query: {
                                term: {
                                    ruleName: ruleName
                                }
                            }
                        }
                    }, function (errPartialUpdate, partialUpdateSuccessful) {
                        if (errPartialUpdate) {
                            return callback(errPartialUpdate)
                        }
                        return callback(null, criteria)
                    })
                })


            }
        });
}
}
}



}


function editRules(req, res, next) {
    logger.info("edit rules API called with the following params")
    logger.info(req.body)
    var certificate = req.certificate
    var key = req.key
    var elasticSearchClient = req.elasticSearchClient
    //when a crontab rule is edited

    logger.info("edit req.body")
    logger.info(JSON.stringify(req.body))
    if(typeof req.body.rule.trigger.type == 'undefined'){ 
		req.body.rule.trigger.type= 'elasticsearch.count_event'
	}
     if (req.body.rule.trigger.type == 'core.st2.CronTimer' && req.body.rule.pack == 'core') {

        var ruleName = userName + '&' + req.body.rule.name
        req.body.rule.name = ruleName


        var id = req.body.rule.id
        //req.body.rule.id
        console.log(id)
        var options = {
            url: 'https://10.225.253.132/api/v1/rules/' + id,
            agentOptions: {
                cert: certificate,
                key: key,
                rejectUnauthorized: false,

            }
        }

        request.delete(options, function (error, response, body) {
            if (error) {
                logger.error("error encountered while using request module:" + error)
                return next(error)
            }
            client.srem(userName + '_rules', id, function (err, data) {
                if (err) {
                    return next(err)
                }
                console.log(data)
                addRuleToMongo(certificate, key, req.body.rule, req.body.UserName, function (err, data) {
                    logger.info("trigger type is cronSt2Timer trigger,  elastic search insertion is not required ")
                    if (err) {
                        return next(err)
                    }
                    res.send(data)
                })


            })


        })
    }
    //editing a regular rule
    else {
        var id = req.body.rule.id //ref or id
        var originalRuleName = req.body.rule.ref.split('.')[1]
        var userName = req.body.UserName
	if(req.body.rule.hasOwnProperty('severity')){
                var severity = req.body.rule.severity
        }
        else{
                 var severity = "Minor"
        }

        var ruleName = userName  + '&' + req.body.rule.name
        var criteriaEditedByUser = req.body.rule.criteriaList
        var criteria_type = req.body.rule.criteria
        console.log("the criteria sent is")
        console.log(criteriaEditedByUser)
	logger.info("rule body :" +JSON.stringify(req.body.rule))
	if(req.body.rule.hasOwnProperty('duration_value')){
                var duration = req.body.rule.duration_value
        }
        else{
                 var duration = 0
        }
	if(req.body.rule.hasOwnProperty('duration')){
                var duration_type = req.body.rule.duration
        }
        else{
                 var duration_type = 0
        }
        if(req.body.rule.hasOwnProperty('type')){
                var Subcriteria = req.body.rule.type
        }
        else{
                var Subcriteria = 0
        }
        var parameters = req.body.rule.action.parameters


        var paramObj = {}
        //if(parameters.length > 0){
 	        Object.keys(parameters).forEach(function (key) {
        	    var getVal = parameters[key]
	             paramObj[key] = getVal.val
		    })
	//}
        logger.info("the paramObj is")
        logger.info(paramObj)
        req.body.rule.action.parameters = paramObj
        req.body.rule.criteria = {"trigger.results.ruleName": {"pattern": ruleName, "type": "equals"}}
        req.body.rule.name = ruleName
	var mongoRule={}
   	mongoRule.id=id
   	mongoRule.name=ruleName
   	mongoRule.description=req.body.rule.description
        mongoRule.enabled=req.body.rule.enabled
        mongoRule.criteria={"trigger.results.ruleName": {"pattern": ruleName, "type": "equals"}}
        mongoRule.pack=req.body.rule.pack
        mongoRule.trigger=req.body.rule.trigger
	req.body.rule.action.ref=actionArr[req.body.rule.action.ref]
        mongoRule.action=req.body.rule.action
	logger.info("mofified rule "+JSON.stringify(mongoRule))
        //mongoRule.action.parameters=paramObj

        var options = {
            url: 'https://10.225.253.132/api/v1/rules/' + id,
            method: "PUT",
            json: mongoRule,
            agentOptions: {
                cert: certificate,
                key: key,
                rejectUnauthorized: false

            }
        }
        request.put(options,
            function (error, response, body) {
                if (error) {
                    return next(error)
                }
                logger.info("editing data pertaining to " + ruleName + " in elastic search")
                editCriteriaInES(elasticSearchClient,criteria_type, originalRuleName,duration,duration_type,Subcriteria, criteriaEditedByUser,severity, function (errUpdatingData, updateSuccessful) {
                    if (errUpdatingData) {
                        return next(errUpdatingData)
                    }
                    var id = body.id
                    var setMembers = id;
                    //logger.info("deleting the rule id " + rule.id + " from  the user's set in redis")
                    logger.info("adding the rule id " + id + " from  the user's set in redis")
/*
			client.srem(userName + '_rules', req.body.rule.id, function (err, data) {
                    		if (err) {
                        		return next(err)
                    		}
			})
*/
		  if (typeof body.id !== 'undefined')
                    {
			client.SADD(userName + '_rules', setMembers, function (err, data) {
                        if (err) {
                            return next(err)
                        }

                        res.json({"status": 1})
                        // var splitStringByAmpersand = body.name.split('&')
                        //  var PLVUserName = splitStringByAmpersand[0].split('_') // SMSC_p-v-l-l
                        //  var ruleNameTobeDisplayed = splitStringByAmpersand[1] // ruleName
                        //  var username = PLVUserName[0]
                        //  var PLVString = PLVUserName[1]
                        //  var PLV = PLVString.split('-')
                        //  var PRODUCT_NAME = PLV[0]
                        //  var VENDOR = PLV[1]
                        //  var LOCATION = PLV.slice(2, PLV.length)
                        //  body.PRODUCT_NAME = PRODUCT_NAME
                        //  body.VENDOR = VENDOR
                        //  body.LOCATION = LOCATION
                        //  body.name = ruleNameTobeDisplayed
                        // res.json({"status": 1, "data": body})
                    })
		}else{
			logger.error("mongo error please check the json body!!")	
                        res.json({"status": 1})
		}
		
                })


            })
    }


}


function deleteRules(req, res, next) {
    //https://10.225.253.132/api/v1/rules/59800c8640c0a003d7456d68
    logger.info("deleteRules API called")
    logger.info(req.body)
    var certificate = req.certificate
    var key = req.key
    var id = req.body.id
    var elasticSearchClient = req.elasticSearchClient
    var userName = req.body.UserName
    logger.info("post params sent is id and userName:" + req.body.id + ", " + req.body.UserName)
    ruleDetails(id, certificate, key, function (ruleErr, ruleFound) {
        if (ruleErr) {
            return next(ruleErr)
        }
        var obj = JSON.parse(ruleFound)
        var ruleName = obj.name	
        var vari= '*'+ruleName+'*'
        client.delwild(vari, function(error, numberDeletedKeys) {
	     if (error) {
                return next(error)
            }
            console.log(numberDeletedKeys);
        })

        logger.info("deleting the rule with the name :" + ruleName)
        var options = {
            url: 'https://10.225.253.132/api/v1/rules/' + id,
            agentOptions: {
                cert: certificate,
                key: key,
                rejectUnauthorized: false,

            }
        }

        request.delete(options,
            function (error, response, body) {
                if (error) {
                    logger.error("error encountered while using request module:" + error)
                    return next(error)
                }
                logger.info("removing rule id from redis set")
                logger.info("removing " + id + " " + "from " + userName + "_rules")
                //delete refID from the hash as well
                client.srem(userName + '_rules', id, function (err, data) {
                    if (err) {
                        return next(err)
                    }
                    //delete name from elastic search
                    elasticSearchClient.deleteByQuery({
                        index: 'lucene_test',
			refresh: 'true',
                        type: 'logs',
                        body: {
                            query: {
                                match: {
                                    ruleName: ruleName
                                }
                            }
                        }
                    }, function (errWhileDeleting, dataDeleted) {
                        if (errWhileDeleting) {
                            logger.error(errWhileDeleting)
                            return next(errWhileDeleting)
                        }
                        logger.info("removing" + "" + ruleName + " " + "from elastic search's lucene_index")
                        res.json({"status": 1})

                    })
                    //res.json({"status": 1})

                })


            })
    })
}


function getRuleDetails(req, res, next) {
    logger.info("getRuleDetails API called")
    var certificate = req.certificate
    var key = req.key
    var refID = req.query.refID
    logger.info("query params sent is:" + req.query)
    var options = {
        url: 'https://10.225.253.132/api/v1/rules/' + refID,
        agentOptions: {
            cert: certificate,
            key: key,
            rejectUnauthorized: false,

        }
    }
    request.get(options,
        function (error, response, body) {
            if (error) {
                logger.error("error encountered while using request module:" + error)
                return next(error)
            }
            res.send(body)

        })
}


function getActionList(req, res, next) {
    var certificate = req.certificate
    var key = req.key
    var elasticSearchClient = req.elasticSearchClient
    var criteria_type = req.body.criteria
    var criteria_type_file = req.body.Subcriteria
    var PRODUCT_NAME = req.body.PRODUCT_NAME
    var VENDOR_NAME = req.body.VENDOR_NAME
    var LOCATION = req.body.LOCATION
    var IP_ADDRESS = req.body.IP_ADDRESS
    logger.info("req.body"+JSON.stringify(req.body))
    logger.info("criteria_type_file"+criteria_type_file)
    if((criteria_type_file == "" &&  criteria_type != 'generic')|| PRODUCT_NAME == "" ||VENDOR_NAME == "" ||LOCATION == "" ||(IP_ADDRESS == "" && criteria_type != 'generic'))
	{
	   res.json({"status":0,"msg":"necessary input data missing!"})
	}
    else{

    var options = {
        url: ' https://10.225.253.132/api/v1/actions',
        agentOptions: {
            cert: certificate,
            key: key,
            rejectUnauthorized: false,

        }
    }
    request.get(options,
        function (error, response, body) {
            if (error) {
                // logger.error(error)
                return next(error)
            }

	   if(criteria_type == 'generic'){	
            var body = JSON.parse(body)
            var actionRefList = []
            var arrayOfActions= config.get('stackStorm.actionArray')
            var criteriaType = ['equals', 'neq', 'lt', 'gt']
            elasticSearchClient.search({
                index: "stackstorm_criteria",
                body: {
                    size: 100,
                    query: {
                        match_all: {}
                    }
                }
            }, function (err, data) {
                if (err) {
                    return next(err)
                }
                var elasticSearchResults = []
                data.hits.hits.forEach(function (key) {
                    elasticSearchResults.push(key._source.criteria_type)
                })
                res.json({
                    "actionName":  arrayOfActions,
                    "criteriaType": criteriaType,
                    "criteriaKey": elasticSearchResults
                })
            })
	}
	else if(criteria_type == 'monitoring' && (criteria_type_file == 'File' || criteria_type_file == 'HTTP'|| criteria_type_file == 'Counter'))
	{
	    var body = JSON.parse(body)
            var actionRefList = []
            var arrayOfActions= config.get('stackStorm.actionArrayMonitoring')
            //var criteriaType = ['average', 'count']
            var criteriaType = ['count']
	    if(criteria_type_file == 'File' ){
		  var index_to_search ='log_monitoring'
		}
	    else if(criteria_type_file == 'HTTP'){
		  var index_to_search ='log_monitoring_http'
		}
	    else if(criteria_type_file == 'Counter'){		
		  var index_to_search ='log_monitoring_counters'
                  criteriaType = []
		}
            elasticSearchClient.search({
                index: index_to_search,
		type:  "logs",
                body: {
                    size: 100,
                    query: {
			 bool: {
      			should: [
				        { match: { PRODUCT_NAME: PRODUCT_NAME }},
				        { match: { VENDOR_NAME: VENDOR_NAME }},
				        { match: { IP_ADDRESS: IP_ADDRESS }},
				        { match: { LOCATION: LOCATION  }}
				   ]
			        }
                    }
                }
            }, function (err, data) {
                if (err) {
                    return next(err)
                }
                var elasticSearchResults = []
                data.hits.hits.forEach(function (key) {
                    elasticSearchResults.push(key._source.displayName)
                })
                res.json({
                    "actionName":  arrayOfActions,
                    "criteriaType": criteriaType,
                    "criteriaKey": elasticSearchResults
                })
            })

	}
   else if(criteria_type == 'audittrail')
        {
            var body = JSON.parse(body)
            var actionRefList = []
            var arrayOfActions= config.get('stackStorm.actionArrayMonitoring')
            var index_to_search ='user_audit_configure'
            if(criteria_type_file == 'accessdetail' ){
                  var TYPE ='Access Details'
            	  var criteriaType = ['LogIn', 'LogOut']
            	  var criteriaOperations = ['Source IP', 'User']
                }
            else if(criteria_type_file == 'configchange'){
                  var TYPE ='Config Change'
                  var criteriaType = []
                  var criteriaOperations = []
                }
            elasticSearchClient.search({
                index: index_to_search,
                type:  "logs",
                body: {
                    size: 100,
                    query: {
                         bool: {
			"must":
                                        [
                                                {"term": {"PRODUCT_NAME": PRODUCT_NAME}},
                                                {"term": {"VENDOR_NAME": VENDOR_NAME}},
                                                {"term": {"IP.keyword": IP_ADDRESS}},
                                                {"term": {"LOCATION": LOCATION}},
                                                {"term": {"TYPE": TYPE}}
                                        ]
/*
                        should: [
                                        { match: { PRODUCT_NAME: PRODUCT_NAME }},
                                        { match: { VENDOR_NAME: VENDOR_NAME }},
                                        { match: { IP_ADDRESS: IP_ADDRESS }},
                                        { match: { LOCATION: LOCATION  }},
                                        { match: { TYPE: TYPE  }}
                                   ]
*/
                                }
                    }
                }
            }, function (err, data) {
                if (err) {
                    return next(err)
                }
                var elasticSearchResults = []
                data.hits.hits.forEach(function (key) {
		    logger.info("data.hits.hits.forEach "+PRODUCT_NAME+' '+VENDOR_NAME+' '+IP_ADDRESS+' '+LOCATION+' '+TYPE+' '+key._source.displayName)
                    elasticSearchResults.push(key._source.displayName)
                })
                res.json({
                    "actionName":  arrayOfActions,
                    "criteriaType": criteriaType,
                    "criteriaOperations": criteriaOperations,
                    "criteriaKey": elasticSearchResults
                })
            })

        }
  else if(criteria_type == 'kpi')
        {
            var body = JSON.parse(body)
            var actionRefList = []
            var arrayOfActions= config.get('stackStorm.actionArrayKpi')
            var index_to_search ='kpi_reports'
            var criteriaType = ['SOFT_LIMIT','HARD_LIMIT']
            elasticSearchClient.search({
                index: index_to_search,
                type:  "logs",
                body: {
                    size: 100,
                    query: {
                         bool: {
				"must":
                                        [
                                                {"term": {"PRODUCT_NAME": PRODUCT_NAME}},
                                                {"term": {"VENDOR_NAME": VENDOR_NAME}},
                                                {"term": {"LOCATION": LOCATION}},
                                                {"term": {"TYPE": criteria_type_file}}
                                        ]

/*
                        should: [
                                        { match: { PRODUCT_NAME: PRODUCT_NAME }},
                                        { match: { VENDOR_NAME: VENDOR_NAME }},
                                        { match: { IP_ADDRESS: IP_ADDRESS }},
                                        { match: { LOCATION: LOCATION  }},
                                        { match: { TYPE: criteria_type_file  }}
                                   ]
*/
                                }
                    }
                }
            }, function (err, data) {
                if (err) {
                    return next(err)
                }
                var elasticSearchResults = []
                data.hits.hits.forEach(function (key) {
		    logger.info("data.hits.hits.forEach "+PRODUCT_NAME+' '+VENDOR_NAME+' '+LOCATION+' '+criteria_type_file+' '+key._source.KPI_NAME)
		   if (key._source.KPI_NAME != undefined){
                    elasticSearchResults.push(key._source.KPI_NAME)
			}
                })
                res.json({
                    "actionName":  arrayOfActions,
                    "criteriaType": criteriaType,
                    "criteriaKey": elasticSearchResults
                })
            })

        }

//end request

        })
}
}


function history(req, res, next) {
    logger.info("history API called")
    var db = req.db
    var userName = req.body.UserName
    var skip_size = parseInt(req.body.pageSize) * parseInt(req.body.pageNumber - 1)
    //logger.info(db)
    logger.info(JSON.stringify(req.body))
    db.collection('action_execution_d_b').count({"rule.name":{$regex: userName}},function(error,documents){
	   if(error){
		   return next(error)
		}
	   else{
			var numberOfDocs = documents
		}		
		
    db.collection('action_execution_d_b').aggregate([{$match: {"rule.name": {$regex: userName}}},
        {
            "$project": {
                status: "$status", start_timestamp: {$add: [new Date(0), {"$divide": ["$start_timestamp", 1000]}]},
                rule: "$rule", "_id": 1, "web_url": 1
            }
        }, {$sort: {start_timestamp: -1}}, {$skip: skip_size}, {$limit: parseInt(req.body.pageSize)}], function (err, docs) {

        if (err) {
            return next(err)
        }
        if (docs.length == 0) {
            logger.info("there are no documents returned")
            res.json(docs)
        }
        else {
            var ruleArray = []
            //var numberOfDocs = docs.length
            logger.info("number of documents returned is"+numberOfDocs)
            docs.forEach(function (doc) {


                if(doc.rule.trigger.type == 'core.st2.CronTimer'){
                   doc.ruleType = 'cron'
                }
                else{
                    doc.ruleType = 'regular'
                }
                var start_timestamp = doc.start_timestamp
                var start_timestamp_date = moment(start_timestamp)
                var dateComponentForStartTimeStamp = start_timestamp_date.utc().format('YYYY-MM-DD');
                var timeComponentForStartTimeStamp = start_timestamp_date.local().format('HH:mm:ss');
                doc.start_timestamp = dateComponentForStartTimeStamp + " " + timeComponentForStartTimeStamp
                //doc.start_timestamp = start_timestamp.toLocaleDateString()+' '+start_timestamp.toTimeString().substring(0, start_timestamp.toTimeString().indexOf("GMT"));
                var splitStringByAmpersand = doc.rule.name.split('&')
                var userName = splitStringByAmpersand[0] // SMSC_p-v-l-l
                //var PLVUserName = splitStringByAmpersand[0].split('_') // SMSC_p-v-l-l
                var ruleNameTobeDisplayed = splitStringByAmpersand[1] // ruleName
                doc.rule.name = ruleNameTobeDisplayed
                ruleArray.push(doc)
                if (docs.length == ruleArray.length) {
                    //res.json({"status": 1, "data": ruleArray})
                    res.json({"count":numberOfDocs,"data":ruleArray})
                   // res.json(ruleArray)
                }

            })
           }

    	})
    })



}


function historyDetails(req, res, next) {

    var db = req.db
    /*
     db.action_execution_d_b.aggregate([{ $match:{ web_url: {$regex: '5982ac9140c0a0054500f942'} } },
     {"$project":{_status:"$status",_start_timestamp:{$add:[new Date(0),"$start_timestamp"]},
     _web_url:"$web_url",_log:"$log",_result:"$result",_parameters:"$parameters",_rule:"$rule","_id":0,
     execution_time:{$ceil:{$multiply:[{$subtract:["$end_timestamp","$start_timestamp"]},0.000001]}}}}])
     */
    db.collection('action_execution_d_b').aggregate([{$match: {_id: new ObjectId(req.query.id)}}, {
        $project: {
            _status: "$status",
            _start_timestamp: {$add: [new Date(0), {"$divide": ["$start_timestamp", 1000]}]},
            _end_timestamp: {$add: [new Date(0), {"$divide": ["$end_timestamp", 1000]}]},
            _web_url: "$web_url",
            _log: "$log",
            _result: "$result",
            _parameters: "$parameters",
            _rule: "$rule",
            _id: 1,
            _input:"$trigger_instance.payload.results.parameters",
            _input_time:{$ifNull:["$trigger_instance.payload.results.time","$trigger_instance.payload.executed_at"]},
            execution_time: {$ceil: {$multiply: [{$subtract: ["$end_timestamp", "$start_timestamp"]}, 0.000001]}}
        }
    }])

        .toArray(function (err, docs) {
            console.log("the docs rae")
            console.log(docs)
            if (err) {
                return next(err)
            }
            if (docs.length == 0) {
                res.send(docs)
            }
            else {


                var ruleArray = []


                docs.forEach(function (doc) {

                    // var ruleType = elasticsearch.count_event

                    var start_timestamp = doc._start_timestamp
                    var start_timestamp_date = moment(start_timestamp)
                    var dateComponentForStartTimeStamp = start_timestamp_date.utc().format('YYYY-MM-DD');
                    var timeComponentForStartTimeStamp = start_timestamp_date.local().format('HH:mm:ss');
                    doc._start_timestamp = dateComponentForStartTimeStamp + " " + timeComponentForStartTimeStamp


                    var end_timestamp = doc._end_timestamp
                    var end_timestamp_date = moment(end_timestamp)
                    var dateComponentForEndTimeStamp = end_timestamp_date.utc().format('YYYY-MM-DD')
                    var timeComponentForEndTimeStamp = end_timestamp_date.local().format('HH:mm:ss');
                    doc._end_timestamp = dateComponentForEndTimeStamp + " " + timeComponentForEndTimeStamp
                    var splitStringByAmpersand = doc._rule.name.split('&')
                    var userName = splitStringByAmpersand[0] // SMSC_p-v-l-l
                    var ruleNameTobeDisplayed = splitStringByAmpersand[1] // ruleName
                    doc.name = ruleNameTobeDisplayed
                    ruleArray.push(doc)
                    if (docs.length == ruleArray.length) {
                        // res.json({"status": 1, "data": ruleArray})
                        res.send(ruleArray)
                    }

                })
            }
        })

}


function actionDetails(req, res, next) {
    //https://10.225.253.132/api/v1/actions/linux.mv
    var certificate = req.certificate
    var key = req.key
    var refID = req.query.refID
//                "actionArrayMonitoring":["core.sendmail"],
	logger.info("refID" +actionArr[refID])

    //https://10.225.253.132/api/v1/actions/views/parameters/595cfe75421aa97f086a378e
    var options = {
        //https://10.225.253.132/api/v1/actions/
        //https://10.225.253.132/api/v1/actions/views/overview
        url: 'https://10.225.253.132/api/v1/actions/views/overview/' + actionArr[refID],
        agentOptions: {
            cert: certificate,
            key: key,
            rejectUnauthorized: false,

        }
    }
    logger.info("options "+options.url)
    request.get(options,
        function (error, response, body) {
            if (error) {
                logger.error(error)
                return next(error)
            }
            //logger.info("response "+ JSON.stringify(body))
            res.json({"status": 1, "data": JSON.parse(body)})

        })
}


exports.getAllRules = getAllRules
exports.getRuleDetails = getRuleDetails
exports.userRules = userRules

exports.addRule = addRule
exports.editRules = editRules
exports.deleteRules = deleteRules
exports.actionList = getActionList
exports.actionDetails = actionDetails


exports.history = history
exports.historyDetails = historyDetails
