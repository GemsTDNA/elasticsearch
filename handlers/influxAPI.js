 /**
 * Created by Radhika on 6/21/2017.
 */

var join = require('join-by-keys')
var flatten = require('array-flatten')
const Influx = require('influx');
const influx = new Influx.InfluxDB({
    host: '10.225.253.132',
    database: 'netdata',
    schema: [
        {
            measurement: 'kpi_alarms',
            fields: {
                VALUE: Influx.FieldType.FLOAT

            },
            tags: [
                'PRODUCT_NAME', 'LOCATION', 'VENDOR_NAME', 'ALARM_NAME', 'STATUS', 'level', 'IP_ADDRESS'
            ],
            timestamp: Date


        }
    ]

});
var logger = require('../libs/logger').influxLogger
var moment = require('moment')


function insertAlarm(req, res, next) {
    var message = JSON.parse(req.body.message)
    logger.info("the parsed message is" + JSON.stringify(message))
    logger.info("the time is" + req.body.time)
    logger.info("the level is" + req.body.level)
    var time = req.body.time
    if (message.VALUE == undefined) {
        var err = new Error("req.body.value is undefined")
        return next(err)
    }
    var epoch = moment.parseZone(req.body.time).unix()
    var level = req.body.level
    influx.writePoints([
        {
            measurement: 'kpi_alarms',
            tags: {
                PRODUCT_NAME: message.PRODUCT_NAME,
                LOCATION: message.LOCATION,
                VENDOR_NAME: message.VENDOR_NAME,
                ALARM_NAME: message.ALARM_NAME,
                STATUS: message.STATUS,
                level: req.body.level,
                IP_ADDRESS: message.IP_ADDRESS

            },
            fields: {VALUE: message.VALUE}, // fields should not be empty

            timestamp: epoch + "000000000"
        }
    ]).then(function () {
        logger.info("data inserted to influx")
        res.json({"status": 1})
    }).catch(function (err) {
        logger.error(err)
        return next(err)
    })

}


/*
 var kvArray = [{key: 1, value: 10},
 {key: 2, value: 20},
 {key: 3, value: 30}];

 var reformattedArray = kvArray.map(function(obj) {
 var rObj = {};
 rObj[obj.key] = obj.value;
 return rObj;
 });
 */
function inventoryData(req, res, next) {
    var prod = req.body.PRODUCT_NAME
    var vendor = req.body.VENDOR_NAME
    var loc = req.body.LOCATION

    influx.query(['select mean(percentage_usage) as MEM from  Memory_Summary  where time > now() - 1m and PRODUCT_NAME=\'' + prod + '\' and LOCATION=\'' + loc + '\' and VENDOR_NAME=\'' + vendor + '\' group by IP_ADDRESS',
        'select mean(percentage_usage) as DISK from Disk_Summary where time > now() - 1m and DETAIL1=\'_home\' and PRODUCT_NAME=\'' + prod + '\' and LOCATION= \'' + loc + '\' and VENDOR_NAME=\'' + vendor + '\' group by IP_ADDRESS',
        'select sum(VALUE) as CPU from system where time > now() - 20s and DETAIL1\'cpu\'= and DETAIL2!=\'idle\' and PRODUCT_NAME=\'' + prod + '\' and LOCATION=\'' + loc + '\' and VENDOR_NAME=\'' + vendor + '\'  group by IP_ADDRESS,time(10s) fill(none)']
    )
        .then(function (results) {
            var jsonArr = []
            var flatArray = flatten(results)
            var array = join(flatArray, ['IP_ADDRESS'])
            var resultsArr = []
            array.forEach(function (obj) {
                var rObj = {}
                rObj.IP_ADDRESS = obj.IP_ADDRESS
                if (obj['MEM'] != undefined) {
                    rObj.MEM = obj['MEM'][0].toFixed(2)
                }
                if (obj['CPU'] != undefined) {
                    rObj.CPU = obj['CPU'][0].toFixed(2)
                }
                if (obj['DISK'] != undefined) {
                    rObj.DISK = obj['DISK'][0].toFixed(2);
                }
                resultsArr.push(rObj)

            })
            res.send(resultsArr)
        })
        .catch(function (err) {
            return next(err)
        })
}


exports.insertAlarm = insertAlarm
exports.inventoryData = inventoryData
//exports.network = network
//exports.getAllCountersForBreaches = getAllCountersForBreaches
