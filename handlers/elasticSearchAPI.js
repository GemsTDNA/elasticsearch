var logger = require('../libs/logger').logger;
var request = require('request')
var moment = require('moment');
var neo4j = require('neo4j');
const Influx = require('influx');
const influx = new Influx.InfluxDB({
    host: '10.225.253.132',
    database: 'netdata'
});

var db = new neo4j.GraphDatabase('http://neo4j:teledna@172.16.23.27:7474');


function insertKPIRules(req, res, next) {
    logger.info("all is well with elastic search when insertKPI is called")
    logger.info("the req body for insertKPI is")
    logger.info(req.body)
    var date = new Date()
    req.body.date = date
    var elasticSearchClient = req.elasticSearchClient
    var numberOfDocuments = req.body.length
    var body = req.body
    var counter = 0
    // bulk  API in elastic search is used to do multiple operations such as insert , update , delete via one API
    body.forEach(function (reqBody) {
        elasticSearchClient.index({
            index: 'test-kpi-alarms',
            type: 'logs',
            body: reqBody
        }, function (error, response) {
            counter++
            if (error) {
                logger.error("error with pushing data to elastic search " + error)

            }
            //logger.info("the response from elastic search index query is " + JSON.stringify(response));
            if (counter == numberOfDocuments) {
                res.json({"status": 1})
            }

        });
    })
}

// this function was written under the assumption that there will be one single format for all the API's in the file
// in case the formats differ , then multiple functions have to be written based on the data formats
function parseRequestData(reqBody, callback) {
    var filterArray = []
    if (reqBody.prodName != undefined) {
        var obj = {"term": {"PRODUCT_NAME": reqBody.prodName}}
        filterArray.push(obj)
    }
    if (reqBody.location != undefined) {
        var obj = {"term": {"LOCATION": reqBody.location}}
        filterArray.push(obj)
    }
    if (reqBody.vendor != undefined) {
        var obj = {"term": {"vendor": reqBody.vendor}}
        filterArray.push(obj)
    }
    if (reqBody.alarm != undefined) {
        var obj = {"term": {"ALARM_NAME.keyword": reqBody.alarm}} //  please change when using live data to ALARM_NAME
        filterArray.push(obj)
    }
    if (reqBody.tag != undefined) {
        var obj = {"term": {"tag": reqBody.tag}}
        filterArray.push(obj)
    }
    if (reqBody.value != undefined) {
        var obj = {"term": {"value": reqBody.value}}
        filterArray.push(obj)
    }
    if (reqBody.level != undefined) {
        var obj = {"term": {"level": reqBody.level.toUpperCase()}}  // level:INFO , level:CRITICAL
        filterArray.push(obj)
    }
    if (reqBody.used != undefined) {
        var obj = {"term": {"used": reqBody.used}}
        filterArray.push(obj)
    }
    if (reqBody.available != undefined) {
        var obj = {"term": {"available": reqBody.available}}
        filterArray.push(obj)
    }
    if (reqBody.fileName != undefined) {
        var appendPath = "/home/GEMS/mugsy/"
        // console.log(appendPath+reqBody.fileName)
        var obj = {"wildcard": {"fileName": '*' + reqBody.fileName + '*'}}
        filterArray.push(obj)
    }
    if (reqBody.from != "" && reqBody.from != undefined) { //yyyy/MM/dd HH:mm:ss
        var from = reqBody.from + ":00"
        var to = reqBody.to + ":00"
        var obj = {"range": {"time": {"gte": from, "lte": to, "format": "yyyy/MM/dd HH:mm:ss"}}}
        filterArray.push(obj)
    }
    return callback(null, filterArray)

}


// since the format has changed for KPI breaches, separate function is written
// if the above format for parseRequestData is not required , it will be removed
function parseRequestDataForKPI(reqBody, callback) {
    var filterArray = []
    if (reqBody.PRODUCT_NAME != undefined) {
        var obj = {"term": {"PRODUCT_NAME": reqBody.PRODUCT_NAME}}
        filterArray.push(obj)
    }
    if (reqBody.LOCATION != undefined) {
        var obj = {"term": {"LOCATION": reqBody.LOCATION}}
        filterArray.push(obj)
    }
    if (reqBody.VENDOR_NAME != undefined) {
        var obj = {"term": {"VENDOR_NAME": reqBody.VENDOR_NAME}}
        filterArray.push(obj)
    }

    if (reqBody.from != "" && reqBody.from != undefined) { //yyyy/MM/dd HH:mm:ss
        var from = reqBody.from + ":00"
        var to = reqBody.to + ":00"
        var obj = {"range": {"date": {"gte": from, "lte": to, "format": "yyyy/MM/dd HH:mm:ss"}}}  // stored as date in this mappings , please cross check as it could be stored as time elsewhere
        filterArray.push(obj)
    }
    return callback(null, filterArray)

}

// TODO : remove filter from query and use must instead as filter caches queries and any new data inserted to elastic search won't be read since data from the cache is loaded each time
function search(req, res, next) {
    var elasticSearchClient = req.elasticSearchClient
    logger.info("all is well with elastic search when search is called")
    logger.info("the req body for search is")
    logger.info(req.body)
    logger.info("the data sent to parseRequest function is" + JSON.stringify(req.body))
    parseRequestData(req.body, function (errWithParsingData, parsedData) {
        logger.info("the data obtained from parseRequest function is" + JSON.stringify(parsedData))
        elasticSearchClient.search({
            index: 'alertsfromsocketio',
            body: {
                size: 1000,
                query: {
                    bool: {
                        must: parsedData
                    }
                }
            }
        }, function (err, data) {
            if (err) {
                logger.error("error querying elasticSearch" + JSON.stringify(err))
                return next(err)
            }
            if (data) {
                logger.info("elastic search returns results")
                res.send(data.hits.hits)
                // res.json({"status":1,"data":data.hits.hits})
            }
        })
    })
}


function parseDataForKPIBreaches(products, callback) {
    var filterArray = []
    products.forEach(function (eachProduct) {
        var obj = {"term": {"PRODUCT_NAME": eachProduct}}
        filterArray.push(obj)
    })
    return callback(null, filterArray)
}


function parseDataTillDataIsPopulatedInES(products, callback) { // for search breaches only
    var filterArray = []
    products.forEach(function (eachProduct) {
        var obj = {"match": {"PRODUCT_NAME": eachProduct}}
        filterArray.push(obj)
    })

    return callback(null, filterArray)
}


function getKPIRules(req, res, next) {
    logger.info("all is well with elastic search when getKPIbreaches is called")
    logger.info("the req query for get kpi breaches is")
    logger.info(req.query)
    var userName = req.query.userName
    var elasticSearchClient = req.elasticSearchClient
    request.post('http://172.16.23.27:3001/getProducts', {json: {UserName: userName}}, function (error, response, body) {
        if (error) {
            logger.error(error)
            return next(error)
        }
        parseDataForKPIBreaches(body, function (err, parsedData) {
            logger.info("the parsed data received by GET KPI breaches")
            logger.info(parsedData)
            if (err) {
                logger.error(err)
                return next(err)
            }
            elasticSearchClient.search({
                index: 'test-kpi-alarms',
                type: 'logs',
                body: {
                    size: 1000,
                    query: {
                        bool: {
                            should: parsedData
                        }
                    }
                }
            }, function (err, responseFromElasticSearch) {
                if (err) {
                    logger.error("elastic search error" + JSON.stringify(err))
                    return next(err)
                } else {
                    var array = [];
                    responseFromElasticSearch.hits.hits.forEach(function (hit) {
                        var jsonObj = hit['_source'];
                        jsonObj['_id'] = hit['_id'];
                        array.push(jsonObj);
                    })
                    logger.info("elastic search returns results")
                    res.json({"status": 1, "data": array});
                }
            })
        })
    })
}

/*
 ALARM has been mapped and not ALARM_TYPE .
 Searching with ALARM_TYPE WONT work unless it is changed to ALARM_TYPE.keyword
 */


function searchKPIRules(req, res, next) {
    logger.info("All is well with elastic search when searchKPIbreaches is called")
    logger.info("the req body for searchKPIbreaches is")
    logger.info(req.body)
    logger.info("the data sent to parseRequest function is" + JSON.stringify(req.body))
    var elasticSearchClient = req.elasticSearchClient
    parseRequestDataForKPI(req.body, function (errWithParsingData, parsedData) {
        logger.info("the data obtained from parseRequest function is" + JSON.stringify(parsedData))
        elasticSearchClient.search({
            index: 'test-kpi-alarms',
            type: 'logs',
            body: {
                size: 1000,
                query: {
                    bool: {
                        must: parsedData
                    }
                }
            }
        }, function (err, data) {
            if (err) {
                logger.error("error querying elasticSearch with index test-kpi-alarms" + JSON.stringify(err))
                return next(err)
            }
            if (data) {
                logger.info("elastic search returns results");
                var resArray = [];
                data.hits.hits.forEach(function (hit) {
                    var jsonObj = hit._source;
                    jsonObj['_id'] = hit._id;
                    resArray.push(jsonObj);
                })
                res.send(resArray);
            }
        })
    })
}


function updateKPIRules(req, res, next) {
    logger.info("All is well with elastic search when updateKPI is called")
    logger.info("the req body for updateKPI  is")
    logger.info(req.body)
    var elasticSearchClient = req.elasticSearchClient
    logger.info("update KPI , req body")
    logger.info(req.body)
    var id = req.body._id;
    delete req.body._id;
    G
}

function deleteKPIRules(req, res, next) {
    var elasticSearchClient = req.elasticSearchClient
    logger.info("delete KPI , req body")
    logger.info(req.body)
    elasticSearchClient.delete({
        index: 'test-kpi-alarms',
        type: 'logs',
        id: req.body._id
    }, function (errWhileDeleting, dataDeleted) {
        if (errWhileDeleting) {
            logger.error(errWhileDeleting)
            return next(errWhileDeleting)
        }
        res.json({"status": 1})

    })


}

function getUtils(req, res) {
    logger.info("All is well with elastic search");
    logger.info("the data sent to getUtils function is" + JSON.stringify(req.body));
    request.post('http://172.16.23.27:3001/getProducts', {
        json: {
            UserName: req.query.userName
        }
    }, function (error, response, body) {
        if (error) {
            logger.error(error);
        } else {
            var query = "select * from tps_stats where "
            body.forEach(function (product) {
                query = query + " PRODUCT_NAME='" + product + "' or ";
            });
            var lastIndex = query.lastIndexOf("or")
            query = query.substr(0, lastIndex)
            logger.info("Query : " + query);
            influx.query(query + ' ORDER BY time DESC limit 1000').then(function (result) {
                logger.info('searchService UTIL sends results from influx')
                var resArray = [];
                result.forEach(function (eachResult) {
                    eachResult.time = moment.parseZone(eachResult.time).local().format("YYYY-MM-DD HH:mm:ss");
                    resArray.push(eachResult);
                })
                res.json(resArray);
            }).catch(function (err) {
                logger.error(err)
            })
        }
    })
}

function searchServiceUtil(req, res) {
    logger.info("All is well with elastic search");
    logger.info("the data sent to searchServiceUtil function is" + JSON.stringify(req.body));

    var query = "select * from tps_stats where ";

    if (req.body.fromDate != undefined) {
        var from = moment(req.body.fromDate + ":00 +05:30", "YYYY/MM/DD HH:mm:ss [Z]").utc().format("YYYY-MM-DD HH:mm:ss");
        var to = moment(req.body.toDate + ":00 +05:30", "YYYY/MM/DD HH:mm:ss [Z]").utc().format("YYYY-MM-DD HH:mm:ss");
        query = query + " time > '" + from + "' and time < '" + to + "' and ";
        delete req.body.fromDate;
        delete req.body.toDate;
    }
    var keys = Object.keys(req.body);
    keys.forEach(function (key) {
        query = query + key + " = '" + req.body[key] + "' and ";
    })

    var lastIndex = query.lastIndexOf("and")
    query = query.substr(0, lastIndex) // to remove the last 'and' in the query
    logger.info("Query : " + query);
    influx.query(query + ' ORDER BY time DESC limit 1000').then(function (result) {
        logger.info('searchService UTIL sends results from influx');
        var resArray = [];
        result.forEach(function (eachResult) {
            eachResult.time = moment.parseZone(eachResult.time).local().format("YYYY-MM-DD HH:mm:ss");
            resArray.push(eachResult);
        })
        res.json(resArray)
    }).catch(function (err) {
        logger.error(err);
        res.status(500);
        res.json(err);
    })
}


function insertDirectoryData(req, res, next) {

    logger.info("All is well with elastic search")
    var date = new Date()
    req.body.date = date
    var elasticSearchClient = req.elasticSearchClient
    elasticSearchClient.index({
        index: 'test-dir-index',
        type: 'product',
        body: req.body
    }, function (error, response) {
        if (error) {
            logger.error("error with pushing data to elastic search " + error)
            return next(error)

        }
        logger.info("the response from elastic search index query is " + JSON.stringify(response));
        res.json({"status": 1})
    });


}


function getDirectoryData(req, res, next) {

    var elasticSearchClient = req.elasticSearchClient
    elasticSearchClient.search({
        index: "test-dir-data",
        body: {
            from: 0, size: 1000,
            query: {
                match_all: {}
            }
        }
    }, function (error, response) {
        if (error) {
            logger.error("error with pushing data to elastic search " + error)

            //  return next (errorHandler(error,req,res,next))
            return next(error)

        }
        logger.info("sending elastic search data")
        res.json({"status": 1, "data": response.hits.hits})
    })


}

function searchConfigDiff(req, res, next) {
    var elasticSearchClient = req.elasticSearchClient
    logger.info("the data sent to parseRequest function is" + JSON.stringify(req.body))
    parseRequestDataTillCorrectMappingIsDoneInES(req.body, function (errWithParsingData, parsedData) {
        logger.info("the data obtained from parseRequest function is" + JSON.stringify(parsedData))
        elasticSearchClient.search({
            index: 'configdiff',
            type: 'logs',
            body: {
                size: 1000,
                query: {
                    bool: {
                        must: parsedData
                    }
                }
            }
        }, function (err, data) {
            if (err) {
                logger.error("error querying elasticSearch" + JSON.stringify(err))
                return next(err)
            }
            if (data) {
                logger.info("elastic search returns results")
                res.send(data.hits.hits)
            }
        })
    })
}

function getBreaches(req, res, next) {
    logger.info("all is well with elastic search when getBreaches is called")
    logger.info("get breaches, req query", req.query)
    var elasticSearchClient = req.elasticSearchClient
    request.post('http://172.16.23.27:3001/getProducts', {json: {UserName: req.query.userName}}, function (error, response, body) {
        if (error) {
            logger.error(error)
            return next(error)
        }
        parseDataTillDataIsPopulatedInES(body, function (err, parsedData) {
            logger.info("the parsed data received by GET KPI breaches")
            logger.info(parsedData)
            if (err) {
                logger.error(err)
                return next(err)
            }
            elasticSearchClient.search({
                index: 'kpi-alarms',
                type: 'logs',
                body: {
                    size: 1000,
                    query: {
                        bool: {
                            should: parsedData
                        }
                    }
                }
            }, function (err, responseFromElasticSearch) {
                if (err) {
                    logger.error("elastic search error" + JSON.stringify(err))
                    return next(err)

                } else {
                    var array = [];
                    responseFromElasticSearch.hits.hits.forEach(function (hit) {
                        var jsonObj = hit['_source'];
                        jsonObj['_id'] = hit['_id'];
                        array.push(jsonObj);
                    })
                    logger.info("elastic search returns results")
                    res.json({"status": 1, "data": array});

                }
            })
        })

    })
}

function parseRequestDataTillCorrectMappingIsDoneInES(reqBody, callback) {  // for search breaches only
    var filterArray = []
    if (reqBody.PRODUCT_NAME != undefined) {
        var obj = {"match": {"PRODUCT_NAME": reqBody.PRODUCT_NAME}}
        filterArray.push(obj)
    }
    if (reqBody.LOCATION != undefined) {
        var obj = {"match": {"LOCATION": reqBody.LOCATION}}
        filterArray.push(obj)
    }
    if (reqBody.VENDOR_NAME != undefined) {
        var obj = {"match": {"VENDOR_NAME": reqBody.VENDOR_NAME}}
        filterArray.push(obj)
    }

    if (reqBody.from != "" && reqBody.from != undefined) { //yyyy/MM/dd HH:mm:ss
        var from = reqBody.from + ":00"
        var to = reqBody.to + ":00"
        var obj = {"range": {"date": {"gte": from, "lte": to, "format": "yyyy/MM/dd HH:mm:ss"}}}  // stored as date in this mappings , please cross check as it could be stored as time elsewhere
        filterArray.push(obj)
    }
    return callback(null, filterArray)

}

function searchBreaches(req, res, next) {

    var elasticSearchClient = req.elasticSearchClient
    logger.info("searchService breaches , req.body" + JSON.stringify(req.body))
    parseRequestDataTillCorrectMappingIsDoneInES(req.body, function (errWithParsingData, parsedData) {
        logger.info("the data obtained from parseRequest function is" + JSON.stringify(parsedData))
        elasticSearchClient.search({
            index: 'kpi-alarms',
            body: {
                from: 0, size: 1000,
                query: {
                    bool: {
                        must: parsedData
                    }
                }
            }
        }, function (err, data) {
            if (err) {
                logger.error("error querying with index test util elasticSearch" + JSON.stringify(err))
                return next(err)
            }
            if (data) {
                logger.info("elastic search returns results")
                res.json({"status": 1, "data": data.hits.hits})
            }
        })


    })

}


function parseRequestDataForAuditTrail(reqBody, callback) {
    var filterArray = []
    if (reqBody.PRODUCT_NAME != undefined) {
        var obj = {"term": {"PRODUCT_NAME": reqBody.PRODUCT_NAME}}
        filterArray.push(obj)
    }
    if (reqBody.LOCATION != undefined) {
        var obj = {"term": {"LOCATION": reqBody.LOCATION}}
        filterArray.push(obj)
    }
    if (reqBody.VENDOR_NAME != undefined) {
        var obj = {"term": {"VENDOR_NAME": reqBody.VENDOR_NAME}}
        filterArray.push(obj)
    }
    if (reqBody.fileName != undefined) {
        var obj = {"wildcard": {"fileName": '*' + reqBody.fileName + '*'}}
        filterArray.push(obj)
    }
    if (reqBody.from != "" && reqBody.from != undefined) { //yyyy/MM/dd HH:mm:ss
        var from = reqBody.from + ":00"
        var to = reqBody.to + ":00"
        // for KPI , we search for date , wheras for auditTrail ,we have to search for time
        var obj = {"range": {"time": {"gte": from, "lte": to, "format": "yyyy/MM/dd HH:mm:ss"}}}  // stored as date in this mappings , please cross check as it could be stored as time elsewhere
        filterArray.push(obj)
    }
    return callback(null, filterArray)

}


function searchAuditTrail(req, res, next) {
    var elasticSearchClient = req.elasticSearchClient
    logger.info("the data sent to parseRequest function is" + JSON.stringify(req.body))
    parseRequestDataForAuditTrail(req.body, function (errWithParsingData, parsedData) {
        logger.info("the data obtained from parseRequest function is" + JSON.stringify(parsedData))
        elasticSearchClient.search({
            index: "useraudit3,configdiff",
            type: "logs",
            body: {
                size: 1000,
                query: {
                    bool: {
                        must: parsedData // [{"match":{"PRODUCT_NAME":"SMSC"}}]
                    }
                }
            }
        }, function (err, data) {
            if (err) {
                logger.error("error querying elasticSearch" + JSON.stringify(err))
                return next(err)
            }
            if (data) {
                logger.info("elastic search returns results")
                var array = [];
                // if(data.hits.total>0){
                data.hits.hits.forEach(function (hit) {
                    var jsonObj = hit['_source'];
                    jsonObj['_id'] = hit['_id'];
                    array.push(jsonObj);
                })

                logger.info("elastic search returns results")
                res.json({"status": 1, "data": array});

            }
        })
    })
}

// data in influx's kpi_alarms is inserted to elastic search as well
function kpiAlarms(req, res, next) {
    var message = JSON.parse(req.body.message)
    logger.info("the parsed message is" + JSON.stringify(message))
    logger.info("the time is" + req.body.time)
    logger.info("the level is" + req.body.level)
    message.time = moment(req.body.time, "YYYY-MM-DD HH:mm:ss [Z]").utc().format("YYYY-MM-DD HH:mm:ss");
    message.level = req.body.level
    logger.info("All is well with elastic search")
    var elasticSearchClient = req.elasticSearchClient
    elasticSearchClient.index({
        index: 'kpi_alarms',
        type: 'logs',
        body: message
    }, function (error, response) {
        if (error) {
            logger.error("error with pushing data to elastic search " + error)
            return next(error)

        }
        logger.info("the response from elastic search index query is " + JSON.stringify(response));
        res.json({"status": 1})
    });
}

function getDetailedDataForBreaches(req, res, next) {
    var ARRAY = [];
    var criticalObj = {"term": {"level": "CRITICAL"}};
    ARRAY.push(criticalObj);
    var elasticSearchClient = req.elasticSearchClient
    var quickRangesObj = {
        "Today so far": "today",
        "This week so far": "week",
        "This month so far": "month",
        "This year so far": "year"
    };

    if (req.body.fromDate != undefined) {
        var From = moment(req.body.fromDate).utc().format("YYYY-MM-DD HH:mm:ss");
        var to = moment(req.body.toDate).startOf('second').utc().format("YYYY-MM-DD HH:mm:ss");
        rangeObj = {"range": {"time": {"gte": From, "lte": to, "format": "yyyy-MM-dd HH:mm:ss"}}};
        ARRAY.push(rangeObj);
    } else if (req.body['QuickRanges'] != undefined) {
        var to = moment().startOf('second').format("YYYY-MM-DD HH:mm:ss");
        var From = moment().startOf(quickRangesObj[req.body['QuickRanges']]).format("YYYY-MM-DD HH:mm:ss");
        rangeObj = {"range": {"time": {"gte": From, "lte": to, "format": "yyyy-MM-dd HH:mm:ss"}}};
        ARRAY.push(rangeObj);
    }
    if (req.body.CATEGORY != undefined) {
        var Obj = {"term": {"CATEGORY": req.body.CATEGORY}};
        ARRAY.push(Obj);
    }
    if (req.body.ALARM_NAME != undefined) {
        var Obj = {"term": {"ALARM_NAME": req.body.ALARM_NAME}};
        ARRAY.push(Obj);
    }
    if (req.body.LOCATION != undefined) {
        var Obj = {"term": {"LOCATION": req.body.LOCATION}};
        ARRAY.push(Obj);
    }
    if (req.body.VENDOR_NAME != undefined) {
        var Obj = {"term": {"VENDOR_NAME": req.body.VENDOR_NAME}};
        ARRAY.push(Obj);
    }
    if (req.body.VENDOR_NAME != undefined) {
        var Obj = {"term": {"PRODUCT_NAME": req.body.PRODUCT_NAME}};
        ARRAY.push(Obj);
    }
    console.log(JSON.stringify(ARRAY))
    elasticSearchClient.search({
        index: 'kpi_alarms',
        type: 'logs',
        body: {
            size: 10000, "sort": [{"time": {"order": "desc"}}],
            "query": {"bool": {"must": ARRAY}}
        }
    }, function (err, data) {
        if (err) {
            return next(err)
        } else {
            console.log(JSON.stringify(data.hits.hits));
            var resArray = []
            data.hits.hits.forEach(function (hit) {
                hit._source.time = moment.parseZone(hit._source.time).local().format("YYYY-MM-DD HH:mm:ss");
                resArray.push(hit._source);
            })
            res.send(resArray);
        }
    })
}


function getDataBasedonCategory(req, res, next) {
    var ARRAY = [];
    var criticalObj = {"term": {"level": "CRITICAL"}};
    ARRAY.push(criticalObj);
    var elasticSearchClient = req.elasticSearchClient
    var quickRangesObj = {
        "Today so far": "today",
        "This week so far": "week",
        "This month so far": "month",
        "This year so far": "year"
    };

    if (req.body.fromDate != undefined) {
        var From = moment(req.body.fromDate).utc().format("YYYY-MM-DD HH:mm:ss");
        var to = moment(req.body.toDate).startOf('second').utc().format("YYYY-MM-DD HH:mm:ss");
        rangeObj = {"range": {"time": {"gte": From, "lte": to, "format": "yyyy-MM-dd HH:mm:ss"}}};
        ARRAY.push(rangeObj);
    } else if (req.body['QuickRanges'] != undefined) {
        var to = moment().startOf('second').format("YYYY-MM-DD HH:mm:ss");
        var From = moment().startOf(quickRangesObj[req.body['QuickRanges']]).format("YYYY-MM-DD HH:mm:ss");
        rangeObj = {"range": {"time": {"gte": From, "lte": to, "format": "yyyy-MM-dd HH:mm:ss"}}};
        ARRAY.push(rangeObj);
    }
    if (req.body.CATEGORY != undefined) {
        var Obj = {"term": {"CATEGORY": req.body.CATEGORY}};
        ARRAY.push(Obj);
    }
    if (req.body.ALARM_NAME != undefined) {
        var Obj = {"term": {"ALARM_NAME": req.body.ALARM_NAME}};
        ARRAY.push(Obj);
    }
    if (req.body.LOCATION != undefined) {
        var Obj = {"term": {"LOCATION": req.body.LOCATION}};
        ARRAY.push(Obj);
    }
    if (req.body.VENDOR_NAME != undefined) {
        var Obj = {"term": {"VENDOR_NAME": req.body.VENDOR_NAME}};
        ARRAY.push(Obj);
    }
    db.cypher({
        query: 'MATCH (n:Login{UserName:"' + req.body.UserName + '",Type:"User"})-[:Products]->(m)-[:Vendors]->(V)-[:Locations]->(L) return COLLECT (distinct m.UserName) as products',
    }, function (err, products) {
        if (err) {
            logger.error(err)
            return next(err)
        }
        var Obj = {"terms": {"PRODUCT_NAME": products}};
        ARRAY.push(Obj);
    })
    console.log(JSON.stringify(ARRAY))
    elasticSearchClient.search({
        index: 'kpi_alarms',
        type: 'logs',
        body: {
            size: 10000,
            "query": {"bool": {"must": ARRAY}},
            "aggs": {
                "Product": {
                    "terms": {"field": "PRODUCT_NAME", "size": 1000},
                    "aggs": {
                        "Vendor": {
                            "terms": {"field": "VENDOR_NAME", "size": 1000},
                            "aggs": {"Location": {"terms": {"field": "LOCATION", "size": 1000}}}
                        }
                    }
                }
            }
        }
    }, function (err, data) {
        if (err) {
            return next(err)
        } else {
            console.log(JSON.stringify(data.aggregations));
            var resArray = []
            data.aggregations.Product.buckets.forEach(function (ProductHit) {
                ProductHit.Vendor.buckets.forEach(function (VendorHit) {
                    VendorHit.Location.buckets.forEach(function (LocationHit) {
                        var Obj = {};
                        Obj.VENDOR_NAME = VendorHit.key;
                        Obj.PRODUCT_NAME = ProductHit.key;
                        Obj.LOCATION = LocationHit.key;
                        Obj.VALUE = LocationHit.doc_count;
                        resArray.push(Obj);
                    })
                })
            })
            res.send(resArray);
        }
    })
}


function EsForCounters(elasticSearchClient, ARRAY, callback) {
    elasticSearchClient.search({
        index: 'kpi_alarms',
        type: 'logs',
        body: {
            size: 0,
            "query": {"bool": {"must": ARRAY}},
            aggregations: {
                Network: {terms: {field: 'CATEGORY'}, aggregations: {AlarmNames: {terms: {field: 'ALARM_NAME'}}}},
                System: {terms: {field: 'VENDOR_NAME'}, aggregations: {AlarmNames: {terms: {field: 'ALARM_NAME'}}}}
            },
        }
    }, function (err, data) {
        if (err) {
            return callback(err)
        }
        console.log(JSON.stringify(data));
        var NetworkObj = {"Customer": 0, "Service": 0, "Compliance": 0};
        var SystemObj = {"TeleDNA": 0, "Acision": 0}
        var id = 8;
        var resultArray = [];
        var Network_count = 0;
        var System_count = 0;
        data.aggregations.Network.buckets.forEach(function (alarmNamesObj) {
            NetworkObj[alarmNamesObj.key] = alarmNamesObj.doc_count;
            Network_count = Network_count + alarmNamesObj.doc_count;
            var jsonObj = {
                "id": alarmNamesObj.key,
                "class": "Network",
                "parent": 1,
                "text": alarmNamesObj.key + "(" + alarmNamesObj.doc_count + ")",
                "state": {"opened": true}
            };
            resultArray.push(jsonObj);
            alarmNamesObj.AlarmNames.buckets.forEach(function (obj) {
                var jsonObj = {
                    "id": id,
                    "class": "Network",
                    "parent": alarmNamesObj.key,
                    "text": obj.key + " (" + obj.doc_count + ")",
                    "state": {"opened": true}
                };
                resultArray.push(jsonObj);
                id++;
            })
        })
        data.aggregations.System.buckets.forEach(function (alarmNamesObj) {
            SystemObj[alarmNamesObj.key] = alarmNamesObj.doc_count; // systemObj[Acision] =  3
            System_count = System_count + alarmNamesObj.doc_count;
            var jsonObj = {
                "id": alarmNamesObj.key,
                "class": "System",
                "parent": 2,
                "text": alarmNamesObj.key + " (" + alarmNamesObj.doc_count + ")",
                "state": {"opened": true}
            };
            resultArray.push(jsonObj)
            alarmNamesObj.AlarmNames.buckets.forEach(function (obj) {   // obj : { key: 'DND', doc_count: 2 }
                var jsonObj = {
                    "id": id,
                    "class": "System",
                    "parent": alarmNamesObj.key,
                    "text": obj.key + "(" + obj.doc_count + ")",
                    "state": {"opened": true}
                };
                resultArray.push(jsonObj)
                id++
            })
        })
        resultArray.push({
            "id": "1",
            "parent": "#",
            "type": "folder",
            "text": "Network" + " (" + Network_count + ")",
            "state": {"opened": false}
        });
        resultArray.push({
            "id": "2",
            "parent": "#",
            "type": "folder",
            "text": "System" + " (" + System_count + ")",
            "state": {"opened": false}
        });
        resultArray.push({"id": "3", "parent": "#", "type": "folder", "text": "Custom", "state": {"opened": false}});
        return callback(null, resultArray);
    });
}

function getAllCountersForBreaches(req, res, next) {
    var ARRAY = []
    var criticalObj = {"term": {"level": "CRITICAL"}};
    ARRAY.push(criticalObj);
    var elasticSearchClient = req.elasticSearchClient
    var quickRangesObj = {
        "Today so far": "today",
        "This week so far": "week",
        "This month so far": "month",
        "This year so far": "year"
    };
    var rangeObj = {};
    if (req.body.fromDate != undefined) {
        var From = moment(req.body.fromDate).utc().format("YYYY-MM-DD HH:mm:ss");
        var to = moment(req.body.toDate).startOf('second').utc().format("YYYY-MM-DD HH:mm:ss");
        rangeObj = {"range": {"time": {"gte": From, "lte": to, "format": "yyyy-MM-dd HH:mm:ss"}}};
        ARRAY.push(rangeObj)
    } else if (req.body['QuickRanges'] != undefined) {
        var to = moment().startOf('second').format("YYYY-MM-DD HH:mm:ss");
        var From = moment().startOf(quickRangesObj[req.body['QuickRanges']]).format("YYYY-MM-DD HH:mm:ss");
        rangeObj = {"range": {"time": {"gte": From, "lte": to, "format": "yyyy-MM-dd HH:mm:ss"}}};
        ARRAY.push(rangeObj)
    }
    db.cypher({
        query: 'MATCH (n:Login{UserName:"' + req.body.UserName + '",Type:"User"})-[:Products]->(m)-[:Vendors]->(V)-[:Locations]->(L) return COLLECT (distinct m.UserName) as products',
    }, function (err, products) {
        if (err) {
            logger.error(err)
            return next(err)
        }
        var Obj = {"terms": {"PRODUCT_NAME": products}};
        ARRAY.push(Obj)
    })
    if (req.body.LOCATION != undefined) {
        var Obj = {"term": {"LOCATION": req.body.LOCATION}};
        ARRAY.push(Obj)
    }
    EsForCounters(elasticSearchClient, ARRAY, function (err, data) {
        if (err) {
            return next(err)
        } else {
            res.send(data)
        }
    })
}


function getAccountDetails(req, res, next) {
    var elasticSearchClient = req.elasticSearchClient
    logger.info("get account details , req.body" + JSON.stringify(req.body))
    parseRequestDataTillCorrectMappingIsDoneInES(req.body, function (errWithParsingData, parsedData) {
        logger.info("the data obtained from parseRequest function is" + JSON.stringify(parsedData))
        elasticSearchClient.search({
            index: 'accounts',
            type: 'logs',
            body: {
                size: 1000,
                query: {
                    bool: {
                        must: parsedData
                    }
                }
            }
        }, function (err, data) {
            if (err) {
                logger.error("error querying with index test util elasticSearch" + JSON.stringify(err))
                return next(err)
            }
            if (data) {
                logger.info("elastic search returns results")
                var resArray = []
                data.hits.hits.forEach(function (hit) {
                    resArray.push(hit._source);
                })
                res.json({"status": 1, "data": resArray})
            }
        })


    })
}

function majorCountForGemsTrail(req, res, products, noOfMinutes, callback) {
    var elasticSearchClient = req.elasticSearchClient
    elasticSearchClient.count({
        index: 'gemstrail',
        type: 'association',
        body: {
            query: {
                bool: {
                    must: [{terms: {"PRODUCT_NAME": products}}, {term: {"level": "MAJOR"}}],

                    filter: {
                        range: {
                            time: {
                                gte: 'now-' + noOfMinutes
                            }
                        }
                    }
                }
            }
        }
    }, function (errFindingGemsAction, gemsActionFound) {
        if (errFindingGemsAction) {
            return callback(errFindingGemsAction)
        }
        console.log(gemsActionFound)
        callback(null, gemsActionFound)
    })
}


function criticalCountForGemsTrail(req, res, products, noOfMinutes, callback) {
    console.log("called criticalCount for gems")
    console.log(products)
    var elasticSearchClient = req.elasticSearchClient
    elasticSearchClient.count({
        index: 'gemstrail',
        type: 'association',
        body: {
            query: {
                bool: {
                    must: [{terms: {"PRODUCT_NAME": products}}, {term: {"level": "CRITICAL"}}],

                    filter: {
                        range: {
                            time: {
                                gte: 'now-' + noOfMinutes
                            }
                        }
                    }
                }
            }
        }
    }, function (errFindingGemsAction, gemsActionFound) {
        if (errFindingGemsAction) {
            return callback(errFindingGemsAction)
        }
        console.log(gemsActionFound)
        callback(null, gemsActionFound)
    })
}


//for dashboard/homepage Based on the requirement changes on 20/9/2017
function systemBreachesData(req, res, next) {
    var elasticSearchClient = req.elasticSearchClient
    logger.info("getDatForTables in the homepage calleds , req.body" + JSON.stringify(req.body))
    var noOfMinutes = req.body.minutes + 'm'
    noOfMinutes = '10000m' //hardcodeddata to be removed later
    // find all products associated with userName

    var products = req.body.PRODUCT_NAME
    var gemsTrail = {}
    var KPIBreaches = {
        "critical": 10,
        "CriticalSoft": 4,
        "CriticalHard": 6,
        "major": 10,
        "majorSoft": 4,
        "majorHard": 6
    }
    var systems = {"critical": 2, "major": 3}
    var services = {"critical": 1, "major": 4}

    elasticSearchClient.search({
        index: 'alertsfromsocketio',
        type: 'logs',
        body: {
            size: 1000,
            query: {
                bool: {
                    must: {
                        terms: {"PRODUCT_NAME": products},
                    },
                    filter: {
                        range: {
                            time: {
                                gte: 'now-' + noOfMinutes
                            }
                        }
                    }
                }
            }
        }
    }, function (err, data) {
        if (err) {
            return next(err)
        }
        if (data) {
            logger.info("elastic search returns results")
            var tableDataArr = []
            data.hits.hits.forEach(function (hit) {
                hit._source.KPIType = 'hourly'
                hit._source.KPIStatus = 'acted_upon'
                hit._source.reportedTime = hit._source.time
                delete hit._source.time
                hit._source.breachValue = hit._source.VALUE
                hit._source.KPIName = hit._source.ALARM_NAME
                delete hit._source.VALUE
                hit._source.severity = hit._source.level
                delete hit._source.level

                tableDataArr.push(hit._source);
            })
            console.log(tableDataArr)
            criticalCountForGemsTrail(req, res, products, noOfMinutes, function (errFindingCriticalCount, criticalCount) {
                if (errFindingCriticalCount) {
                    return next(errFindingCriticalCount)
                }
                gemsTrail.critical = criticalCount.count
                majorCountForGemsTrail(req, res, products, noOfMinutes, function (errFindingMajorCount, majorCount) {
                    if (errFindingMajorCount) {
                        return next(errFindingMajorCount)
                    }
                    gemsTrail.major = majorCount.count
                    res.json({
                        "status": 1,
                        "gemsAction": gemsTrail,
                        "KPIBreaches": KPIBreaches,
                        "services": services,
                        "systems": systems,
                        "data": tableDataArr
                    })
                })

            })


        }
    })
}

//used in dashboard homepage
function viewKPIFormula(req, res, next) {
    var vendor = req.body.VENDOR_NAME
    var product = req.body.PRODUCT_NAME
    var location = req.body.LOCATION
    var KPIName = req.body.KPIName
    // query the data from db after the data is inserted
    //for now , hardcode a value

    var formula = 'Offer Acceptance Rate (%) = ( Number of Acceptances / Number of Offers ) x 100'
    res.json({"formula": formula})

}
// shilpi
function systemDataGraph(req, res, next) {
    var vendor = req.body.VENDOR_NAME
    var product = req.body.PRODUCT_NAME
    var location = req.body.LOCATION
    var KPIName = req.body.KPIName
    var elasticSearchClient = req.elasticSearchClient
    // no KPI name field in ES right now .
    var resultArr = []
    elasticSearchClient.search({
        index: 'alertsfromsocketio',
        type: 'logs',
        body: {
            size: 20,
            query: {
                bool: {
                    must: {
                        term: {"ALARM_NAME": KPIName},
                    },
                    filter: {
                        range: {
                            time: {
                                gte: 'now-30h'
                            }
                        }
                    }
                }
            }
        }
    }, function (errFindingKPI, KPIFound) {
        if (errFindingKPI) {
            return next(errFindingKPI)
        }
        KPIFound.hits.hits.forEach(function (hit) {
            hit._source.KPIType = 'hourly'
            hit._source.KPIStatus = 'acted_upon'
            hit._source.reportedTime = hit._source.time
            delete hit._source.time
            hit._source.breachValue = hit._source.VALUE
            hit._source.KPIName = hit._source.ALARM_NAME
            delete hit._source.VALUE
            hit._source.severity = hit._source.level
            delete hit._source.level
            resultArr.push(hit._source)
        })
        res.json({"status": 1, "data": resultArr})

    })
}


function getKPIForDataOnBoarding(req, res, next) {
    var elasticSearchClient = req.elasticSearchClient
    logger.info("getKPIForDataOnBoarding called , req.body" + JSON.stringify(req.body))
    var products = req.body.PRODUCT_NAME
    elasticSearchClient.search({
        index: 'alertsfromsocketio',
        type: 'logs',
        body: {
            size: 100,
            query: {
                terms: {"PRODUCT_NAME": products},

            }
        }
    },
            function (err, data) {
                if (err) {
                    return next(err)
                }
                if (data) {
                    logger.info("elastic search returns results")
                    var tableDataArr = []
                    data.hits.hits.forEach(function (hit) {
                        hit._source.KPIType = 'hourly'
                        hit._source.KPIStatus = 'acted_upon'
                        hit._source.reportedTime = hit._source.time
                        delete hit._source.time
                        hit._source.breachValue = hit._source.VALUE
                        hit._source.KPIName = hit._source.ALARM_NAME
                        delete hit._source.VALUE

                        tableDataArr.push(hit._source);
                    })

                    res.json({
                        "data": tableDataArr
                    })


                }
            }
    )
}


function userAudit(req, res, next) {
    // add configAuditTrail when it's ready
   // var products = req.body.PRODUCT_NAME
    var userName = req.query.userName
  
    // var lowercaseArr = []
    //
    // products.forEach(function (eachProduct) {
    //     lowercaseArr.push(eachProduct.toLowerCase())
    // })
    request.post('http://172.16.23.27:3001/getProducts', {json: {UserName: userName}}, function (error, response, body) {
        if (error) {
            logger.error(error)
            return next(error)
        }
        logger.info("the data returned by the API is shown below")
        logger.info(body)
       
       var elasticSearchClient = req.elasticSearchClient
    elasticSearchClient.search({
        index: 'useraudit,configdiff',
        type: 'logs',
        body: {
            size: 10000, "sort": [{"TIME": {"order": "desc"}}],
            query: {
                //converting it to lower case since the mapping of this index is text and text fields are analyzed
                //terms: {"PRODUCT_NAME": lowercaseArr}
                bool: {
                    must_not: {

                        term: {"Description": "Not Modified."}
                    },
                    must:{
                        terms:{"PRODUCT_NAME":body}
                    }
                
                }



            }
        }
    },
            function (err, data) {
                if (err) {
                    return next(err)
                }
                var results = []

                data.hits.hits.forEach(function (item) {
                    var source = item._source
                    source._id = item._id
                    results.push(source)


                })
                res.send(results)

            })
        
        
    })

    
}


exports.search = search


//KPI Rules
exports.getKPIRules = getKPIRules
exports.insertKPIRules = insertKPIRules
exports.searchKPIRules = searchKPIRules
exports.updateKPIRules = updateKPIRules
exports.deleteKPIRules = deleteKPIRules


//Inventory Utilisation 
exports.searchUtils = searchServiceUtil
exports.getUtils = getUtils
//exports.searchServiceBreaches = searchServiceBreaches
exports.getBreaches = getBreaches
exports.searchBreaches = searchBreaches


exports.insertDirectoryData = insertDirectoryData
exports.getDirectoryData = getDirectoryData
exports.searchConfigDiff = searchConfigDiff
exports.searchAuditTrail = searchAuditTrail

exports.kpiAlarms = kpiAlarms


//KPI/SLA Breaches
exports.getDataBasedonCategory = getDataBasedonCategory
exports.getAllCountersForBreaches = getAllCountersForBreaches
exports.getDetailedDataForBreaches = getDetailedDataForBreaches


exports.getAccountDetails = getAccountDetails

//for the new homepage (table in place of attentionWindow)
exports.systemBreachesData = systemBreachesData
//view formula
exports.viewKPIFormula = viewKPIFormula
exports.systemDataGraph = systemDataGraph
exports.getKPIForDataOnBoarding = getKPIForDataOnBoarding
exports.userAudit = userAudit
