/**
 * Created by Radhika on 6/8/2017.
 */



var logger = require('../libs/logger').workflowLogger;


// TODO : handle file upload when action is 'script'
// TODO : insert , list , edit ? (  elastic search replaces document with the matching id )

// check if workflow/metric name already exists
/*
 {"workFlowName":"", rawCondition:"", "condition":"", "action":"[]", product , vendor , location}
 check if workflow name already exists ,
 yes => name already exists
 no => insert to elastic search , res.send(status:1)
 */
/*
 GET test-kpi-alarms/_search
 {
 "query": {
 "match" : {
 "LOCATION": "Pondicherry"
 }
 }
 }

 */


/*
 POST twitter/_update_by_query?conflicts=proceed
 {
 "query": {
 "term": {
 "user": "kimchy"
 }
 }
 }

 */

function documentExists(req, res, next) {
    console.log("documemnt exists")
    var elasticSearchClient = req.elasticSearchClient
    console.log(req.body.Workflow_Name)
    console.log(req.body.prodName)
    console.log(req.body.Vendor)
    console.log(req.body.Location)

    elasticSearchClient.search({
            index: 'workflow-node',
            type: 'workflow',
            size: 1000,
            body: {
                query: {
                    bool: {
                        must: [
                            {
                                term: {
                                    Workflow_Name: req.body.Workflow_Name
                                }
                            },
                            {
                                term: {
                                    prodName: req.body.prodName
                                }
                            },
                            {
                                term: {
                                    Vendor: req.body.Vendor
                                }
                            }
                            ,
                            {
                                term: {
                                    Location: req.body.Location
                                }
                            }

                        ]

                    }

                }
            }

        },
        function (error, response) {
            if (error) {
                logger.error(error)
                return next(error)
            }
            if (response.hits.hits.length > 0) {
                console.log("records present")
                res.json({"msg": "rule exists", "status": 0})
            }
            else {
                console.log("no records found - insert")
                console.log(response)
                console.log(response.hits.hits.length)
                insert(req, res, next)
            }
        }
    )

}
function insert(req, res, next) {
    console.log("insert")
    var elasticSearchClient = req.elasticSearchClient
    elasticSearchClient.index({
        index: 'workflow-node',
        type: 'workflow',
        body: req.body
    }, function (error, response) {
        if (error) {
            logger.error("error with pushing data to elastic search " + error)
            return next(error)

        }

        res.json({"status": 1})
    });
}


function getData(req, res, next) { // list all data in elastic search
    var elasticSearchClient = req.elasticSearchClient
    elasticSearchClient.search({
        index: 'workflow-node',
        body: {
            from: 0, size: 1000,
            //  _source: ["name", "action", "type", "status", "createdBy"],
            query: {
                match_all: {}
            }
        }
    }, function (err, data) {
        if (err) {
            logger.error("getData API: error when querying elastic search " + JSON.stringify(err))
            return next(err)
        }
        if (data) {
            logger.info("elastic search returns results")
            res.json({"status": 1, "data": data.hits.hits})

        }
    })


}


function update1(req, res, next) {
    var elasticSearchClient = req.elasticSearchClient

    // var theScript = {
    //     "inline": "ctx._source.color = 'pink'; ctx._source.weight = 500; ctx._source.diet = 'omnivore';"
    // }


  console.log(req.body.Actions)


    var theScript = {
        "inline": " ctx._source.Location = '"+req.body.Location+"';ctx._source.prodName = '"+req.body.prodName+"';ctx._source.Vendor = '"+req.body.Vendor+"';ctx._source.Group = '"+req.body.Group+"';ctx._source.Workflow_Name = '"+req.body.Workflow_Name+"';ctx._source.Actions= '"+req.body.Actions+"';" +
        "ctx._source.Priority = '"+req.body.Priority+"';"

    }
    elasticSearchClient.updateByQuery({
        index: 'workflow-node',
        type: 'workflow',
        conflicts: 'proceed', // when update by query hits versionconflict
        body: {
            "query": {
                "term": {
                    "Workflow_Name": req.body.Workflow_Name
                }

            },
            "script": theScript

        }

    }, function (error, response) {
        if (error) {
            logger.error("error with pushing data to elastic search " + error)
            return next(error)

        }
        console.log(response)
        res.json({"status": 1})
    });
}


function update(req,res,next) {
    var elasticSearchClient = req.elasticSearchClient
    elasticSearchClient.search({
        index: 'workflow-node',
        type: 'workflow',
        body: {
            from: 0, size: 1000,
            //  _source: ["name", "action", "type", "status", "createdBy"],
            query: {
                match: {
                    "Workflow_Name": req.body.Workflow_Name
                }
            }
        }
    }, function (err, data) {
        if (err) {
            logger.error("getData API: error when querying elastic search " + JSON.stringify(err))
            return next(err)
        }
        if (data) {
            logger.info("elastic search returns results")
            var id = data.hits.hits[0]._id
            elasticSearchClient.delete({
                index: 'workflow-node',
                type: 'workflow',
                id:id


            },function(errWhileDeleting, dataDeleted){
                if (errWhileDeleting) {
                    logger.error(errWhileDeleting)
                }
                else{
                    elasticSearchClient.create({
                        index: 'workflow-node',
                        type: 'workflow',
                        id: id,
                        body: req.body
                    },function (err1, data1){
                        if (err1) {
                            logger.error("getData API: error when querying elastic search " + JSON.stringify(err1))
                            return next(err1)
                        }
                        if (data1) {
                            res.json({"status": 1})
                        }
                    })
                }
            })


        }
    })


}

function fileUpload(req, res, next) {
    var multer = require('multer')
    var path = require('path')
    var mkdirp = require('mkdirp')
    var uploadPath = path.join(__dirname, '..', 'uploads')
    mkdirp.sync(uploadPath);
    var upload = multer({dest: uploadPath}).single('file')
    upload(req, res, function (err) {
        if (err) {
            return next(err)
        }
        console.log(req.body)
        console.dir(req.file)
        var elasticSearchClient = req.elasticSearchClient
        elasticSearchClient.index({
            index: 'workflow',
            type: 'data',
            body: req.body
        }, function (error, response) {
            if (error) {
                logger.error("error with pushing data to elastic search " + error)
                return next(error)

            }
            logger
            res.json({"status": 1})
        });
    })


}


function setData(req, res, next) {
    var updateAllowed = ["status", "type", "action"]
    var updateFields = {}
    updateAllowed.forEach(function (field, index, array) {
        if (req.body.hasOwnProperty(field))
            console.log(field)
        var fieldInUpper = field.toUpperCase()
        //var obj = {field.toUpperCase(): reqBody.alarm}
        updateFields[field.toUpperCase()] = req.body[field]

        // if(field == 'TYPE'){
        //     updateFields["TYPE"] = req.body[field];
        // }
        // if(field == "STATUS"){
        //     updateFields["TYPE"] = req.body[field];
        // }
        // if(field== "ACTION"){
        //     updateFields["ACTION"] = req.body[field];
        // }

        console.log(updateFields)


    })

    console.log(updateFields)
    // var elasticSearchClient = req.elasticSearchClient
    // elasticSearchClient.index({
    //     "index":'workflow'
    // })

}


exports.insert = documentExists
exports.getData = getData
exports.update = update
//exports.test = fileUpload
//exports.setData = setData



