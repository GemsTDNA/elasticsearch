var mongodb = require('mongodb');
var MongoClient = mongodb.MongoClient;
var mongoUrl = 'mongodb://172.16.23.20:27017/st2';
var db;

exports.connect = function(callback) {
  MongoClient.connect(mongoUrl, function(err, database) {
    if( err ) throw err;
    db = database;
    callback();
  }
}

exports.doSomethingWithDatabase = function(callback){
  // this is using the same db connection
  db.collection('db_name').find({}, function(err, docs) {
    // do something
    callback(docs);
  });
