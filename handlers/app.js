var express = require('express');
var app = express();
var database = require('./database.js');

// Initialize connection
database.connect(function() {
  // Start the application after the database connection is ready
  app.listen(3000);
  console.log("Listening on port 3000");

  app.get("/", function(req, res) {
    //  do some stuff
    database.doSomethingWithDatabase(someCallback);
  });
});
