var path = require('path')
var request = require('request')
var fs = require('file-system')


var certificatePath = path.join(__dirname, '../', 'certificate/st2.crt');
var certificateKey = path.join(__dirname, '../', 'certificate/st2.key');


var certificate = fs.readFileSync(certificatePath);
var key = fs.readFileSync(certificateKey);
var redis = require('redis')
var client = redis.createClient(6379, '172.16.23.27');

var neo4j = require('neo4j');
var db = new neo4j.GraphDatabase('http://neo4j:teledna@172.16.23.27:7474');
var logger = require('../libs/logger').stackStormLogger;

getAllRules()

function getAllRules(req, res, next) {


    var options = {
        url: 'http://172.16.23.20/api/v1/rules',
        agentOptions: {
            cert: certificate,
            key: key,
            rejectUnauthorized: false,

        }
    }

    request.get(options,
        function (error, response, body) {
            if (error) {
                // logger.error(error)
                return next(error)
            }
            //console.log(body)
            res.send(body)

        })
}

