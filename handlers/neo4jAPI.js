const Influx = require('influx');

var neo4j = require('neo4j');
var join = require('join-by-keys')
var db = new neo4j.GraphDatabase('http://neo4j:teledna@172.16.23.27:7474');
var logger = require('../libs/logger').neo4jLogger;
var nodemailer = require('nodemailer');
var smtpTransport = require('nodemailer-smtp-transport');


/*

 MATCH (n:Login{UserName:"'+req.body.UserName+'"})-[:Products]->(p)-[:Vendors]->(v)-[:Locations]->(l)-[:Customers]->(c) RETURN
 COLLECT({PRODUCT_NAME:p.UserName,VENDOR_NAME:v.UserName,LOCATION:l.UserName,CUSTOMER:c.UserName}) as results
 */
/*
 MATCH (n:Login{UserName:"sreepad"})-[:Products]->(p)-[:Vendors]->(v)-[:Locations]->(l) RETURN
 COLLECT({PRODUCT_NAME:p.UserName,VENDOR_NAME:v.UserName,LOCATION:l.UserName}) as results
 */
function PLV(req, res, next) {

    db.cypher({
        query: 'MATCH (n:Login{UserName:{UserName}})-[:Products]->(p)-[:Vendors]->(v)-[:Locations]->(l) RETURN COLLECT({PRODUCT_NAME:p.UserName,VENDOR_NAME:v.UserName,LOCATION:l.UserName}) as results',
        params: {
            UserName: req.body.UserName,
        },

    }, function (err, results) {
        if (err) {
            return next(err)
        }

        var iterate = results[0].results
        var array = join(iterate, ['PRODUCT_NAME', 'VENDOR_NAME'])
        var resultArray = join(array, ['PRODUCT_NAME'])
        var obj = {}
        resultArray.forEach(function (item) {
            var product = item.PRODUCT_NAME
            var vendorArray = item.VENDOR_NAME
            var locationArray = item.LOCATION
            var vendorsObj = {}

            locationArray.forEach(function (eachLocationArr, index) {
                var vendorName = vendorArray[index]
                vendorsObj[vendorName] = locationArray[index]
            })

            obj[product] = vendorsObj

        })
        res.send({"status": 1, "data": obj})

    });
}


function VLP(req, res, next) {
    var noOfParams = Object.keys(req.body).length
    db.cypher({
        query: 'MATCH (n:Login{UserName:{UserName}})-[:Products]->(p)-[:Vendors]->(v)-[:Locations]->(l) RETURN COLLECT({PRODUCT_NAME:p.UserName,VENDOR_NAME:v.UserName,LOCATION:l.UserName}) as results',
        params: {
            UserName: req.body.UserName,
        },

    }, function (err, results) {
        if (err) {
            return next(err)
        }
        var iterate = results[0].results
        var array = join(iterate, ['VENDOR_NAME', 'LOCATION',])
        var resultArray = join(array, ['VENDOR_NAME'])
        var obj = {}


        resultArray.forEach(function (item) {
            var productArrays = item.PRODUCT_NAME
            var vendors = item.VENDOR_NAME
            var location = item.LOCATION
            var locationObj = {}
            var vendorObj = {}
            location.forEach(function (eachProductArr, index) {
                var locationName = location[index]
                var productName = productArrays[index]
                locationObj[locationName] = productName
            })

            obj[vendors] = locationObj
        })
        // res.send(obj)  // api required by ranjeeth ends here

        if (noOfParams == 1) {
            var keys = Object.keys(obj)
            res.json(keys)
        }
        else if (noOfParams == 2 && req.body.vendorName != null) {
            var vendor = req.body.vendorName
            if (obj[vendor] != undefined) {
                res.json(Object.keys(obj[vendor]))
            }
            else {
                res.json([])
            }

        }
        else if (noOfParams == 3 && req.body.locationName != null) {
            var loc = req.body.locationName
            var vendor = req.body.vendorName
            if (obj[vendor][loc] != null) {
                res.send(obj[vendor][loc])
            }
            else {
                res.json([])
            }


        }
        else {
            res.json([])
        }

    });
}


function LVP(req, res, next) {
    var noOfParams = Object.keys(req.body).length
    db.cypher({
        query: 'MATCH (n:Login{UserName:{UserName}})-[:Products]->(p)-[:Vendors]->(v)-[:Locations]->(l) RETURN COLLECT({PRODUCT_NAME:p.UserName,VENDOR_NAME:v.UserName,LOCATION:l.UserName}) as results',
        params: {
            UserName: req.body.UserName,
        },

    }, function (err, results) {
        if (err) {
            return next(err)
        }
        var iterate = results[0].results
        var array = join(iterate, ['LOCATION', 'VENDOR_NAME'])
        var resultArray = join(array, ['LOCATION'])
        var obj = {}

        var productArr = []
        var locationArr = []
        var vendorArr = []


        resultArray.forEach(function (item) {
            var productArrays = item.PRODUCT_NAME
            var vendorArray = item.VENDOR_NAME
            var location = item.LOCATION
            var vendorsObj = {}

            productArrays.forEach(function (eachProductArr, index) {
                var vendorName = vendorArray[index]
                vendorsObj[vendorName] = productArrays[index]
            })

            obj[location] = vendorsObj
        })

        if (noOfParams == 1) {
            var keys = Object.keys(obj)
            res.json(keys)
        }
        else if (noOfParams == 2 && req.body.locationName != undefined) {
            var loc = req.body.locationName
            if (obj[loc] != null) {
                res.json(Object.keys(obj[loc]))
            }
            else {
                res.json([])
            }

        }
        else if (noOfParams == 3 && req.body.vendorName != undefined) {
            var loc = req.body.locationName
            var vendor = req.body.vendorName
            if (obj[loc][vendor] != null) {
                res.send(obj[loc][vendor])
            }
            else {
                res.json([])
            }


        }
        else {
            res.json("invalid parameters")
        }

    });
}


function VLPStackstorm(req, res, next) {
    var noOfParams = Object.keys(req.body).length
    db.cypher({
        query: 'MATCH (n:Login{UserName:{UserName}})-[:Products]->(p)-[:Vendors]->(v)-[:Locations]->(l) RETURN COLLECT({PRODUCT_NAME:p.UserName,VENDOR_NAME:v.UserName,LOCATION:l.UserName}) as results',
        params: {
            UserName: req.body.UserName,
        },

    }, function (err, results) {
        if (err) {
            return next(err)
        }
        var iterate = results[0].results
        var array = join(iterate, ['VENDOR_NAME', 'LOCATION',])
        var resultArray = join(array, ['VENDOR_NAME'])
        var obj = {}


        resultArray.forEach(function (item) {
            var productArrays = item.PRODUCT_NAME
            var vendors = item.VENDOR_NAME
            var location = item.LOCATION
            var locationObj = {}
            var vendorObj = {}
            location.forEach(function (eachProductArr, index) {
                var locationName = location[index]
                var productName = productArrays[index]
                locationObj[locationName] = productName
            })

            obj[vendors] = locationObj
        })
        res.json({"status": 1, "data": obj})
    })
}


function getGeoHash(req, res, next) {

    db.cypher({
        query: 'MATCH (n:Login{UserName:{UserName}})-[:Products]->(p)-[:Vendors]->(v)-[:Locations]->(l)-[:Customers]->(c) return {Product:p.UserName,Location:l.UserName,Customer:c.UserName,geohash:c.geohash}',
        params: {
            UserName: req.body.UserName,
        },

    }, function (err, results) {
        if (err) {
            return next(err)
        }


        res.send({"status": 1, "data": results})

    });
}
/*

 */

//alternate query
/*
 'MATCH(n:`TDNA SMSC`{Name:"TDNA SMSC"})-[r:Floors]->(allFloors)-[r1:Racks]->(allRacks)<-->(node)' +
 ' WITH n,{ allFloors : allFloors, allRacks : allRacks,nodes : COLLECT(node)} AS racks' +
 ' WITH{n : n ,racks : COLLECT(racks)}AS n RETURN n',
 */

function getServerDetails(req, res, next) {
    // {
    //     UserName:”sreepad”,
    //     Location:”Banglore”,
    //     PRODUCT_NAME: “SMSC”,
    //     Customer:”Aircel”
    // }

    db.cypher({
        query: ' MATCH (n{Name:{UserName}})-[r:Floors]->(allFloors)-[r1:Racks]->(allRacks)-[:Contains]->(node) WITH {root:n,Floors : allFloors, Racks : allRacks,nodes : COLLECT(node)} AS racks RETURN racks',
        params: {
            UserName: req.body.UserName,
        },

    }, function (err, results) {
        if (err) {
            return next(err)
        }
        res.send({"status": 1, "data": results})

    });
}

//  UserName:”sreepad”,
// Location:”Banglore”,
// Product: “SMSC”,
// Customer:”Aircel”

function topologyDisplay(req, res, next) {
    db.cypher({
        query: 'MATCH(n)-[r{Name:"Aircel Chennai"}]-(m)return DISTINCT {S:n.PRODUCT_NAME,R:TYPE(r),E:m.PRODUCT_NAME} as results',
        params: {
            UserName: req.body.UserName,
        },

    }, function (err, results) {
        if (err) {
            return next(err)
        } else {
            var node = {};
            var edges = [];
            var id = 0;
            console.log(JSON.stringify(results));
            results.forEach(function (Obj) {
                console.log(JSON.stringify(Obj));
                var link = {};
                link.source = Obj.results.S;
                link.target = Obj.results.E;
                link.type = Obj.results.R;
                link._id = id;
                id++;
                edges.push(link);
                node[Obj.results.S] = {};
                node[Obj.results.E] = {};
                node[Obj.results.S].UserName = Obj.results.S;
                node[Obj.results.E].UserName = Obj.results.E;
            })
            console.log(JSON.stringify(node));
            var nodes = [];
            var IDObj = {};
            var id = 1000;
            Object.keys(node).forEach(function (key) {
                console.log("Key : " + JSON.stringify(node.key));
                var Obj = {};
                Obj.properties = {};
                Obj.labels = [];
                Obj.labels.push("Service Topology");
                Obj.properties['UserName'] = key;
                Obj.properties['Type'] = "Service";
                Obj.id = id;
                Obj._id = id;
                Obj.size = 6104;
                IDObj['key'] = id;
                id++;
                nodes.push(Obj);
            })
            var links = [];
            edges.forEach(function (Obj) {
                Obj['source'] = IDObj[Obj.source];
                Obj['target'] = IDObj[Obj.target];
                links.push(Obj);
            })
            nodes[0].properties.Type = "root";
            var Obj = {
                "nodes": [{
                    "properties": {"UserName": "STP", "Type": "root"},
                    "labels": ["Service Topology"],
                    "id": 1000,
                    "_id": 1000,
                    "size": 6104
                }, {
                    "properties": {"UserName": "SMSC", "Type": "Service"},
                    "labels": ["Service Topology"],
                    "id": 1001,
                    "_id": 1001,
                    "size": 6104
                }], "links": [{"type": "Diameter", "_id": 1, "source": 1000, "target": 1001}]
            };
            res.send(Obj);
        }

    });
}
/*
 $MATCH(n)-[r{Name:"Aircel Chennai"}]-(m)return DISTINCT {N:n.PRODUCT_NAME,R:TYPE(r),M:m.PRODUCT_NAME}



 */

function getIPAndServerDetails(req, res, next) {
    // {
    //     "PRODUCT_NAME":"SMSC",
    //     "LOCATION":"Delhi",
    //     "VENDOR_NAME":"Acision",
    //     "Eth0":"10.178.38.180",
    //     "UserName":"ramya"
    //
    // }
    //MATCH (n:server{PRODUCT_NAME:"SMSC",Eth0:"10.178.38.180"})<-[:Contains]-(R)<-[:Racks]-(F) return {Floor:F.Name,Rack:R.Name,Server:n} as Actual_Data


    db.cypher({
        query: 'MATCH (n:server{PRODUCT_NAME:{PRODUCT_NAME},Eth0:{Eth0}})<-[:Contains]-(R)<-[:Racks]-(F) return {Floor:F.Name,Rack:R.Name,Server:n} as Actual_Data',
        params: {
            PRODUCT_NAME: req.body.PRODUCT_NAME,
            Eth0: req.body.Eth0
        },

    }, function (err, actualData) {
        if (err) {
            return next(err)
        }
        console.log(actualData)
        db.cypher({
            query: ' MATCH (n{Name:{UserName}})-[r:Floors]->(allFloors)-[r1:Racks]->(allRacks)-[:Contains]->(node) WITH {root:n,Floors : allFloors, Racks : allRacks,nodes : COLLECT(node)} AS racks RETURN racks',
            params: {
                UserName: req.body.UserName,
            },

        }, function (err, results) {
            if (err) {
                return next(err)
            }
            console.log(results)
            res.send({"status": 1, "ActualData": actualData, "TotalData": results})

        });
        // res.send({"status": 1, "data": results})

    });


}


function getIPBasedOnProduct(req, res, next) {
    db.cypher({
        query: 'MATCH (n{PRODUCT_NAME:{PRODUCT_NAME}})return n.Eth0 as IP',
        params: {
            PRODUCT_NAME: req.body.PRODUCT_NAME,
        },

    }, function (err, data) {
        if (err) {
            return next(err)
        }

        var arr = []
        data.forEach(function (value, index) {
            arr.push(value.IP)
        })
        var resultArr = arr.filter(function (value) {

            if (value != "" || value != null || value != '') {
                return value
            }


        })
        res.send(resultArr)

    });


}

//LOGICAL ONBOARDING
//receives product_name and returns all groups associated with that product name
function getServerInfoBasedOnProduct(req, res, next) {
    db.cypher({
        query: 'MATCH (n:server{PRODUCT_NAME:{PRODUCT_NAME}}) WHERE exists(n.GROUP) return  ' +
        'n.GROUP as GROUP , COLLECT(n.Eth0) as IP ',
        params: {
            PRODUCT_NAME: req.query.PRODUCT_NAME,
        },

    }, function (err, data) {
        if (err) {
            return next(err)
        }
        db.cypher({
            query: 'MATCH (n:server{PRODUCT_NAME:{PRODUCT_NAME}})-[r]-(m)' +
            'return DISTINCT(type(r)) as connectivity ,m.PRODUCT_NAME as title',
            params: {
                PRODUCT_NAME: req.query.PRODUCT_NAME,
            },
        }, function (err1, data1) {
            if (err1) {
                return next(err1)
            }
            var connArr = []
            var externalElementsArr = []
            data1.forEach(function (item) {
                connArr.push(item.connectivity)
                if (item.title != null) {
                    externalElementsArr.push(item.title)
                }

            })

            res.json({"status": 1, "groups": data, "connectivity": connArr, "externalElements": externalElementsArr})


        })


    });

}

function getLocationsBasedOnUserName(req, res, next) {

    db.cypher({
        query: 'MATCH (n:Login{UserName:{UserName}})-[:Products]->(p)-[:Vendors]->(v)-[:Locations]->(l) RETURN COLLECT({PRODUCT_NAME:p.UserName,VENDOR_NAME:v.UserName,LOCATION:l.UserName}) as results',
        params: {
            UserName: req.body.UserName,
        },

    }, function (err, results) {
        if (err) {
            return next(err)
        }
        var iterate = results[0].results
        var array = join(iterate, ['LOCATION', 'VENDOR_NAME'])
        var resultArray = join(array, ['LOCATION'])
        var obj = {}
        console.log(resultArray)
        resultArray.forEach(function (item) {

            var productArrays = item.PRODUCT_NAME
            var vendorArray = item.VENDOR_NAME
            var location = item.LOCATION
            var vendorsObj = {}

            productArrays.forEach(function (eachProductArr, index) {
                var vendorName = vendorArray[index]
                vendorsObj[vendorName] = productArrays[index]
            })
            obj[location] = vendorsObj
        })
        res.send({"status": 1, "data": obj})

    });

}

// used in the homepage
function userListEscalate(req, res, next) {

    db.cypher({
        query: 'MATCH(n:Login{Type:"User"})-[:Products]->(p{UserName:{product}})-[:Vendors]->(v{UserName:{vendor}})-[:Locations]->(l{UserName:{location}})return ID(n) as ID,n.Email as Email,n.UserName as UserName',
        params: {
            product: req.body.PRODUCT_NAME,
            vendor: req.body.VENDOR,
            location: req.body.LOCATION

        },

    }, function (err, results) {
        if (err) {
            return next(err)
        }
        res.json({"status": 1, "data": results})

    });

}


function sendEmail(req, res, next) {
    /**
     * Created by Radhika on 9/21/2017.
     */
    logger.info('send Email' + JSON.stringify(req.body))

    var emailArr = req.body.email
    var stringifiedEmail = emailArr.join(',')
    var message = req.body.message
    if (message == undefined) {
        message = 'hello world'
    }

    var transporter = nodemailer.createTransport(smtpTransport({
        host: 'localhost',
        port: 25
    }));

// setup email data with unicode symbols
    let mailOptions = {
        from: 'support.GEMS@localhost.com', // sender address
        to: stringifiedEmail, // list of receivers
        subject: 'Hello ✔', // Subject line
        text: message// plain text body
       // html: '<b>Hello world?</b>' // html body
    };

// send mail with defined transport object
    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            return next(error)
            //return console.log(error);
        }
        logger.info('Message sent: %s', info.messageId);
        res.json({"status":1,"messageId":info.messageId})


    });

}


function getIPAddressBasedOnVLP(req, res, next) {
    var product = req.body.PRODUCT_NAME
    var vendor = req.body.VENDOR
    var location = req.body.LOCATION
    db.cypher({
        query: 'MATCH (n:server) WHERE n.PRODUCT_NAME = {product} AND n.VENDOR_NAME = {vendor} AND n.LOCATION = {location}  RETURN n.Eth0 as IP',
        params: {
            product: req.body.PRODUCT_NAME,
            vendor: req.body.VENDOR,
            location: req.body.LOCATION

        },

    }, function (err, results) {
        if (err) {
            return next(err)
        }
        var arr = []
        var IPAddresses = []

        results.forEach(function (item) {

            if (item.IP != "" && item.IP != null) {
                IPAddresses.push(item.IP)
            }
        })
        res.json({"status": 1, "data": IPAddresses})

    });

}


exports.getTopology = topologyDisplay
exports.getGeoHash = getGeoHash
exports.getServerDetails = getServerDetails
exports.getIPAndServerDetails = getIPAndServerDetails
exports.getIPBasedOnProduct = getIPBasedOnProduct
exports.PLV = PLV
exports.LVP = LVP
exports.VLP = VLP
exports.VLPStackstorm = VLPStackstorm

//logical onboarding
exports.serverInfoBasedOnProduct = getServerInfoBasedOnProduct
exports.userListEscalate = userListEscalate
exports.sendEmail = sendEmail
exports.getIPAddressBasedOnVLP = getIPAddressBasedOnVLP





