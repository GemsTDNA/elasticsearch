/**
 * Created by Radhika on 6/10/2017.
 */
var logger = require('../libs/logger').metricLogger



function documentExists(req,res,next){
    console.log(req.body)
    var elasticSearchClient = req.elasticSearchClient
    elasticSearchClient.search({
        index: 'rule-node',
        type: 'rule',
        body:{
            query:{
                match:{
                    ruleName: req.body.ruleName
                }
            }
        }

    }, function(error,response){
        if(error){
            logger.error(error)
            return next(error)
        }
        if(response.hits.hits.length>0)
        {
            res.json({"msg":"rule exists", "status":0})
        }
        else {
           insert(req,res,next)
        }
    })

}
function insert(req, res, next) {
    console.log("called")
    var elasticSearchClient = req.elasticSearchClient
    elasticSearchClient.index({
        index: 'rule-node',
        type: 'rule',
        body: req.body
    }, function (error, response) {
        if (error) {
            logger.error("error with pushing data to elastic search " + error)
            return next(error)

        }

        res.json({"status": 1})
    });
}

function getData(req, res, next) { // list all data in elastic search
    var elasticSearchClient = req.elasticSearchClient
    elasticSearchClient.search({
        index: 'rule-node',
        body: {
            from: 0, size: 1000,
            //  _source: ["name", "action", "type", "status", "createdBy"],
            query: {
                match_all: {}
            }
        }
    }, function (err, data) {
        if (err) {
            logger.error("getData API: error when querying elastic search " + JSON.stringify(err))
            return next(err)
        }
        if (data) {
            logger.info("elastic search returns results")
            res.json({"status": 1, "data": data.hits.hits})

        }
    })


}

exports.insert = documentExists
exports.getData = getData