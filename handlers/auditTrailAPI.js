/**
 * Created by Radhika on 9/27/2017.
 */
var Promise = require("bluebird");
var async = require('async');
var request = require('request')
var moment = require('moment')
var logger = require('../libs/logger').auditTrailLogger;
var config = require('config')
var st2Certificate = config.get('stackStorm.certificate')
var st2Key = config.get('stackStorm.key')
var promisifiedRequest = Promise.promisify(require("request"));
var util = require('util')
var path = require('path')
var errorHandler = require(config.errorMessages.path)

function insertAuditTrailData(req, res, next) {
    logger.info("insertAuditTrail function called")
    if (req.body.type == config.auditTrail.configChange) {


        isDisplayNameUnique(req.body.displayName, function (err, data) {
            if (err) {
                logger.info(err)
                //return next(new Error(config.errorMessages.error))
                return next(new errorHandler.serverError())
            }
            if (data.status == 0) {
                //return next(new Error(data.data))
                return next(new errorHandler.displayName())
            } else {
                // check if filePath is unique
                isFilePathUnique(req.body.product, req.body.vendor, req.body.location, req.body.IP, req.body.path, function (errCheckingFilePath, result) {
                    if (errCheckingFilePath) {
                        logger.info(errCheckingFilePath)
                        //return next(new Error(config.errorMessages.error))
                        return next(new errorHandler.serverError())
                    }
                    if (result.status == 0) {
                        //return next(new Error(result.data))
                        return next(new errorHandler.filePath())
                    } else {
                        // res.json(result)
                        //add code
                        logger.info("type is configuration change")
                        var jsonObj = {

                            "USERNAME": req.body.UserName,
                            "PRODUCT_NAME": req.body.product,
                            "VENDOR_NAME": req.body.vendor,
                            "LOCATION": req.body.location,
                            "path": req.body.path,
                            "IP_ADDRESS": req.body.IP,
                            "displayName": req.body.displayName,
                            "severity": req.body.severity

                        }
                        var url = 'http://' + req.body.IP + ':5000/configurationManagement/add'
                        logger.info("hitting the url via request module:" + url)

                        var options = {
                            url: url,
                            method: 'POST',
                            json: jsonObj
                        }
                        logger.info("the options params contain:" + JSON.stringify(options))
                        request.post(options, function (error, response, body) {
                            if (error) {
                                logger.error(error)
                                return next(new errorHandler.serverError())
                            }
                            var elasticSearchClient = req.elasticSearchClient
                            insertToAuditConfigureIndex(elasticSearchClient, req.body, function (errInsertingToAuditConfigure, dataInserted) {
                                if (errInsertingToAuditConfigure) {
                                    logger.error(errInsertingToAuditConfigure)
                                    //return next(new Error(config.errorMessages.error))
                                    return next(new errorHandler.serverError())
                                }
                                logger.info("the response sent by the configManagement API is shown below")
                                logger.info(body)
                                res.send(body)
                            })

                        })
                    }

                })
            }
        })


    } else if (req.body.type == config.auditTrail.accessDetails) {
        isDisplayNameUnique(req.body.displayName, function (err, data) {
            if (err) {
                logger.info(err)
                //return next(new Error(config.errorMessages.error))

                return next(new errorHandler.serverError())
            }
            if (data.status == 0) {
                return next(new errorHandler.displayName())
            } else {
                checkIfIPExistsForASetOfPLV(req.body.product, req.body.vendor, req.body.location, req.body.IP, function (errCheckingForUniqueIP, result) {
                    if (errCheckingForUniqueIP) {

                        logger.info(errCheckingForUniqueIP)
                        //return next(new Error(config.errorMessages.error))
                        return next(new errorHandler.serverError())
                    }
                    if (result.status == 0) {
                        return next(new errorHandler.IPisNotUnique())
                    } else {
                        //everything else
                        console.log("type is access details")
                        logger.info("type is access details")
                        logger.info("add Audit trail called with:" + JSON.toString(req.body))
                        var product = req.body.product
                        var vendor = req.body.vendor
                        var location = req.body.location
                        var IP = req.body.IP
                        var type = req.body.type
                        var displayName = req.body.displayName

                        ///add to index

                        var elasticSearchClient = req.elasticSearchClient

                        insertToAuditConfigureIndex(elasticSearchClient, req.body, function (err, response) {
                            if (err) {
                                logger.info(err)
                                //return next(new Error(config.errorMessages.error))
                                return next(new errorHandler.serverError())
                            }
                            searchUserAuditMonitoring(elasticSearchClient, req.body, function (errSearchingUserAuditMonitoring, resultFromUserAuditMonitoring) {
                                if (errSearchingUserAuditMonitoring) {

                                    logger.error(errSearchingUserAuditMonitoring)
                                    // return next(new Error(config.errorMessages.error))
                                    return next(new errorHandler.serverError())
                                }

                                if (resultFromUserAuditMonitoring.count > 0) {
                                    if (resultFromUserAuditMonitoring.flag == -1) {
                                        // append
                                        logger.info('append new IP')
                                        appendToUserAuditMonitoring(elasticSearchClient, req.body, function (errAppendingUserAuditMonitoring, dataAppended) {
                                            if (errAppendingUserAuditMonitoring) {
                                                logger.info(errAppendingUserAuditMonitoring)
                                                // return next(new Error(config.errorMessages.error))
                                                return next(new errorHandler.serverError())
                                            }
                                            res.json({"status": 1})
                                            logger.info("Passing params to function signalForUserAccess:" + product + " " + vendor + " " + location + " " + IP + " " + displayName)
                                            signalForUserAccess(product, vendor, location, IP, displayName, function (errWithHTTPRequest, HTTPRequestInfo) {
                                                if (errWithHTTPRequest) {
                                                    logger.info(errWithHTTPRequest)
                                                    // return next(new Error(config.errorMessages.error))
                                                    return next(new errorHandler.serverError())
                                                }
                                            })
                                            //  res.json({"status": 1})
                                        })
                                    } else if (resultFromUserAuditMonitoring.flag == 0) {
                                        logger.info("Do nothing as IP exists")
                                        res.json({"status": 1})
                                        logger.info("Passing params to function signalForUserAccess:" + product + " " + vendor + " " + location + " " + IP + " " + displayName)
                                        signalForUserAccess(product, vendor, location, IP, displayName, function (errWithHTTPRequest, HTTPRequestInfo) {
                                            if (errWithHTTPRequest) {
                                                logger.info(errWithHTTPRequest)
                                                // return next(new Error(config.errorMessages.error))
                                                return next(new errorHandler.serverError())
                                            }
                                        })
                                        //res.json({"status": 1})
                                    }
                                } else if (resultFromUserAuditMonitoring.count == 0) {
                                    //create
                                    logger.info("create")
                                    createUserAuditMonitoring(elasticSearchClient, req.body, function (errCreatingIndex, indexCreated) {
                                        if (errCreatingIndex) {
                                            logger.info(errCreatingIndex)
                                            //return next(new Error(config.errorMessages.error))
                                            return next(new errorHandler.serverError())
                                        }
                                        res.json({"status": 1})
                                        logger.info("Passing params to function signalForUserAccess:" + product + " " + vendor + " " + location + " " + IP + " " + displayName)
                                        signalForUserAccess(product, vendor, location, IP, displayName, function (errWithHTTPRequest, HTTPRequestInfo) {
                                            if (errWithHTTPRequest) {
                                                logger.info(errWithHTTPRequest)
                                                // return next(new Error(config.errorMessages.error))
                                                return next(new errorHandler.serverError())
                                            }
                                        })
                                        // res.send(indexCreated)
                                    })

                                } else {
                                    res.json({"status": 1, "msg": "success"})
                                }

                            })


                        })
                    }


                })
            }

        })
    } else {
        res.send("invalid type sent:" + req.body.type)
    }

}


function signalForUserAccess(product, vendor, location, IP, displayName, callback) {
    logger.info("signal for user access function recieves:" + product + " " + vendor + " " + location + " " + IP + " " + displayName)

    var url = config.signalForUserAccess.url;
    var headers = {
        'Accept': config.signalForUserAccess.Accept,
        'X-Auth-Token': config.signalForUserAccess.AuthToken,
        'Content-type': config.signalForUserAccess.ContentType,
    };


    async.series([
            function (callback) {
                promisifiedRequest({
                    url: url,
                    headers: headers,
                    method: "POST",
                    json: true,
                    body: [{
                        "client": "local",
                        "tgt": IP,
                        "fun": "cmd.run",

                        "kwarg": {
                            "cmd": 'export PM2_HOME=/root/.pm2 && pm2 start /root/.GEMS/audit.sh --name="GEMS-audit"',
                            "stdin": "y"
                        },
                        "username": "salt",
                        "password": "salt",
                        "eauth": "pam"
                    }]
                })
                    .then(function (response) {
                        return callback(null, response.body)
                    })
                // .catch(function (err) {
                //     return callback(err, null)
                // })
            },
            function (callback) {
                promisifiedRequest({
                    url: url,
                    headers: headers,
                    method: "POST",
                    json: true,
                    body: [{
                        "client": "local",
                        "tgt": IP,
                        "fun": "cp.get_file",
                        "kwarg": {"path": "salt://audit.json", "dest": "/root/.GEMS/audit.json"},
                        "username": "salt",
                        "password": "salt",
                        "eauth": "pam"
                    }]
                })
                    .then(function (response) {
                        return callback(null, response.body)
                    })
                // .catch(function (err) {
                //     return callback(err, null)
                // })
            },
            function (callback) {
                var args = " " + product + " " + vendor + " " + location + " " + IP + " " + displayName + " /etc/rsyslog.conf /root/.GEMS/audit.json";
                promisifiedRequest({
                    url: url,
                    headers: headers,
                    method: "POST",
                    json: true,
                    body: [{
                        "client": "local",
                        "tgt": IP,
                        "fun": "cmd.script",
                        "cwd": '/root',
                        "kwarg": {"source": "salt://Audit_config_update.py", "args": args},
                        "username": "salt",
                        "password": "salt",
                        "eauth": "pam"
                    }]
                })
                    .then(function (response) {
                        return callback(null, response.body)
                    })
                // .catch(function (err) {
                //     return callback(err, null)
                // })
            },
            function (callback) {
                promisifiedRequest({
                    url: url,
                    headers: headers,
                    method: "POST",
                    json: true,
                    body: [{
                        "client": "local",
                        "tgt": IP,
                        "fun": "cmd.run",
                        "kwarg": {"cmd": 'service rsyslog restart', "stdin": "y"},
                        "username": "salt",
                        "password": "salt",
                        "eauth": "pam"
                    }]
                })
                    .then(function (response) {
                        return callback(null, response.body)
                    })
                // .catch(function (err) {
                //    return  callback(err, null)
                // })
            }
        ],
        function (err, results) {
            logger.info("the URL:https://10.225.253.133:9999/run returns:" + JSON.stringify(results))

            if (err) {
                return callback(err, null)
            }

            //callback(null,results)
        });
}


function signalForUserAccessDelete(IP, callback) {

    var url = config.signalForUserAccess.url;
    var headers = {
        'Accept': config.signalForUserAccess.Accept,
        'X-Auth-Token': config.signalForUserAccess.AuthToken,
        'Content-type': config.signalForUserAccess.ContentType,
    };


    promisifiedRequest({
        url: url,
        headers: headers,
        method: "POST",
        json: true,
        body: [{
            "client": "local",
            "tgt": IP,
            "fun": "cmd.run",
            //"cd /root/.pm2/ && pm2 stop GEMS-audit", "stdin": "y"
            "kwarg": {"cmd": "export PM2_HOME=/root/.pm2 && pm2 stop GEMS-audit", "stdin": "y"},
            "username": "salt",
            "password": "salt",
            "eauth": "pam"
        }]
    })
        .then(function (response) {
            return callback(null, response)
        })
        .catch(function (err) {

            return callback(err)
        })
}

// yet to decide if the below function is required
function searchAuditConfigureIndex(elasticSearchClient, data, callback) {
    elasticSearchClient.search({
        index: "user_audit_configure",
        type: "logs",
        body: {
            query: {
                bool: {
                    must: [
                        {term: {PRODUCT_NAME: data.product}},
                        {term: {LOCATION: data.location}},
                        {term: {VENDOR_NAME: data.vendor}},
                        {term: {IP_ADDRESS: data.IP}}
                    ]
                }
            }
        }
    }, function (err, response) {
        if (err) {
            return callback(err)
        }


        if (response.hits.total == 0) {
            //create a record
            return callback({"status": 1})
        } else {
            return callback({"status": 0})


        }


    })
}

function insertToAuditConfigureIndex(elasticSearchClient, data, callback) {

    var date = moment().format('YYYY-MM-DD hh:mm:ss')


    if (data.type == config.auditTrail.accessDetails) {
        var obj = {
            'PRODUCT_NAME': data.product,
            'VENDOR_NAME': data.vendor,
            'LOCATION': data.location,
            'IP': data.IP,
            'TYPE': config.auditTrail.accessDetails,
            'severity': data.severity,
            'displayName': data.displayName,
            'time': date
        }
    } else {
        var obj = {
            'PRODUCT_NAME': data.product,
            'VENDOR_NAME': data.vendor,
            'LOCATION': data.location,
            'IP': data.IP,
            'TYPE': config.auditTrail.configChange,
            'severity': data.severity,
            'filePath': data.path,
            'displayName': data.displayName,
            'time': date

        }
    }
    elasticSearchClient.index({
        index: "user_audit_configure",
        type: "logs",
        body: obj

    }, function (err, response) {
        if (err) {

            return callback(err)
        }
        //logger.info("addAuditDetails for " + data.product + " " + data.vendor + " " + data.location + " " + "with IP" + " " + data.IP + " " + "has been added to userAuditMonitoring")
        return callback(null, {"status": 1})


    })
}


function createUserAuditMonitoring(elasticSearchClient, data, callback) {

    elasticSearchClient.index({
        index: "user_audit_monitoring",
        type: "logs",
        body: {

            PRODUCT_NAME: data.product,
            VENDOR_NAME: data.vendor,
            LOCATION: data.location,
            IP: [data.IP]
        }

    }, function (err, response) {
        if (err) {

            return callback(err)
        }
        logger.info("addAuditDetails for " + data.product + " " + data.vendor + " " + data.location + " " + "with IP" + " " + data.IP + " " + "has been added to userAuditMonitoring")
        return callback(null, {"status": 1})


    })
}


function appendToUserAuditMonitoring(elasticSearchClient, data, callback) {
    elasticSearchClient.updateByQuery({
        index: "user_audit_monitoring",
        type: "logs",
        body: {
            script: {
                inline: "ctx._source.IP.add(params.IP)",
                params: {"IP": data.IP}
            },
            query: {
                bool: {
                    must: [
                        {term: {PRODUCT_NAME: data.product}},
                        {term: {LOCATION: data.location}},
                        {term: {VENDOR_NAME: data.vendor}}
                    ]
                },

            }

        }

    }, function (err, response) {
        if (err) {
            return callback(err)
        }
        //logger.info("addAuditDetails for "+data.product+" "+data.vendor+" "+location+" "+"with IP"+" "+IP+" "+"has been added to userAuditMonitoring")
        // return callback(null,response)
        return callback(null, {"status": 1})
    })
}


function searchUserAuditMonitoring(elasticSearchClient, data, callback) {
    elasticSearchClient.search({
        index: "user_audit_monitoring",
        type: "logs",
        body: {
            query: {
                bool: {
                    must: [
                        {term: {PRODUCT_NAME: data.product}},
                        {term: {LOCATION: data.location}},
                        {term: {VENDOR_NAME: data.vendor}}
                    ]
                }
            }
        }
    }, function (err, response) {
        if (err) {
            return callback(err)
        }


        if (response.hits.total == 0) {
            //create a record
            return callback(null, {"count": response.hits.total, "flag": 1}) //create a record
        } else {
            //append
            var IPArr = response.hits.hits[0]._source.IP
            if (IPArr.indexOf(data.IP) == -1) {
                return callback(null, {"count": response.hits.total, "flag": -1}) // IP not found , append
            } else {
                return callback(null, {"count": response.hits.total, "flag": 0}) //Ip found , do nothing
            }


        }


    })

}


function listUserAuditConfigureDetails(req, res, next) {
    req.elasticSearchClient = elasticSearchClient
    elasticSearchClient.search({
        index: "user_audit_configure",
        type: "logs",
        body: {
            "size": 1000,
            "query": {
                "match_all": {}
            },

            "sort": {"time": {"order": "desc"}}
        }

    }, function (err, response) {
        if (err) {
            logger.info(err)
            // return next(new Error(config.errorMessages.error))
            return next(new errorHandler.serverError())
        }
        var resArr = []
        response.hits.hits.forEach(function (item) {
            item._source._id = item._id
            resArr.push(item._source)
        })
        res.json({"status": 1, "data": resArr})
    })
}


function validateForEdit(type, displayName, product, vendor, location, IP, path, _id, callback) {
    if (type == config.auditTrail.configChange) {
        isDisplayNameUniqueForEdit(displayName, _id, function (err, data) {
            if (err) {
                return callback(err)
            }
            if (data.status == 0) {
                return callback(null, data)
            } else {
                // check if filePath is unique
                isFilePathUniqueForEdit(product, vendor, location, IP, path, _id, function (errCheckingFilePath, result) {
                    if (errCheckingFilePath) {
                        return callback(errCheckingFilePath)
                    }

                    return callback(null, result)


                })
            }
        })


    }

    if (type == config.auditTrail.accessDetails) {
        console.log("access details")
        isDisplayNameUnique(displayName, function (err, data) {
            if (err) {
                return callback(err)
            }
            if (data.status == 0) {
                return callback(null, data)
            } else {
                checkIfIPExistsForASetOfPLV(product, vendor, location, IP, function (errCheckingForUniqueIP, result) {
                    if (errCheckingForUniqueIP) {
                        return callback(errCheckingForUniqueIP)
                    }
                    return callback(null, result)

                })
            }

        })
    }
}


// not in use , will be removed when UI doesn't point to this API anymore
function validateWhenAddingAuditTrailData(req, res, next) {
    if (req.body.type == config.auditTrail.configChange) {
        isDisplayNameUnique(req.body.displayName, function (err, data) {
            if (err) {
                logger.info(err)
                //return next(new Error(config.errorMessages.error))
                return next(new errorHandler.serverError())
            }
            if (data.status == 0) {
                //return next(new Error(data.data))
                return next(new errorHandler.displayName())
            } else {
                // check if filePath is unique
                isFilePathUnique(req.body.product, req.body.vendor, req.body.location, req.body.IP, req.body.path, function (errCheckingFilePath, result) {
                    if (errCheckingFilePath) {
                        logger.info(errCheckingFilePath)
                        //return next(new Error(config.errorMessages.error))
                        return next(new errorHandler.serverError())
                    }
                    if (result.status == 0) {
                        //return next(new Error(result.data))
                        return next(new errorHandler.filePath())
                    } else {
                        res.json(result)
                    }

                })
            }
        })


    }

    if (req.body.type == config.auditTrail.accessDetails) {
        console.log("access details")
        isDisplayNameUnique(req.body.displayName, function (err, data) {
            if (err) {
                logger.info(err)
                //return next(new Error(config.errorMessages.error))
                return next(new errorHandler.serverError())
            }
            if (data.status == 0) {
                return next(new errorHandler.displayName())
            } else {
                checkIfIPExistsForASetOfPLV(req.body.product, req.body.vendor, req.body.location, req.body.IP, function (errCheckingForUniqueIP, result) {
                    if (errCheckingForUniqueIP) {

                        logger.info(errCheckingForUniqueIP)
                        //return next(new Error(config.errorMessages.error))
                        return next(new errorHandler.serverError())
                    }
                    if (result.status == 0) {
                        return next(new errorHandler.IPisNotUnique())
                    } else {
                        //everything else
                    }


                })
            }

        })
    }
}

function checkIfIPExistsForASetOfPLV(product, vendor, location, IP, callback) {
    console.log(product + " " + vendor + " " + location + " " + IP)
    elasticSearchClient.count({
        index: "user_audit_configure",
        type: "logs",
        body: {
            query: {
                bool: {
                    must: [{term: {"PRODUCT_NAME": product}},
                        {term: {"VENDOR_NAME": vendor}},
                        {term: {"LOCATION": location}},
                        {term: {"IP": IP}},
                        {term: {"TYPE": config.auditTrail.accessDetails}}]
                }

            }
        }
    }, function (err, response) {
        if (err) {
            return callback(err)
        }

        if (response.count == 0) {
            callback(null, {"status": 1, "data": "success"})
        } else {
            callback(null, {"status": 0, "data": config.errorMessages.IP})

        }
    })
}


function deleteUserAuditConfigureData(req, res, next) {
// will require type and IP
    logger.info("delete user audit configure data:" + JSON.stringify(req.body))
    console.log("delete api called")
    //req.elasticSearchClient = elasticSearchClient
    if (req.body.type == config.auditTrail.configChange) {
        fetchDataForConfigDelete(req.body._id, function (errWhileFetchingData, dataFetched) {
            if (errWhileFetchingData) {
                logger.info(errWhileFetchingData)
                return next(new errorHandler.serverError())
            }

            var objToSendViaRequest = {

                "PRODUCT_NAME": dataFetched.hits.hits[0]._source.PRODUCT_NAME,
                "VENDOR_NAME": dataFetched.hits.hits[0]._source.VENDOR_NAME,
                "LOCATION": dataFetched.hits.hits[0]._source.LOCATION,
                "path": dataFetched.hits.hits[0]._source.filePath,
                "IP_ADDRESS": dataFetched.hits.hits[0]._source.IP,
                "displayName": dataFetched.hits.hits[0]._source.displayName,
                "severity": dataFetched.hits.hits[0]._source.severity,
                "USERNAME":req.body.UserName
            }
            var url = 'http://' + req.body.IP + ':5000/configurationManagement/delete'

            logger.info("hitting the url via request module to update the old path:" + url)
            var options = {
                url: url,
                method: 'POST',
                json: objToSendViaRequest
            }
            logger.info("the options params contain:" + util.format(options))
            request.post(options, function (error, response, body) {
                if (error) {
                    logger.info(error)
                    //return next(new Error(config.errorMessages.error))
                    return next(new errorHandler.serverError())
                } else {
                    deleteDataFromUserAuditConfigure(req.body._id, function (errWhileDeletingData, dataDeleted) {
                        if (errWhileDeletingData) {
                            logger.info(errWhileDeletingData)
                            //return next(new Error(config.errorMessages.error))
                            return next(new errorHandler.serverError())
                        }
                        res.json({"status": 1})
                    })

                }
            })


        })

    } else {
        console.log("type is access details")
        //delete id
        //call the API

        signalForUserAccessDelete(req.body.IP, function (err, data) {
            if (err) {
                logger.info("signalForUserAccess returns an error")
                logger.info(err)
                //return next(new Error(config.errorMessages.error))
                return next(new errorHandler.serverError())
            }
            logger.info("signal for useraccess returns")
            logger.info(data)
            logger.info(data.statusCode)

            if (data.statusCode == 200) {
                logger.info("the status code is 200")
                console.log("in the if condition")
                deleteDataFromUserAuditConfigure(req.body._id, function (errWhileDeletingData, dataDeleted) {
                    if (errWhileDeletingData) {
                        logger.info(errWhileDeletingData)
                        //return next(new Error(config.errorMessages.error))
                        return next(new errorHandler.serverError())
                    }
                    res.json(dataDeleted)
                })
            } else {
                res.status(500)
                res.json({"status": 0})
            }
        })

    }

}

function deleteDataFromUserAuditConfigure(_id, callback) {
    elasticSearchClient.delete({
        index: 'user_audit_configure',
        type: 'logs',
        id: _id
    }, function (err, data) {
        if (err) {
            logger.error(err)
            return callback(err)

        }
        //signalForUserAccessOnDelete(IP,function(err,))
        return callback(null, {"status": 1})

    })
}


function editUserAuditTrailData(req, res, next) {

    logger.info("edit user audit trail API called:" + util.format(req.body))
    var path = req.body.newPath
    var oldPath = req.body.oldPath
    var displayName = req.body.displayName
    var IP = req.body.IP
    var type = req.body.type
    var product = req.body.product
    var vendor = req.body.vendor
    var location = req.body.location
    var IP = req.body.IP
    var _id = req.body._id
    var severity = req.body.severity


logger.info("the DISPLAYNAME in EDIT API is:"+ displayName)


//    if (path == undefined) {
//        var inlineScript = "ctx._source.displayName='"+displayName+"'";
//    }
    //  if (path != undefined && displayName != undefined) {
    var inlineScript = "ctx._source.filePath='"+path+"';ctx._source.displayName='"+displayName+"'";
    //}
    //type,displayName,product,vendor,location,IP,path,callback
    validateForEdit(type, displayName, product, vendor, location, IP, path, _id, function (validationErr, validationSuccess) {
        if (validationErr) {
            logger.info(validationErr)
            //return next(new Error(config.errorMessages.error))
            return next(new errorHandler.serverError())
        }
        if (validationSuccess.status == 0) {
            return next(new errorHandler.customErrorMessage(validationSuccess.data, "200"))

        } else {
            //begin
            if (req.body.type == config.auditTrail.configChange) {

                var objToSendViaRequest =
                    {
                        "USERNAME": req.body.UserName,
                        "PRODUCT_NAME": product,
                        "VENDOR_NAME": vendor,
                        "LOCATION": location,
                        "path": path,
                        "IP_ADDRESS": IP,
                        "displayName": displayName,
                        "severity": severity,
                        "oldpath": oldPath
                    }
                var url = 'http://' + req.body.IP + ':5000/configurationManagement/edit'

                logger.info("hitting the url via request module to update the old path:" + url)
                var options = {
                    url: url,
                    method: 'POST',
                    json: objToSendViaRequest
                }
                logger.info("the options params contain:" + util.format(options))
                request.post(options, function (error, response, body) {
                    if (error) {
			logger.info("the error returned by the request module on hitting the API is")
                        logger.info(error)
                        
                        return next(new errorHandler.serverError())
                    } else {
                        elasticSearchClient.updateByQuery({
                            index: "user_audit_configure",
                            type: "logs",
                            body: {
                                script: {
                                    inline: inlineScript,

                                },
                                query: {
                                    match: {
                                        _id: req.body._id
                                    }
                                }
                            }
                        }, function (err, response) {
                            if (err) {
                                logger.info(err)
                                // return next(new Error(config.errorMessages.error))
                                return next(new errorHandler.serverError())
                            } else {
                                return res.send({"status": 1, "msg": "success"})
                            }


                        })

                    }
                })
            }
            //end
            else {


                elasticSearchClient.updateByQuery({
                    index: "user_audit_configure",
                    type: "logs",
                    body: {
                        script: {
                            inline: inlineScript,

                        },
                        query: {
                            match: {
                                _id: req.body._id
                            }
                        }
                    }
                }, function (err, response) {
                    if (err) {
                        logger.info(err)
                        // return next(new Error(config.errorMessages.error))
                        return next(new errorHandler.serverError())
                    } else {
                        return res.send({"status": 1, "msg": "success"})
                    }


                })
            }
        }

    })
}

//not in use
function validationWhileEditingUserAuditData(req, res, next) {
    isDisplayNameUnique(req.body.displayName, function (err, data) {
        if (err) {
            logger.info(err)
            //return next(new Error(config.errorMessages.error))
            return next(new errorHandler.serverError())
        }
        res.json(data)
    })

}

function isDisplayNameUnique(displayName, callback) {

    elasticSearchClient.count({
        index: "user_audit_configure",
        type: "logs",
        body: {
            query: {
                match: {"displayName": displayName}

            }
        }
    }, function (err, response) {
        if (err) {
            return callback(err)
        }
        if (response.count != 0) {
            callback(null, {"status": 0, "data": config.errorMessages.displayName})
        } else {
            callback(null, {"status": 1, "data": "success"})

        }
    })
}

function isFilePathUniqueForEdit(product, vendor, location, IP, path, _id, callback) {
    console.log(product + vendor + location + IP + path + _id)
    elasticSearchClient.count({
        index: "user_audit_configure",
        type: "logs",
        body: {
            query: {
                bool: {
                    must: [
                        {term: {"PRODUCT_NAME": product}},
                        {term: {"VENDOR_NAME": vendor}},
                        {term: {"LOCATION": location}},
                        {term: {"IP": IP}},
                        {term: {"filePath": path}}


                    ],
                    must_not: {term: {"_id": _id}}

                }
            }
        }
    }, function (err, response) {
        if (err) {
            return callback(err)
        }


        if (response.count > 0) {

            return callback(null, {"status": 0, "data": config.errorMessages.filePath})
        } else {
            return callback(null, {"status": 1, "data": "success"})
        }
    })
}


function isDisplayNameUniqueForEdit(displayName, _id, callback) {
    elasticSearchClient.count({
        index: "user_audit_configure",
        type: "logs",
        body: {
            query: {
                bool: {
                    must: {term: {"displayName": displayName}},
                    must_not: {term: {"_id": _id}}
                }


            }
        }
    }, function (err, response) {
        if (err) {
            return callback(err)
        }
        if (response.count != 0) {

            callback(null, {"status": 0, "data": config.errorMessages.displayName})
        } else {
            callback(null, {"status": 1, "data": "success"})

        }
    })
}

function isFilePathUnique(product, vendor, location, IP, path, callback) {
    elasticSearchClient.count({
        index: "user_audit_configure",
        type: "logs",
        body: {
            query: {
                bool: {
                    must: [
                        {term: {"PRODUCT_NAME": product}},
                        {term: {"VENDOR_NAME": vendor}},
                        {term: {"LOCATION": location}},
                        {term: {"IP": IP}},
                        {term: {"filePath": path}}


                    ]

                }
            }
        }
    }, function (err, response) {
        if (err) {
            return callback(err)
        }

        if (response.count != 0) {
            return callback(null, {"status": 0, "data": config.errorMessages.filePath})
        } else {
            return callback(null, {"status": 1, "data": "success"})
        }
    })
}


/*
 *  elasticSearchClient.search({
 index: "user_audit_configure",
 type: "logs",
 body: {
 query: {
 bool: {
 must: [
 {term: {PRODUCT_NAME: data.product}},
 {term: {LOCATION: data.location}},
 {term: {VENDOR_NAME: data.vendor}},
 {term: {IP_ADDRESS: data.IP}}
 ]
 }
 }
 }
 }
 */

function getModifiedFileBasedOnOriginalFile(req, res, next) {
    var fileName = req.body.fileName
    elasticSearchClient.search({
        index: "configdiff",
        type: "logs",
        body: {
            size: config.auditTrail.numberOfFiles,
            query: {
                bool: {
                    must: [
                        {term: {fileName: req.body.fileName}},
                        {term: {Description: "File Modified."}}
                    ]
                }
            },
            sort: {"TIME": {"order": "desc"}}
        }
    }, function (err, response) {
        if (err) {
            logger.error(err)
            return next(new errorHandler.serverError())
        }
        // fetch modified  files from data
        var original = []
        var modified = []

        response.hits.hits.forEach(function (item) {
            // item._source._id = item._id
            original.push(item._source.Original)
            modified.push(item._source.Modified)

        })
        res.json({"status": 1, "original": original, "modified": modified})
    })
}


function fetchDataForConfigDelete(_id, callback) {
    elasticSearchClient.search({
        index: "user_audit_configure",
        type: "logs",
        body: {
            query: {
                match: {_id: _id}

            }
        }
    }, function (err, response) {
        if (err) {
            //logger.error(err);
            return callback(err)
        }
        return callback(null, response)

    })

}


exports.insertAuditTrailData = insertAuditTrailData
exports.listUserAuditConfigureDetails = listUserAuditConfigureDetails
exports.validateWhenAddingAuditTrailData = validateWhenAddingAuditTrailData
exports.deleteUserAuditConfigureData = deleteUserAuditConfigureData
exports.editUserAuditTrailData = editUserAuditTrailData
//exports.validationWhileEditingUserAuditData = validationWhileEditingUserAuditData
exports.getModifiedFileBasedOnOriginalFile = getModifiedFileBasedOnOriginalFile
