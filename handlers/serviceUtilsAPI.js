
var moment = require('moment');
var logger = require('../libs/logger').serviceUtilsLogger

function getServiceUtils(req, res) {
    logger.info("All is well with elastic search");
    var elasticSearchClient = req.elasticSearchClient
    elasticSearchClient.search({
        index: 'service-utils',
        type: 'logs',
        body: {
            size: 1000,
            query: {
                match_all:{}
            }
        }
    }, function (err, responseFromElasticSearch) {
        if (err) {
            logger.error("elastic search error" + JSON.stringify(err))
            return next(err)
        } else {
            var array = [];
            responseFromElasticSearch.hits.hits.forEach(function (hit) {
                var jsonObj = hit['_source'];
                jsonObj['_id'] = hit['_id'];
		jsonObj['USED'] = parseInt(jsonObj['USED']);
		jsonObj['AVAILABLE'] = parseInt(jsonObj['AVAILABLE']);
		jsonObj['LICENSED'] = parseInt(jsonObj['LICENSED']);
                jsonObj.time = moment.parseZone(hit._source.time).local().format("YYYY-MM-DD HH:mm:ss");
                array.push(jsonObj);
            })
            logger.info("elastic search returns results")
            res.json({"status": 1, "data": array});
        }
    })
}

function getDetailedServiceUtils (req, res) {
	if(req.body.SERVICE_NAME == "P2PSMS") {
		var elasticSearchClient = req.elasticSearchClient
		var query = {"bool":{"must":[{"term":{"PRODUCT_NAME":{"value":req.body.PRODUCT_NAME}}},{"term":{"LOCATION":{"value":req.body.LOCATION}}},{"term":{"VENDOR_NAME":{"value":req.body.VENDOR_NAME}}}]}}
		elasticSearchClient.search({
			index: 'a2psms',
			type: 'logs',
			body: {
				size: 100,
				query: {"bool":{"must":[{"term":{"PRODUCT_NAME":{"value":req.body.PRODUCT_NAME}}},{"term":{"LOCATION":{"value":req.body.LOCATION}}},{"term":{"VENDOR_NAME":{"value":req.body.VENDOR_NAME}}}]}}
			}
		}, function (err, responseFromElasticSearch) {
			var resArray = [];
			responseFromElasticSearch.hits.hits.forEach(function (Obj) {
				var temp = {};
				temp = Obj._source
				//temp._id = Obj._id
				delete temp.PRODUCT_NAME;
				delete temp.VENDOR_NAME;
				delete temp.LOCATION;
				Object.keys(temp).forEach(function (key) {
					var tempObj = {};
					tempObj = temp[key];
					tempObj.ACCOUNT_NAME = key;
					resArray.push(tempObj);
				})
			})
			res.send(resArray);
		})
	}
	else {
		var elasticSearchClient = req.elasticSearchClient
		var query = {"query":{"bool":{"must":[{"term":{"PRODUCT_NAME":{"value":req.body.PRODUCT_NAME}}},{"term":{"LOCATION":{"value":req.body.LOCATION}}},{"term":{"VENDOR_NAME":{"value":req.body.VENDOR_NAME}}}]}}}
		elasticSearchClient.search({
			index: 'accounts',
			type: 'logs',
			body: {
				size: 100,
				query: {"bool":{"must":[{"term":{"PRODUCT_NAME":{"value":req.body.PRODUCT_NAME}}},{"term":{"LOCATION":{"value":req.body.LOCATION}}},{"term":{"VENDOR_NAME":{"value":req.body.VENDOR_NAME}}}]}}
			}
		}, function (err, responseFromElasticSearch) {
			var resArray = [];
			responseFromElasticSearch.hits.hits.forEach(function (Obj) {
				var temp = {};
				temp = Obj._source
				//temp._id = Obj._id
				resArray.push(temp)
			})
			res.send(resArray);
		})
	}
}

exports.getServiceUtils = getServiceUtils
exports.getDetailedServiceUtils = getDetailedServiceUtils
//exports.geta2pServiceUtils = geta2pServiceUtils
